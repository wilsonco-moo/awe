# AWE

The AWE library (**A**sset based **W**orld **E**nvironment) provides a series of
utilities for 3D game development. AWE provides resource management for
textures, models, assets, instances and level design, physics
integration using [Bullet Physics](https://github.com/bulletphysics/bullet3),
utilities for heightmaps, various types of camera, lighting and dealing with
shaders. A bash script is also provided for preprocessing shaders.

This library was developed as part of
[this project](https://gitlab.com/wilsonco-moo/newgl), and contains many of the
techniques used.


## Dependencies and compiling

This library requires a C++14 compliant compiler, although I have only tested
it with GCC. To compile, the following dependencies are required:
 * OpenGL library and headers, this can be linked by passing `-lGL`.
 * Freeglut: this is available as the `freeglut3-dev` package in Debian and
   other Linux distributions. This can be linked by passing `-lglut`.
 * [glm](https://github.com/g-truc/glm): This is used extensively for vector and
   matrix maths.
 * [gl3w](https://github.com/skaslev/gl3w): This is required for access to
   modern OpenGL functions.
 * [Bullet Physics](https://github.com/bulletphysics/bullet3): Required for
   physics integration. Only the rigid body physics and dependent parts
   of the Bullet Physics library are required, (see the
   [Bullet Physics user manual](https://github.com/bulletphysics/bullet3/raw/master/docs/Bullet_User_Manual.pdf)).
 * [tinyxml2](https://github.com/leethomason/tinyxml2): Required for reading
   XML files for the world, and for assets.
 * [My thread pool library](https://gitlab.com/wilsonco-moo/threading): This
   is used for asynchronously loading resources such as 3D models and textures.
 * [wool (Rasc's graphics utility library)](https://gitlab.com/wilsonco-moo/wool):
   This is used mainly for loading images, since it has a copy of the
   [lodepng library](https://github.com/lvandeve/lodepng) built in. In the
   future I might remove wool as a dependency, and require only lodepng.
 * [rascUI](https://gitlab.com/wilsonco-moo/rascui): This is required mainly for
   some of its data structure utilities.


## Features (more detailed)

**ModelManager**

This provides a convenient way to load models from `obj` files, where each
vertex has an associated UV map coordinate, but no associated normals. It is
expected that normals are provided separately using an object space normal map
(see `AssetManager`).

A model can be requested from a filename, and a `Model` instance is returned.
The model file is automatically loaded asynchronously, then a Vertex Buffer
Object is created for the model. This allows the model to be conveniently
rendered, using a call to `glDrawArrays` or similar.

The same model file can be loaded multiple times independently, but the
implementation will only load it once, and only create one vertex buffer
object. Once the user is done with the model, they can abandon it using
`ModelManager`'s `abandonModel` method. Once no usages of the model remain,
it is removed from memory.

For more information about model format and usage, see documentation in
`ModelManager.h` and `Model.h`.


**TextureManager**

This provides a convenient way to load many textures, from `png` files.
A texture can be requested using a filename, and a `void *` texture token
is returned. Just like `ModelManager`, the same texture file can be
requested multiple times, but will not be duplicated internally. Once
the user is done with the texture, they can abandon it. Once there are no
more usages of a texture, it is removed from memory.

`TextureManager` internally loads all texture files asynchronously, builds
mipmaps, and loads them all into a single texture array. This texture array
is reallocated as textures are loaded/unloaded. Copying texture data is done
across multiple frames, to avoid large delays.

To access the layer within the texture array, from a texture token, the
`TextureManager::getTextureLayer` method should be called each frame. It is
intended that this is used for a shader uniform variable, in order to draw
using the appropriate texture. This is done automatically by `Asset` for
drawing instances.


**AssetManager**

This provides a convenient way to load Assets, defined by `xml` files.
An asset is comprised of a model (loaded using `ModelManager`), a texture
and object space normal map texture (loaded using `TextureManager`),
a base transformation (altering the display of the model), and physics
information (static/dynamic, mass and shape).

Assets can be loaded just like models and textures: from a filename, without
internal duplication if loaded multiple times, returning an `Asset` instance.
Internally, the asset automatically requests its model and texture for the
duration of its existence.

`AssetManager` provides the ability to draw all instances of all assets
registered to it, so requires a shader program, and provides a `draw` method.

Assets can also be deformed using a skeleton, using the techniques developed
in [my newgl project](https://gitlab.com/wilsonco-moo/newgl) -
4: transformation demo. Note that this is fairly complicated to do,
see documentation in `SkelNode.h`. The skeleton is defined as part of the
asset's XML file.


**Instances**

An instance of an asset can be created using the `Instance` class. Instances
can be created manually, or loaded using `WorldManager`. Each
instance requires a unique name, the filename of an asset, and an initial
transformation. That asset is automatically requested for the duration of the
existence of the instance. After being created, all instances are automatically
drawn by calling the `draw` method of the related `AssetManager`. If enabled
for the related asset, `Instances` are automatically moved according to
rigid body physics.


**LightingManager**

`LightingManager` provides a simple grid based lighting system. Lights can be
created using the built-in `Light` class, or loaded using `WorldManager`.
The world is split into an axis aligned 3D grid with up to 4096 grid cells,
and contains up to 255 point light sources, where up to 4 light sources can
overlap in each grid cell.

This is provided to compatible shaders using a uniform buffer object, allowing
multiple shader programs to have consistent lighting. A single directional light
source, and an ambient light colour is also provided, using regular uniform
variables.


**WorldManager**

`WorldManager` provides a way to load information about many instances from
an `xml` file. The `xml` file specifies the names and locations of instances,
properties of the lighting grid, and positions of lights.

The `xml` file provides a way to build scenes. It features `<transform>` and
`<array>` tags, which provide the ability to move sections of the scene,
and duplicate instances.

There are some scripts in my
[lunar logistics project](https://gitlab.com/wilsonco-moo/lunarlogistics),
at [development/blenderExtract](https://gitlab.com/wilsonco-moo/lunarlogistics/-/tree/master/development/blenderExtract),
which make it possible to build an awe world `xml` file from a blender scene,
(`blend` file).


**Heightmaps**

`HeightMap` and `PhysicsHeightMap` provide functionality for heightmaps,
with terrain textures and normal maps. The texture format for using these
is fairly complicated, see documentation in `HeightMap.h`. There are scripts
for automating the creation of height and terrain map textures in my
[lunar logistics project](https://gitlab.com/wilsonco-moo/lunarlogistics),
at [development/heightmapConvert](https://gitlab.com/wilsonco-moo/lunarlogistics/-/tree/master/development/heightmapConvert).


**Other utilities and shaders**

 * This library also provides a utility for compiling shaders, (in `Util.h`).
 * A system for working out progress while loading resources is provided in
   `ProgressControl.h`.
 * Shaders for drawing heightmaps and assets are provided in `heightmap/shader/`
   and `defaultShader/` respectively.
 * Various camera controllers (first person, free camera, third person camera
   following an instance), are provided in `world/`.
 * Utility methods for converting between glm and bullet physics vectors and
   quaternions are provided in `ConversionUtil.h`.


## Usage examples and screenshots

The most up-to-date example is
[my Lunar Logistics game project](https://gitlab.com/wilsonco-moo/lunarlogistics).
This uses the heightmap system and physics, for a simple car game.
![Image](https://gitlab.com/wilsonco-moo/lunarlogistics/-/wikis/screenshots/moonTextures.png)

An older example can be found in
[my newgl project](https://gitlab.com/wilsonco-moo/newgl) - 6: Models, physics,
lighting and first person camera. This allows a player to interact with the
scene, using physics.
![Image](https://gitlab.com/wilsonco-moo/newgl/-/raw/master/development/screenshots/6-model.png)
