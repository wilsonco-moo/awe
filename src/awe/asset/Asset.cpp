/*
 * Asset.cpp
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "Asset.h"

#include <glm/gtx/matrix_decompose.hpp>
#include <threading/ThreadQueue.h>
#include <tinyxml2/tinyxml2.h>
#include <algorithm>
#include <iostream>
#include <cstdlib>

#include "../util/physics/AssetCollisionShapeDef.h"
#include "../texture/TextureManager.h"
#include "../asset/AssetManager.h"
#include "../model/ModelManager.h"
#include "../instance/Instance.h"
#include "../model/Model.h"
#include "../util/Util.h"

namespace awe {

    const std::string Asset::ASSET_XML_VERSION = "0.2";
    
    const size_t Asset::UNIFORM_SIZE = 16384;
    
    
    Asset::Asset(threading::ThreadQueue * queue, TextureManager * textureManager, ModelManager * modelManager, AssetManager * assetManager, const std::string & filename) :
        // Setup initially
        queue(queue),
        token(queue->createToken()),
        textureManager(textureManager),
        modelManager(modelManager),
        assetManager(assetManager),
        filename(filename),
        
        // Loading first XML info (properties)
        baseTexFilename(""),
        normalMapTexFilename(""),
        modelFilename(""),
        hasSkeleton(false),
        baseTransform(),
        baseSkelNodes(),
        // Collision shape def. This is set to something non NULL if we have
        // collision.
        collisionShapeDef(NULL),
        
        // Loaded later from isLoaded
        baseTex(NULL),
        normalMapTex(NULL),
        model(NULL),
        skelVerticesLoadJobId(threading::ThreadQueue::INVALID_JOB),
        skelVertices(),
        instanceDataSize(0),
        internalHasLoaded(false),
        
        // Uses and drawing
        uses(0),
        vertexBuffer(0),
        vao(0),
        
        // Managing instances
        instanceInternalIds(),
        instancePerPosition(),
        instanceBufferSize(0),
        instanceBuffer(0) {
        
        // Start loading the xml file.
        firstLoadJobId = queue->add(token, [this](void) {
            loadXmlFile();
        });
    }
    
    Asset::~Asset(void) {
        // Make sure our job is stopped before trying to delete anything.
        queue->deleteToken(token);
        // If we have a vertex buffer AND a skeleton, then WE allocated the vertex buffer, so delete it.
        if (vertexBuffer != 0 && getHasSkeleton()) glDeleteBuffers(1, &vertexBuffer);
        // If we have an instance buffer, delete it.
        if (instanceBuffer != 0) glDeleteBuffers(1, &instanceBuffer);
        // If we created our vertex array object, delete it.
        if (vao != 0) glDeleteVertexArrays(1, &vao);
        // Abandon all models and textures, if they have been loaded.
        if (baseTex != NULL) textureManager->abandonTexture(baseTex);
        if (normalMapTex != NULL) textureManager->abandonTexture(normalMapTex);
        if (model != NULL) modelManager->abandonModel(model);
        delete collisionShapeDef;
    }
    
    void Asset::setupFailed(void) {
        hasSkeleton = false;
        baseTexFilename = TextureManager::NO_TEXTURE_FILENAME;
        normalMapTexFilename = TextureManager::NO_TEXTURE_FILENAME;
        modelFilename = ModelManager::NO_MODEL_FILENAME;
        baseTransform = GLmat4(1.0f); // Set base transform to identity matrix.
        delete collisionShapeDef; // Make sure this is deleted an NULL (disable all collision).
        collisionShapeDef = NULL;
    }
    
    void Asset::loadXmlFile(void) {
        
        // When any XML errors happen, print the error and filename, setup failed, and return.
        #define PRINT_XML_ERROR(xmlErr)                                                                      \
            std::cerr << "WARNING: Failed to load asset XML file [" << filename << "]:\n" << xmlErr << '\n'; \
            setupFailed(); return;
        
        tinyxml2::XMLDocument doc;
        if (doc.LoadFile(filename.c_str()) != tinyxml2::XML_SUCCESS) {
            PRINT_XML_ERROR("Failed to read file: Check that it exists, we have permission to read it, and that it is valid XML.")
        }
        
        // Load elements and stuff from xml file.
        GET_ELEM(assetElem, &doc, "asset")
            GET_ATTRIB(versionValue, assetElem, "version")
            // Check asset XML version.
            if (ASSET_XML_VERSION != versionValue) {
                PRINT_XML_ERROR("Wrong asset XML version, found: " << versionValue << ", required: " << ASSET_XML_VERSION << '.')
            }
            GET_ELEM(modelElem, assetElem, "model")
                GET_ATTRIB(foundModelFilename, modelElem, "filename")
            GET_ELEM(texturesElem, assetElem, "textures")
                GET_ATTRIB(foundBaseTexFilename, texturesElem, "baseTex")
                GET_ATTRIB(foundNormalMapTexFilename, texturesElem, "normalMapTex")
            GET_ELEM(baseTransformElem, assetElem, "baseTransform")
                GLmat4 foundBaseTransform = Util::readTransformation(baseTransformElem);
            
            // Read collision info. Only create a collision shape def if we have a collision element.
            // If the AssetCollisionShapeDef reports an error, complain and give up.
            tinyxml2::XMLElement * collisionElem = assetElem->FirstChildElement("collision");
            if (collisionElem != NULL) {
                bool error = false;
                collisionShapeDef = new AssetCollisionShapeDef(collisionElem, error);
                if (error) {
                    PRINT_XML_ERROR("Failed to load asset collision information.")
                }
            }
        
            // If there is a skeleton, load it.
            tinyxml2::XMLElement * skeletonElem = assetElem->FirstChildElement("skeleton");
            if (skeletonElem == NULL) {
                hasSkeleton = false;
            } else {
                hasSkeleton = true;
                if (!SkelNode::buildSkelNodesFromXML(baseSkelNodes, skeletonElem)) {
                    PRINT_XML_ERROR("Failed to load skeleton.")
                }
            }
                
        // Set things from what we have loaded.
        // Note that we will only get to here if everything loaded successfully.
        baseTexFilename = foundBaseTexFilename;
        normalMapTexFilename = foundNormalMapTexFilename;
        modelFilename = foundModelFilename;
        baseTransform = foundBaseTransform;
    }
    
    void Asset::onLoaded(void) {
        internalHasLoaded = true; // Do this since we finished loading.
        assertBufferSizes();
        copySkeletonToAllInstances();
        setupInstanceDataSize();
        setupVertexBuffer();
        setupInstanceBuffer();
        updateAllInstances();
        createVertexArrayObject();
        setupVertexArrayAttributes(vao, assetManager->getShaderProgram(), vertexBuffer, instanceBuffer, hasSkeleton);
        notifyAllInstancesWeLoaded();
    }
    
    void Asset::copySkeletonToInstance(Instance * instance) {
        if (getHasSkeleton()) {
            instance->getSkelNodes() = baseSkelNodes;
        }
    }
    
    void Asset::copySkeletonToAllInstances(void) {
        if (getHasSkeleton()) {
            for (Instance * instance : instancePerPosition) {
                copySkeletonToInstance(instance);
            }
        }
    }
    
    void Asset::notifyAllInstancesWeLoaded(void) {
        for (Instance * instance : instancePerPosition) {
            instance->onLoaded();
        }
    }
    
    // ------------------------- Buffer management -----------------------------
    
    void Asset::assertBufferSizes(void) {
        // Check max uniform block size.
        GLint actualMaxUniformBlockSize;
        glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &actualMaxUniformBlockSize);
        if ((long long int)actualMaxUniformBlockSize < (long long int)UNIFORM_SIZE) {
            std::cerr << "ERROR: OpenGL implementation only supports uniform block sizes of " << actualMaxUniformBlockSize << " bytes. " << UNIFORM_SIZE << " bytes are required.\n";
            exit(EXIT_FAILURE);
        }
        
        // Check that UNIFORM_SIZE is a multiple of the offset alignment.
        // Otherwise we can't use ranges of that size.
        GLint offsetAlignment;
        glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &offsetAlignment);
        if (offsetAlignment < 0 || (size_t)offsetAlignment > UNIFORM_SIZE || UNIFORM_SIZE % ((size_t)offsetAlignment) != 0) {
            std::cerr << "ERROR: OpenGL uniform block size (" << UNIFORM_SIZE << "), is not a multiple of the uniform buffer offset alignment (" << offsetAlignment << ").\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void * Asset::writeInstance(void * locationData, const Instance * instance) {
        GLmat4 * location = (GLmat4 *)locationData;
        if (getHasSkeleton()) {
            // Write the transformations of each instance's skelnodes, if in skeleton mode.
            // Modify the transformation by our baseTransfrom, then the instance's transformation.
            for (const SkelNode & node : instance->getSkelNodes()) {
                *(location++) = instance->getTransformation() * baseTransform * node.getCalcTransform();
            }
            return location;
        } else {
            // Write the transformation (modified by our own baseTransform) of each instance, if not in skeleton mode.
            *location = instance->getTransformation() * baseTransform;
            return location + 1;
        }
    }
    
    void Asset::setupVertexBuffer(void) {
        if (getHasSkeleton()) {
            // If we do have a skeleton, allocate a buffer, and copy the skeleton vertices into it.
            glGenBuffers(1, &vertexBuffer);
            if (vertexBuffer == 0) { std::cerr << "WARNING: Failed to generate vertex buffer for asset skeleton.\n"; return; }
            glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
            // Allocate memory for the vertex buffer, copy the vertex data into it, unbind it.
            glBufferData(GL_ARRAY_BUFFER, skelVertices.size() * sizeof(SkelVertex), &skelVertices[0], GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        } else {
            // If we don't have a skeleton, just use our model's vertex buffer.
            vertexBuffer = model->getVertexBuffer();
        }
    }
    
    void Asset::setupInstanceDataSize(void) {
        if (getHasSkeleton()) {
            // Make sure we round up to a power of two, so we get a fixed number of instances'
            // skeleton vertices in each UNIFORM_SIZE block.
            instanceDataSize = Util::roundUpToPower2(sizeof(GLmat4) * baseSkelNodes.size());
        } else {
            // In non-skeleton mode, the size of each instance is just the size of a 4x4 matrix.
            // This is used as a vertex attribute, so alignment and powers of two and such
            // doesn't really matter.
            instanceDataSize = sizeof(GLmat4);
        }
    }
    
    void Asset::setupInstanceBuffer(void) {
        // Allocate a buffer for instances.
        glGenBuffers(1, &instanceBuffer);
        if (instanceBuffer == 0) { std::cerr << "WARNING: Failed to generate instance buffer for asset.\n"; return; }
        glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);
        // Work out size for instance buffer, and allocate it. Use GL_STATIC_DRAW because most of the buffer won't change all that often.
        // Make sure we NEVER initialise instanceBufferSize to have zero size - addLastInstance reallocates by doubling the size until it is large
        // enough. Zero doubled is still zero.
        instanceBufferSize = std::max(getInstanceCount(), (size_t)1);
        glBufferData(GL_ARRAY_BUFFER, instanceDataSize * instanceBufferSize, NULL, GL_STATIC_DRAW);
        // Unbind the buffer, now we are done.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    void Asset::addLastInstance(Instance * instance) {
        if (!isLoaded()) return; // If we have not loaded, we cannot do any buffer operations, as our buffer doesn't exist yet.
        
        // If there is not enough room in the buffer...
        if (instanceBufferSize < getInstanceCount()) {
            // Double instance buffer size until it surpasses the number of instances.
            size_t newInstanceBufferSize = instanceBufferSize;
            while(newInstanceBufferSize < getInstanceCount()) {
                newInstanceBufferSize *= 2;
            }
            // Allocate a new buffer.
            GLuint newInstanceBuffer;
            glGenBuffers(1, &newInstanceBuffer);
            glBindBuffer(GL_COPY_WRITE_BUFFER, newInstanceBuffer);
            glBufferData(GL_COPY_WRITE_BUFFER, newInstanceBufferSize * instanceDataSize, NULL, GL_STATIC_DRAW);
            // Copy the old buffer into it. Use the size of the old buffer.
            glBindBuffer(GL_COPY_READ_BUFFER, instanceBuffer);
            glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 0, 0, instanceBufferSize * instanceDataSize);
            // Unbind the buffers.
            glBindBuffer(GL_COPY_WRITE_BUFFER, 0);
            glBindBuffer(GL_COPY_READ_BUFFER, 0);
            
            // Delete the old buffer, and set use of the new buffer.
            glDeleteBuffers(1, &instanceBuffer);
            instanceBuffer = newInstanceBuffer;
            instanceBufferSize = newInstanceBufferSize;
            
            // We reallocated the instance buffer, so the vertex array object attributes need changing.
            setupVertexArrayAttributes(vao, assetManager->getShaderProgram(), vertexBuffer, instanceBuffer, hasSkeleton);
        }
        
        // Then just write the instance's data with updateInstance.
        updateInstance(instance);
    }
    
    void Asset::updateInstance(Instance * instance) {
        if (!isLoaded()) return; // If we have not loaded, we cannot do any buffer operations, as our buffer doesn't exist yet.
        // Bind buffer and map it.
        glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);
        void * mappedBuffer = glMapBufferRange(
            GL_ARRAY_BUFFER,                                     // Target
            instanceDataSize * instanceInternalIds.at(instance), // offset
            instanceDataSize,                                    // length
            GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_RANGE_BIT       // access (we want to write to it, and don't care about what was there before).
        );
        if (mappedBuffer == NULL) { std::cerr << "WARNING: Failed to map instance buffer, when updating asset.\n"; return; }
        // Write the instance's data to the mapped part of the buffer.
        writeInstance(mappedBuffer, instance);
        // Unmap and unbind the buffer, now we are done.
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    void Asset::updateAllInstances(void) {
        if (!isLoaded() || getInstanceCount() == 0) return; // If we have not loaded, we cannot do any buffer operations, as our buffer doesn't exist yet.
                                                            // If we don't have any instances, we cannot update them.
        // Bind buffer and map it.
        glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);
        void * mappedBuffer = glMapBufferRange(
            GL_ARRAY_BUFFER,                               // Target
            0,                                             // offset
            instanceDataSize * getInstanceCount(),         // length
            GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_RANGE_BIT // access (we want to write to it, and don't care about what was there before).
        );
        if (mappedBuffer == NULL) { std::cerr << "WARNING: Failed to map instance buffer, when updating all instances.\n"; return; }
        
        // Copy instance data for each instance.
        for (Instance * instance : instancePerPosition) {
            mappedBuffer = writeInstance(mappedBuffer, instance);
        }
        
        // Unmap and unbind the buffer, now we are done.
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    // ---------------- Vertex array object and attribute management ---------------------
    
    void Asset::createVertexArrayObject(void) {
        glGenVertexArrays(1, &vao);
        if (vao == 0) { std::cerr << "WARNING: Failed to create vertex array object for Asset.\n"; }
    }
    
    void Asset::setupVertexArrayAttributes(GLuint vao, GLuint shaderProgram, GLuint vertexBuffer, GLuint instanceBuffer, bool hasSkeleton) {
        // Bind the vertex array, since we are modifying it.
        glBindVertexArray(vao);
        
        // Attributes used for both skeleton and non-skeleton assets.
        GLuint uvLoc        = glGetAttribLocation(shaderProgram, "inUv"),
               position1Loc = glGetAttribLocation(shaderProgram, "inPosition1"),
               transformLoc = glGetAttribLocation(shaderProgram, "inTransform");
        
        if (hasSkeleton) {
            
            // https://stackoverflow.com/questions/20647207/glsl-replace-large-uniform-int-array-with-buffer-or-texture
            // https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBindBufferRange.xhtml
            // https://community.khronos.org/t/uniform-buffers-and-bindbufferrange/65397
            
            // TODO: Instance buffer in skeleton mode must have at most this many bytes bound at once: GL_MAX_UNIFORM_BLOCK_SIZE.
            // This is guaranteed to be 16KiB so complain if it isn't.
            // When binding ranges, the offset in the buffer must be multiple of GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT.
            // The smallest range we can bind is GL_UNIFORM_BLOCK_SIZE_DATA.
            
            
            // If we do have a skeleton, first get the extra attributes needed.
            GLuint point1Loc    = glGetAttribLocation(shaderProgram, "inPoint1"),
                   point2Loc    = glGetAttribLocation(shaderProgram, "inPoint2"),
                   position2Loc = glGetAttribLocation(shaderProgram, "inPosition2"),
                   mixLoc       = glGetAttribLocation(shaderProgram, "inMix");
            // Then setup the attributes for vertices.
            glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
            glVertexAttribPointer(uvLoc,        2, GL_FLOAT,        GL_FALSE, sizeof(SkelVertex), (void *)offsetof(SkelVertex, uv));
            glVertexAttribIPointer(point1Loc,   1, GL_UNSIGNED_INT,           sizeof(SkelVertex), (void *)offsetof(SkelVertex, skelNodeId1));
            glVertexAttribIPointer(point2Loc,   1, GL_UNSIGNED_INT,           sizeof(SkelVertex), (void *)offsetof(SkelVertex, skelNodeId2));
            glVertexAttribPointer(position1Loc, 3, GL_FLOAT,        GL_FALSE, sizeof(SkelVertex), (void *)offsetof(SkelVertex, position1));
            glVertexAttribPointer(position2Loc, 3, GL_FLOAT,        GL_FALSE, sizeof(SkelVertex), (void *)offsetof(SkelVertex, position2));
            glVertexAttribPointer(mixLoc,       1, GL_FLOAT,        GL_FALSE, sizeof(SkelVertex), (void *)offsetof(SkelVertex, mix));
            glEnableVertexAttribArray(uvLoc);
            glEnableVertexAttribArray(point1Loc);
            glEnableVertexAttribArray(point2Loc);
            glEnableVertexAttribArray(position1Loc);
            glEnableVertexAttribArray(position2Loc);
            glEnableVertexAttribArray(mixLoc);
            
        } else {
            // If we don't have a skeleton, set up attributes for vertices.
            glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
            glVertexAttribPointer(uvLoc,        2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, uv));
            glVertexAttribPointer(position1Loc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)offsetof(Vertex, position));
            glEnableVertexAttribArray(uvLoc);
            glEnableVertexAttribArray(position1Loc);
            // Set up attributes for instances. Note that a matrix requires 4 vertex attributes.
            glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);
            for (unsigned int i = 0; i < 4; i++) {
                glVertexAttribPointer(transformLoc + i, 4, GL_FLOAT, GL_FALSE, sizeof(GLmat4), (void *)(sizeof(GLvec4) * i));
                glVertexAttribDivisor(transformLoc + i, 1);
                glEnableVertexAttribArray(transformLoc + i);
            }
        }
        
        // Unbind the vertex buffer and array at the end.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }
    
    // ---------------------- Instance management ------------------------------

    void Asset::registerInstance(Instance * instance) {
        // Check whether we are loaded BEFORE adding the instance to internal
        // data structures. Otherwise, we would add to internal data structures,
        // call isLoaded (which usually runs the instance's onLoaded), then
        // end up running the instance's onLoaded *again*.
        bool areWeLoaded = isLoaded();
        
        instanceInternalIds[instance] = instancePerPosition.size();
        instancePerPosition.push_back(instance);
        addLastInstance(instance); // Add it to the buffer, as the last instance.
        // If we have loaded, copy our skeleton, and notify the instance that we have loaded.
        if (areWeLoaded) {
            copySkeletonToInstance(instance);
            instance->onLoaded();
        }
    }
    
    void Asset::unregisterInstance(Instance * instance) {
        size_t position = instanceInternalIds.at(instance),
                 endPos = instancePerPosition.size() - 1;
        if (position != endPos) {
            Instance * lastInstance = instancePerPosition[endPos];
            instanceInternalIds[lastInstance] = position;
            instancePerPosition[position] = lastInstance;
            updateInstance(lastInstance); // Update lastInstance since it has moved places in the buffer.
        }
        instancePerPosition.pop_back();
        instanceInternalIds.erase(instance);
    }

    // -------------------- Public API -----------------------------------------
    
    bool Asset::isLoaded(void) {
        // If we have already reported that we have loaded, return true.
        if (internalHasLoaded) {
            return true;
        }
        
        if (queue->getJobStatus(firstLoadJobId) == threading::JobStatus::complete) {
            // If loading the XML file has completed, baseTexFilename, normalMapTexFilename and modelFilename will be defined.
            // If we have not done so already, request a baseTex, normalMap and model.
            if (baseTex == NULL) {
                // Request the model first, so we can display model before texture is loaded.
                model = modelManager->requestModel(modelFilename);
                baseTex = textureManager->requestTexture(baseTexFilename);
                normalMapTex = textureManager->requestTexture(normalMapTexFilename);
            }
            
            // If the model has not loaded yet, report that *we* have not yet loaded.
            if (!model->isLoaded()) return false;
            
            // If we have a skeleton, we need to launch a job to build our skelVertices.
            if (hasSkeleton) {
                
                // If job not started, start it.
                if (skelVerticesLoadJobId == threading::ThreadQueue::INVALID_JOB) {
                    // Make a copy of the model's vertices, since we don't want to access the vertices
                    // asynchronously from another thread. If the model is deleted before this job ends,
                    // that would be undefined.
                    skelVerticesLoadJobId = queue->add(token, [this, vertices = std::vector<Vertex>(model->getVertices())](void) {
                        SkelNode::convertToSkelNodeVertices(skelVertices, vertices, baseSkelNodes);
                    });
                    return false;
                
                // Otherwise, report finish if that job has finished, and return false otherwise.
                } else if (queue->getJobStatus(skelVerticesLoadJobId) == threading::JobStatus::complete) {
                    onLoaded(); // Loading has finished, so run this. This will set internalHasLoaded to true.
                    return true;
                } else {
                    return false;
                }
                
            // If we don't have a skeleton, at this point we can immediately
            // report as loaded, since the model has loaded. Ignore the textures: they do not matter.
            } else {
                onLoaded(); // Loading has finished, so run this. This will set internalHasLoaded to true.
                return true;
            }
            
        // If loading the XML file is ongoing, always return false.
        } else {
            return false;
        }
    }
    
    void Asset::waitForLoad(void) {
        // Wait for first load job (load XML) to complete.
        queue->waitForJobToComplete(firstLoadJobId);
        // Run isLoaded so the model and textures are requested.
        isLoaded();
        // Now the model is guaranteed to exist, wait for it to load.
        model->waitForLoad();
        // isLoaded should return true if we have no skeleton here, or if we were
        // already loaded when this method was called.
        if (isLoaded()) return;
        // The only way that we are not loaded by now, is if we have a skeleton,
        // and the skeleton vertices load job has not finished. So wait for that to finish.
        queue->waitForJobToComplete(skelVerticesLoadJobId);
        // Run isLoaded just so internalHasLoaded gets set.
        isLoaded();
        // Now the skeleton vertices load job has finished, we MUST have finished loading.
    }
    
    void Asset::draw(void) {
        if (isLoaded() && getInstanceCount() > 0) {
            
            // Regardless of whether drawing skeleton or not, bind the vertex array.
            glBindVertexArray(vao);
            
            // Set texture uniforms
            glUniform1ui(assetManager->getTextureUniformLocation(), awe::TextureManager::getTextureLayer(baseTex));
            glUniform1ui(assetManager->getNormalMapUniformLocation(), awe::TextureManager::getTextureLayer(normalMapTex));
            
            if (getHasSkeleton()) {
                
                // Set appropriate number of skeleton nodes.
                glUniform1ui(assetManager->getSkeletonNodeCountUniformLocation(), baseSkelNodes.size());
                
                // If we only have one buffer bindings worth of instances, just bind buffer and do all possible drawing.
                if (instanceBufferSize * instanceDataSize <= UNIFORM_SIZE) {
                    
                    glBindBufferRange(GL_UNIFORM_BUFFER, AssetManager::BufferBindings::skeletonArray, instanceBuffer, 0, instanceBufferSize * instanceDataSize);
                    glDrawArraysInstanced(GL_TRIANGLES, 0, model->getVertexCount(), getInstanceCount());
                    
                // If we have multiple buffer bindings worth of instances...
                } else {
                    
                    // Work out number of instances that we can draw per buffer, and
                    // use instancesRemaining as a count of how many left to draw.
                    unsigned int instancesPerBuffer = UNIFORM_SIZE / instanceDataSize,
                                 bufferId = 0;
                    size_t instancesRemaining = getInstanceCount();
                    // Keep drawing at most a buffer's worth of instances, until we run out of instances.
                    while(instancesRemaining > 0) {
                        // Work out number of instances to draw this time. This may be fewer thant the maximum on the last iteration.
                        size_t thisInstanceCount = std::min(instancesRemaining, (size_t)instancesPerBuffer);
                        instancesRemaining -= thisInstanceCount;
                        // Bind buffer and do drawing.
                        glBindBufferRange(GL_UNIFORM_BUFFER, AssetManager::BufferBindings::skeletonArray, instanceBuffer, bufferId * UNIFORM_SIZE, thisInstanceCount * instanceDataSize);
                        glDrawArraysInstanced(GL_TRIANGLES, 0, model->getVertexCount(), thisInstanceCount);
                        bufferId++;
                    }
                }
                
            } else {
                // Set 0 skeleton nodes: Disable skeleton. Then just do drawing.
                glUniform1ui(assetManager->getSkeletonNodeCountUniformLocation(), 0);
                glDrawArraysInstanced(GL_TRIANGLES, 0, model->getVertexCount(), getInstanceCount());
            }
            
            glBindVertexArray(0);
        }
    }
    
    void Asset::drawCustomVAO(size_t count) {
        if (isLoaded()) {
            
            // Set texture uniforms
            glUniform1ui(assetManager->getTextureUniformLocation(), awe::TextureManager::getTextureLayer(baseTex));
            glUniform1ui(assetManager->getNormalMapUniformLocation(), awe::TextureManager::getTextureLayer(normalMapTex));
            
            // Set 0 skeleton nodes: Disable skeleton. Then just do drawing.
            glUniform1ui(assetManager->getSkeletonNodeCountUniformLocation(), 0);
            glDrawArraysInstanced(GL_TRIANGLES, 0, model->getVertexCount(), count);
        }
    }
    
    bool Asset::isCollisionStatic(void) const {
        return collisionShapeDef->isCollisionStatic();
    }
    
    bool Asset::doesRequireCompoundShape(void) const {
        return collisionShapeDef->doesRequireCompoundShape();
    }
}
