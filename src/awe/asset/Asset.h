/*
 * Asset.h
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_ASSET_ASSET_H_
#define AWE_ASSET_ASSET_H_

#include <unordered_map>
#include <cstdint>
#include <cstddef>
#include <cstdint>
#include <vector>
#include <string>

#include "../util/geometry/SkelNode.h"
#include "../util/geometry/Vertex.h"
#include "../util/types/Types.h"

namespace threading {
    class ThreadQueue;
}

namespace awe {
    class AssetCollisionShapeDef;
    class TextureManager;
    class AssetManager;
    class ModelManager;
    class Instance;
    class Model;

    /**
     *
     */
    class Asset {
    public:
        const static std::string ASSET_XML_VERSION;
        
        /**
         * This is the maximum uniform size that we should have bound at any one time.
         * This must be smaller or equal to GL_UNIFORM_BLOCK_SIZE_DATA.
         * OpenGL guarantees that GL_UNIFORM_BLOCK_SIZE_DATA is at least 16KiB.
         * By default, we have this value set to 16384 to support all hardware.
         */
        const static size_t UNIFORM_SIZE;
        
        
    private:
        friend class AssetManager;
    
        // Set initially.
        threading::ThreadQueue * queue;
        void * token;
        TextureManager * textureManager;
        ModelManager * modelManager;
        AssetManager * assetManager;
        std::string filename;
        
        // Loading first XML info
        uint64_t firstLoadJobId;
        // Properties
        std::string baseTexFilename,
                    normalMapTexFilename,
                    modelFilename;
        bool hasSkeleton;
        GLmat4 baseTransform;
        std::vector<SkelNode> baseSkelNodes;
        // Collision info (also loaded from XML). If we have a collision, this
        // is set to something non NULL.
        AssetCollisionShapeDef * collisionShapeDef;
        
        // Set up later from isLoaded.
        void * baseTex,
             * normalMapTex;
        Model * model;
        uint64_t skelVerticesLoadJobId;
        std::vector<SkelVertex> skelVertices;
        size_t instanceDataSize; // The number of bytes that each instance takes up in the instance buffer.
        bool internalHasLoaded;
        
        // Uses and drawing
        size_t uses; // Set by AssetManager.
        GLuint vertexBuffer, // Created by us if we have skeleton, created by our model otherwise.
               vao;          // Our vertex array object, that we can use for drawing ourself.
        
        // Managing instances
        std::unordered_map<Instance *, size_t> instanceInternalIds;
        std::vector<Instance *> instancePerPosition; // The instance at each position.
        size_t instanceBufferSize; // The number of INSTANCES (not bytes) the buffer has space for.
        GLuint instanceBuffer; // Created when we load.
    
        // Don't allow copying: We hold raw resources and perform asynchronous operations.
        Asset(const Asset & other);
        Asset & operator = (const Asset & other);
    
    public:
        /**
         * 
         */
        Asset(threading::ThreadQueue * queue, TextureManager * textureManager, ModelManager * modelManager, AssetManager * assetManager, const std::string & filename);
        virtual ~Asset(void);
    private:
        
        // Initialises baseTexFilename, normalMapTexFilename and modelFilename to default values:
        // The no texture filename, and the no model filename.
        void setupFailed(void);
        
        // After this returns:
        //    hasSkeleton, baseTexFilename, normalMapTexFilename, modelFilename, baseTransform
        // will all have values.
        // If unsuccessful, these will be assigned via setupFailed.
        void loadXmlFile(void);
        
        // This is called when loading completes. Here, any appropriate operations should be
        // run, such as setting internalHasLoaded to true, requesting/setting up vertex/instance buffers.
        void onLoaded(void);
        
        // Copies our default skeleton to the instance, if we have a skeleton. This should be done
        // when we are loaded, and an instance is registered.
        void copySkeletonToInstance(Instance * instance);
        
        // Copies our skeleton to all instances, if we have a skeleton.
        // This should be done when loading completes.
        void copySkeletonToAllInstances(void);
        
        // Runs the onLoaded method for all of our existing instances.
        void notifyAllInstancesWeLoaded(void);
        
        // -------------- Buffer management -------------
        
        // Checks that the OpenGL implementation supports appropriate buffer sizes. Complains and
        // exits otherwise.
        void assertBufferSizes(void);
        
        // Writes the instance data to the specified location. This is necessary for setting up the
        // vertex buffer, or updating an instance. The location should be a mapped buffer.
        // Returns the position after this instance was written.
        void * writeInstance(void * location, const Instance * instance);
        
        // If we don't have a skeleton, request it from our model. If we *do*
        // have a skeleton, create a buffer for ourself.
        void setupVertexBuffer(void);
        
        // Sets instanceDataSize to the number of bytes that each instance takes
        // up in the instance buffer. This will be different depending on
        // whether we have a skeleton or not.
        void setupInstanceDataSize(void);
        
        // Initialises the instance buffer. This should be run in isLoaded,
        // as soon as loading finishes. Note that instance data is NOT copied
        // into the buffer. That must be done by calling updateAllInstances.
        void setupInstanceBuffer(void);
        
        // Adds the specified instance, as the last instance in the buffer.
        // This must be called AFTER the instance is added to our data structures.
        // Note that there is no need for a corresponding remove operation, since
        // why make the instance buffer smaller again, when something else will likely
        // need it again in the future.
        void addLastInstance(Instance * instance);
        
        // ---------------- Vertex array object and attribute management ---------------------
        
        // Creates our internal vertex array object.
        void createVertexArrayObject(void);
        
    public:
    
        // ----------------------Static utility methods ------------------------
        
        /**
         * Sets up the vertex array object's attributes, using the provided
         * instance buffer and vertex buffer. If hasSkeleton is set to true,
         * the vertex buffer is not used as it is assumed that a uniform buffer
         * is used instead.
         * A shader program is required for attribute locations.
         * This is convenient to use for the drawCustomVAO method, (which does
         * not support skeletons).
         * This must be run from an OpenGL context.
         */
        static void setupVertexArrayAttributes(GLuint vao, GLuint shaderProgram, GLuint vertexBuffer, GLuint instanceBuffer, bool hasSkeleton = false);
        
        
        // ---------------------- Instance management --------------------------
    
        /**
         * Should be called to register an instance.
         * This should be called by Instance in it's constructor.
         * This MUST be called from an OpenGL context.
         * The asset does not need to be loaded to run this.
         */
        void registerInstance(Instance * instance);
        
        /**
         * Should be called to unregister an instance.
         * This should be called by Instance in it's destructor.
         * This MUST be called from an OpenGL context.
         * The asset does not need to be loaded to run this.
         */
        void unregisterInstance(Instance * instance);
        
        /**
         * Updates the data stored in the instance buffer about the specified
         * instance. This should be called by the instance whenever it's
         * transformation or skeleton is updated.
         * This MUST be called from an OpenGL context.
         * If we (the asset) are not loaded yet, this does nothing.
         */
        void updateInstance(Instance * instance);
        
        /**
         * This acts as if updateInstance has been performed for all of our
         * instances. This is SIGNIFICANTLY faster than calling it for
         * all instances individually, as this copies over instance data with
         * one buffer mapping.
         * If we (the asset) are not loaded yet, this does nothing.
         * This is automatically called when the asset loads initially.
         */
        void updateAllInstances(void);
        
        
        // -------------------- Public API -------------------------------------
    
        /**
         * Returns true if the asset (and associated model if applicable), has
         * finished loading, and false otherwise.
         * NOTE: If the asset has a skeleton, this will NEVER return true until
         *       either isLoaded has been called at least once already, or
         *       waitForLoad has been called.
         * NOTE: This MUST be called from an OpenGL context.
         */
        bool isLoaded(void);
        
        /**
         * Blocks until the asset has finished loading. If the asset has already
         * loaded, this does nothing.
         * After this call, isLoaded will always return true, and it will always
         * be safe to access the asset.
         */
        void waitForLoad(void);
        
        /**
         * Draws all registered instances of this asset.
         * This assumes that the shader object and texture is bound, and the
         * view uniform is set.
         */
        void draw(void);
        
        /**
         * Draws this asset using a custom VAO (which is already assumed to
         * be bound), count times, implicitly using the vertex and instance
         * buffers assigned to it. Note that this does not support skeletons,
         * and must not be used for skeleton assets. It is convenient to set up
         * the VAO using our setupVertexArrayAttributes static utility method.
         * This assumes that the shader object and texture is bound, and the
         * view uniform is set.
         */
        void drawCustomVAO(size_t count);
        
        // ---------------------- Accessor methods -----------------------------
        
        /**
         * Returns the number of instances that we currently have registered.
         * Each time draw is called, this many instances will get drawn.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline size_t getInstanceCount(void) const {
            return instanceInternalIds.size();
        }
        
        /**
         * Allows access to the filename from which this model was created.
         */
        inline const std::string & getFilename(void) const {
            return filename;
        }
        
        /**
         * Returns true if this asset has a loaded skeleton.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline bool getHasSkeleton(void) const {
            return hasSkeleton;
        }
        
        /**
         * Accesses our base transformation. This is the transformation applied
         * after the transformation of each instance (relative transformation
         * for drawing model).
         */
        inline const GLmat4 & getBaseTransform(void) const {
            return baseTransform;
        }
        
        /**
         * Returns true if this asset is indented to have (static or dynamic)
         * collision.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline bool getHasCollision(void) const {
            return collisionShapeDef != NULL;
        }
        
        /**
         * Allows access to our collision shape def. This is NULL unless we
         * have collision (getHasCollision() is true).
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline const AssetCollisionShapeDef * getCollisionShapeDef(void) const {
            return collisionShapeDef;
        }
        
        /**
         * Returns true if this asset is intended to have a static collision.
         * Note: The result of this call is not defined until isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        bool isCollisionStatic(void) const;
        
        /**
         * Returns true if this asset should have a compound collision shape.
         * This is true whenever we have more than one collision box, or the
         * user has explicitly stated (in the XML file) that a compound
         * collision shape should be used. Where a compound collision shape
         * is used with a single collision box, it adds the possibility of a
         * custom centre of mass: bullet physics always considers the origin
         * coordinate of a shape to be its centre of mass.
         * Note: The result of this call is not defined until isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        bool doesRequireCompoundShape(void) const;
        
        /**
         * Accesses our base texture token.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline void * getBaseTex(void) const {
            return baseTex;
        }
        
        /**
         * Accesses our normal map texture token.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline void * getNormalMapTex(void) const {
            return normalMapTex;
        }
        
        /**
         * Accesses our model.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline Model * getModel(void) const {
            return model;
        }
        
        /**
         * Accesses our internal vertex buffer. Note that for non-skeletal
         * assets, this buffer is the same as the model's vertex buffer.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline GLuint getVertexBuffer(void) const {
            return vertexBuffer;
        }
        
        /**
         * Accesses our internal instance buffer. This is used as a vertex
         * array attribute for non-skeletal assets, and as a uniform buffer
         * for skeletal assets.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline GLuint getInstanceBuffer(void) const {
            return instanceBuffer;
        }
    };
}

#endif
