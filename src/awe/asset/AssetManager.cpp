/*
 * AssetManager.cpp
 *
 *  Created on: 16 Jan 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "AssetManager.h"

#include "../util/Util.h"
#include "Asset.h"

namespace awe {
    
    const std::string AssetManager::NO_ASSET_FILENAME = "noAsset.xml";
    
    AssetManager::AssetManager(threading::ThreadQueue * queue, TextureManager * textureManager, ModelManager * modelManager, GLuint shaderProgram) :
        queue(queue),
        textureManager(textureManager),
        modelManager(modelManager),
        
        assets(),
        disusedAssets(),
        assetsStillLoading(),
        
        shaderProgram(shaderProgram),
        textureUniformLocation(Util::getUniformLocationChecked(shaderProgram, "AssetManager constructor", "baseTex")),
        normalMapUniformLocation(Util::getUniformLocationChecked(shaderProgram, "AssetManager constructor", "normalMap")),
        skeletonNodeCountUniformLocation(Util::getUniformLocationChecked(shaderProgram, "AssetManager constructor", "skeletonNodeCount")) {
            
        // Set up uniform buffer bindings. Make sure the shader program is bound for this operation.
        glUseProgram(shaderProgram);
        GLuint skeletonArrayIndex = Util::getUniformBlockIndexChecked(shaderProgram, "AssetManager constructor", "skeletonArrayBlock");
        glUniformBlockBinding(shaderProgram, skeletonArrayIndex, BufferBindings::skeletonArray);
        glUseProgram(0);
    }
    
    AssetManager::~AssetManager(void) {
        for (const std::pair<std::string, Asset *> & pair : assets) {
            delete pair.second;
        }
    }
    
    Asset * AssetManager::createAsset(const std::string & filename) {
        Asset * asset = new Asset(queue, textureManager, modelManager, this, filename);
        assets[filename] = asset;
        assetsStillLoading.insert(asset);
        return asset;
    }
    
    void AssetManager::removeAsset(Asset * asset) {
        assets.erase(asset->getFilename());
        assetsStillLoading.erase(asset);
        delete asset;
    }
    
    void AssetManager::removeDisusedAssets(void) {
        for (Asset * asset : disusedAssets) {
            removeAsset(asset);
        }
        disusedAssets.clear();
    }
    
    // This is the number of assets to iterate (maximum) in updateLoadAndProgress.
    // This stops updateLoadAndProgress taking excessively long, when we are loading
    // a very very large number of assets, (which may happen).
    #define MAX_LOAD_QUERY 64
    
    unsigned int AssetManager::updateLoadAndProgress(void) {
        // Iterate through MAX_LOAD_QUERY first unloaded assets, remove
        // from assetsStillLoading any which finish.
        std::vector<Asset *> noLongerLoading;
        unsigned int i = 0;
        for (Asset * asset : assetsStillLoading) {
            if ((i++) > MAX_LOAD_QUERY) break;
            if (asset->isLoaded()) {
                noLongerLoading.push_back(asset);
            }
        }
        for (Asset * asset : noLongerLoading) {
            assetsStillLoading.erase(asset);
        }
        // Number left to load is number of assets left in assetsStillLoading.
        return assetsStillLoading.size();
    }
    
    Asset * AssetManager::requestAsset(const std::string & filename) {
        auto iter = assets.find(filename);
        if (iter == assets.end()) {
            // If the asset doesn't exist, create it and increment it's uses.
            Asset * asset = createAsset(filename);
            asset->uses++;
            return asset;
        } else {
            // If the asset already exists, make sure it is no longer disused,
            // increment it's uses and return it.
            disusedAssets.erase(iter->second);
            iter->second->uses++;
            return iter->second;
        }
    }
    
    void AssetManager::abandonAsset(Asset * asset) {
        asset->uses--;
        if (asset->uses == 0) {
            disusedAssets.insert(asset);
        }
    }
    
    unsigned int AssetManager::step(unsigned int maxTime) {
        removeDisusedAssets();
        
        return updateLoadAndProgress();
    }
    
    void AssetManager::waitForLoad(void) {
        for (const std::pair<std::string, Asset *> & pair : assets) {
            pair.second->waitForLoad();
        }
        assetsStillLoading.clear();
    }
    
    void AssetManager::draw(void) const {
        for (const std::pair<std::string, Asset *> & pair : assets) {
            pair.second->draw();
        }
    }
}
