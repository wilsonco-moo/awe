/*
 * AssetManager.h
 *
 *  Created on: 16 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_ASSET_ASSETMANAGER_H_
#define AWE_ASSET_ASSETMANAGER_H_

#include <unordered_set>
#include <unordered_map>
#include <GL/gl.h>
#include <string>

namespace threading {
    class ThreadQueue;
}

namespace awe {

    class TextureManager;
    class ModelManager;
    class Asset;
    
    /**
     * AssetManager allows assets to be conveniently loaded from XML files,
     * without creating duplicate assets when the same file is loaded multiple
     * times. AssetManager also handles allocation of these assets, so that when
     * all uses of a particular asset file have been abandoned, the asset is
     * removed from memory.
     * 
     * NOTE: Access to this class is not thread safe. Access should either be
     *       synchronised outside this class, or done from only one thread.
     */
    class AssetManager {
    public:
        /**
         * This should be used in the absence of any sensible asset to load.
         * It is assumed that this will always cause the loading of the
         * "no asset" asset, (consisting of default "no model" model, default
         * "no texture" texture, and no skeleton).
         */
        const static std::string NO_ASSET_FILENAME;
        
        /**
         * The bindings for uniform buffers.
         */
        class BufferBindings {
        public:
            enum {
                skeletonArray,
                lightSources,
                lightGrid
            };
        };
        
    private:
        threading::ThreadQueue * queue;
        TextureManager * textureManager;
        ModelManager * modelManager;
        
        std::unordered_map<std::string, Asset *> assets;
        std::unordered_set<Asset *> disusedAssets,
                                    assetsStillLoading;
        GLuint shaderProgram,
               textureUniformLocation,
               normalMapUniformLocation,
               skeletonNodeCountUniformLocation;
        
        // Don't allow copying: We hold raw resources.
        AssetManager(const AssetManager & other);
        AssetManager & operator = (const AssetManager & other);

    public:
        /**
         * A ThreadQueue is required for asynchronous loading operations.
         */
        AssetManager(threading::ThreadQueue * queue, TextureManager * textureManager, ModelManager * modelManager, GLuint shaderProgram);
        virtual ~AssetManager(void);
        
    private:
        
        // Adds and removes assets from our internal data structures respectively.
        // Note: Uses are not incremented by creating assets with createAsset.
        Asset * createAsset(const std::string & filename);
        void removeAsset(Asset * asset);
        
        // Removes all disused assets. After this call, disusedAssets will be empty.
        void removeDisusedAssets(void);
        
        // Makes sure isLoaded gets called on the assets which are still loading,
        // to progress their load, returning number left loading.
        unsigned int updateLoadAndProgress(void);
        
    public:
    
        // ------------------------ Public API ---------------------------------
        
        /**
         * Requests the load of an asset, from the specified filename.
         * The asset will be created immediately, (if it doesn't already exist),
         * but may not contain useful data. Asset loading is done asynchronously
         * so the Asset's data will not be available until it's isLoaded()
         * method returns true.
         */
        Asset * requestAsset(const std::string & filename);
        
        /**
         * Abandons the use of the asset. This MUST only be called ONCE per
         * call to requestAsset. Once this has been called, the specified
         * asset must NEVER be used again.
         * Once all uses of an asset have been abandoned, the asset will be
         * removed from memory next time step or waitForLoad is called.
         */
        void abandonAsset(Asset * asset);
        
        /**
         * This should be called each step. This manages asynchronous loading
         * and unloading of assets.
         * Returned is an estimate of the number of pending load operations.
         * The operation will attempt to avoid exceeding the provided maxTime,
         * which should be in milliseconds.
         */
        unsigned int step(unsigned int maxTime);
        
        /**
         * Returns true if there are currently any background operations going
         * on to load models.
         */
        inline bool isLoadOngoing(void) const {
            return true;
        }
        
        /**
         * Blocks until all pending load operations have finished.
         * After this call, isLoadOngoing will always return false, and it will
         * be safe to access all assets loaded with this AssetManager.
         * 
         * NOTE: This is slow, and in a frame based application (like a game), a
         *       poor way to wait for assets to load. A better way would be to
         *       call step() each frame, displaying the returned value from
         *       step() as a progress bar, and stop when that returns zero.
         * 
         * In something like a command-line application waitForLoad is fine to
         * use.
         */
        void waitForLoad(void);
        
        /**
         * Returns the shader program that we were assigned at the start.
         */
        inline GLuint getShaderProgram(void) const {
            return shaderProgram;
        }
        
        inline GLuint getTextureUniformLocation(void) const {
            return textureUniformLocation;
        }
        
        inline GLuint getNormalMapUniformLocation(void) const {
            return normalMapUniformLocation;
        }
        
        inline GLuint getSkeletonNodeCountUniformLocation(void) const {
            return skeletonNodeCountUniformLocation;
        }
        
        /**
         * Calls the draw method for each of our assets.
         * This has the effect of drawing all instances, in all assets.
         * This assumes that the shader object and texture is bound, and the
         * view uniform is set.
         */
        void draw(void) const;
    };
}

#endif
