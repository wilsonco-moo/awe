#define SHADER_VERSION #version 330 core
SHADER_VERSION
#undef SHADER_VERSION

#include "../util/types/SizeDef.h"

/**
 * The number of mat4's in the skeleton array.
 */
#define SKELETON_ARRAY_SIZE (SIZEOF_MAX_UNIFORM / SIZEOF_MAT4)


// ------------------------- Inputs (vertex attributes) ------------------------

// NOTE:
//     Attributes which are ALWAYS ENABLED must be first in the layout. This is
//     due to a bug in AMD drivers in Windows, where everything fails if vertex
//     attribute zero is disabled.
// Reference: https://community.khronos.org/t/behavior-of-a-non-enabled-vertex-attribute/69746
// Reference: https://stackoverflow.com/a/13349282
//            (http://web.archive.org/web/20150512182351/https://stackoverflow.com/questions/13348885/why-does-opengl-drawing-fail-when-vertex-attrib-array-zero-is-disabled)

// Used in non-skeletal assets.
layout(location = 0) in vec2 inUv;        // (ALWAYS ENABLED)
layout(location = 1) in vec3 inPosition1; // (ALWAYS ENABLED)
layout(location = 2) in mat4 inTransform; // (Enabled only for NON-SKELETAL assets).
// Used in skeletal assets only.
layout(location = 6) in uint inPoint1;    // (Enabled only for SKELETAL).
layout(location = 7) in uint inPoint2;    // (Enabled only for SKELETAL).
layout(location = 8) in vec3 inPosition2; // (Enabled only for SKELETAL).
layout(location = 9) in float inMix;      // (Enabled only for SKELETAL).


// ------------------------ Outputs (to fragment shader) -----------------------
out vec2 vfUv;
out vec3 vfWorldPosition;
out mat4 vfNormalTransform;

// Uniforms
uniform mat4 viewProjectionTransform;
uniform uint skeletonNodeCount;

// The array (and block) for skeleton data.
// Layout is fine here, since each element is a mat4, which has a size which
// is a multiple of a vec4.
layout (std140) uniform skeletonArrayBlock {
    mat4 skeletonArray[SKELETON_ARRAY_SIZE];
};

mat4 mixMatrices(mat4 a, mat4 b, float t) {
	return a + (b - a) * t;
}


void main(void) {
    
    if (skeletonNodeCount >= 2u) {
        // Get the offset within the skeletonArray which our skeleton's nodes are stored.
        uint offset = uint(gl_InstanceID) * skeletonNodeCount;
        // Work out the two transformation matrices.
        mat4 transform1 = skeletonArray[offset + inPoint1];
        mat4 transform2 = skeletonArray[offset + inPoint2];
        // Work out the two positions, transformed by the appropriate skeleton transformations.
        vec4 pos1 = transform1 * vec4(inPosition1, 1.0f);
        vec4 pos2 = transform2 * vec4(inPosition2, 1.0f);
        // Mix the two positions by the mix, and transform by view projection.
        vec4 worldPos = mix(pos1, pos2, inMix);
        gl_Position = viewProjectionTransform * worldPos;
        vfWorldPosition = worldPos.xyz;
        vfNormalTransform = mixMatrices(transform1, transform2, inMix);
        
    } else {
        // Simply transform the in position, by that instance's transform, and the view projection.
        vec4 worldPos = inTransform * vec4(inPosition1, 1.0f);
        gl_Position = viewProjectionTransform * worldPos;
        vfWorldPosition = worldPos.xyz;
        vfNormalTransform = inTransform;
    }
    vfUv = inUv;
}
