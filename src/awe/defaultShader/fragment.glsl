#define SHADER_VERSION #version 330 core
SHADER_VERSION
#undef SHADER_VERSION

#include "../lighting/LightingShader.glh"

// Inputs (from vertex shader)
in vec2 vfUv;
in vec3 vfWorldPosition;
in mat4 vfNormalTransform;

// Outputs
out vec4 outColour;

// Texture
uniform sampler2DArray tex;

// Uniforms for texture array IDs.
uniform uint baseTex;
uniform uint normalMap;

void main(void) {
    // Get the normal from the normal map, then transform it by the normal transform.
    vec3 normal = texture(tex, vec3(vfUv, normalMap)).rgb - 0.5f;
    normal = normalize((vfNormalTransform * vec4(normal, 0.0f)).xyz);
    
    // Use this normal to work out total incident light.
    vec3 incidentLight = lighting_getTotal(vfWorldPosition, normal);
    
    // Multiply texture by this incident light, and clamp between 0 and 1.
    outColour = clamp(texture(tex, vec3(vfUv, baseTex)) * vec4(incidentLight, 1.0f), 0.0f, 1.0f);
}
