/*  
 * InstanceFollowCamera.cpp
 *
 *  Created on: 23 Jul 2020
 *      Author: wilson
 */

#include "InstanceFollowCamera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <GL/freeglut.h>
#include <cstddef>
#include <cmath>

#include "../instance/Instance.h"

namespace awe {

    InstanceFollowCamera::InstanceFollowCamera(Instance * instance, GLfloat initialHDir, GLfloat initialVDir) :
        BaseCamera(instance == NULL ? GLvec3(0,0,0) : instance->calculatePosition(), initialHDir, initialVDir),
        instance(instance) {
    }
    
    InstanceFollowCamera::~InstanceFollowCamera(void) {
    }
    
    void InstanceFollowCamera::updatePosition(GLfloat elapsed) {
        if (instance == NULL) {
            position = GLvec3(0,0,0);
        } else {
            position = instance->calculatePosition();
        }
        if (zoomingIn) {
            addCameraDistanceExp(-zoomSpeed * elapsed);
        }
        if (zoomingOut) {
            addCameraDistanceExp(zoomSpeed * elapsed);
        }
    }
    
    GLvec3 InstanceFollowCamera::calculateEyepoint(void) const {
        return GLvec3(position.x - cos(hDir) * cos(vDir) * cameraDistance,
                      position.y + sin(hDir) * cos(vDir) * cameraDistance,
                      position.z + sin(vDir) * cameraDistance);
    }
    
    GLmat4 InstanceFollowCamera::getViewTransform(void) const {
        return glm::lookAt(
            GLvec3(position.x - cos(hDir) * cos(vDir) * cameraDistance, position.y + sin(hDir) * cos(vDir) * cameraDistance, position.z + sin(vDir) * cameraDistance),
            GLvec3(position.x, position.y, position.z),
            GLvec3(sin(vDir) * cos(hDir), -sin(vDir) * sin(hDir), cos(vDir))
        );
    }
}
