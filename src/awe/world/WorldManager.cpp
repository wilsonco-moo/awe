/*
 * WorldManager.cpp
 *
 *  Created on: 24 Jan 2020
 *      Author: wilson
 */

#include "WorldManager.h"

#include <threading/ThreadQueue.h>
#include <tinyxml2/tinyxml2.h>
#include <functional>
#include <iostream>
#include <cstddef>

#include "../lighting/LightingManager.h"
#include "../instance/Instance.h"
#include "../util/Util.h"

namespace awe {
    
    const std::string WorldManager::WORLD_XML_VERSION = "0.1";
    
    // When any XML errors happen, print the error and filename, setup failed, and return.
    #define PRINT_XML_ERROR(xmlErr)                                                           \
        std::cerr << "WARNING: WorldManager: Failed to load world file:\n" << xmlErr << '\n'; \
        return;
    
    WorldManager::WorldManager(threading::ThreadQueue * threadQueue, AssetManager * assetManager, LightingManager * lightingManager, PhysicsWorld * physicsWorld) :
        threadQueue(threadQueue),
        token(threadQueue->createToken()),
        assetManager(assetManager),
        lightingManager(lightingManager),
        physicsWorld(physicsWorld),
        instances(),
        lights(),
        isLoading(false),
        isSaving(false),
        loadedDocument(NULL),
        loadJob(threading::ThreadQueue::INVALID_JOB),
        saveJob(threading::ThreadQueue::INVALID_JOB) {
    }
    
    WorldManager::WorldManager(threading::ThreadQueue * threadQueue, AssetManager * assetManager, LightingManager * lightingManager, PhysicsWorld * physicsWorld, const std::string & filename) :
        WorldManager(threadQueue, assetManager, lightingManager, physicsWorld) {
        loadFromFile(filename);
    }
    
    WorldManager::~WorldManager(void) {
        // Delete our thread queue token (so make sure any background operations have stopped), before doing anything else.
        threadQueue->deleteToken(token);
        // This may have not been deleted yet.
        delete loadedDocument;
        // Get rid of all assets and lights.
        eraseAllAssetsAndLights();
    }
    
    void WorldManager::eraseAllAssetsAndLights(void) {
        for (std::pair<const std::string, std::unordered_map<std::string, Instance *>> & asset : instances) {
            for (std::pair<const std::string, Instance *> & instance : asset.second) {
                delete instance.second;
            }
        }
        instances.clear();
        for (std::pair<const std::string, Light *> & light : lights) {
            delete light.second;
        }
        lights.clear();
    }
    
    void WorldManager::addInstanceFromXML(tinyxml2::XMLElement * instanceElem, const GLmat4 & adjustedTransform, const std::string & suffix, const std::string & assetName) {
        // Read attributes for each instance.
        GET_ATTRIB_STDSTRING(instanceNameAttr, instanceElem, "name")
        GLmat4 baseTransformAttr = Util::readTransformation(instanceElem);
        
        // Transform the instance's baseTransform by the provided adjusted transform.
        baseTransformAttr = adjustedTransform * baseTransformAttr;
        
        // Add the name suffix.
        instanceNameAttr += suffix;
        
        addInstance(assetName, instanceNameAttr, baseTransformAttr);
    }
    
    void WorldManager::addLightFromXML(tinyxml2::XMLElement * lightElem, const GLmat4 & adjustedTransform, const std::string & suffix) {
        // Read properties for each light.
        GET_ATTRIB_STDSTRING(lightNameAttr, lightElem, "name")
        GET_ELEM(positionElem, lightElem, "position")
            GET_ATTRIB_GLFLOAT(positionXAttr, positionElem, "x")
            GET_ATTRIB_GLFLOAT(positionYAttr, positionElem, "y")
            GET_ATTRIB_GLFLOAT(positionZAttr, positionElem, "z")
        GET_ELEM(brightnessElem, lightElem, "brightness")
            GET_ATTRIB_GLFLOAT(brightnessRedAttr,   brightnessElem, "red")
            GET_ATTRIB_GLFLOAT(brightnessGreenAttr, brightnessElem, "green")
            GET_ATTRIB_GLFLOAT(brightnessBlueAttr,  brightnessElem, "blue")
        GET_ELEM(radiusElem, lightElem, "radius")
            GET_ATTRIB_GLFLOAT(radiusAttr, radiusElem, "value")
        GET_ELEM(falloffElem, lightElem, "falloff")
            GET_ATTRIB_GLFLOAT(falloffAttr, falloffElem, "value")
        
        // Transform the position by the provided adjusted transform.
        GLvec3 adjustedPosition = GLvec3(adjustedTransform * GLvec4(positionXAttr, positionYAttr, positionZAttr, 1.0f));
        
        // Add the name suffix.
        lightNameAttr += suffix;
        
        // Add the light source.
        addLight(
            lightNameAttr, 
            adjustedPosition,
            GLvec3(brightnessRedAttr, brightnessGreenAttr, brightnessBlueAttr),
            radiusAttr, falloffAttr
        );
    }
    
    /**
     * For each <array> element within the provided parentElement XMLElement:
     * Extracts the base transform and count specified by the element, and repeats count number of times:
     *  > Recursively calls recurseFunctionName, with the extra arguments, (to recursively interpret the provided parentElement's children,
     *  > Makes sure the suffix gets a "-array0", "-array1", "-array2", ... etc added onto it, to keep names unique.
     *  > Accumulates the transformation specified by the array element onto the provided adjustedTransformation.
     * This has an affect like Blender's array modififier, for any instances contained within the array element.
     */
    #define INTERPRET_ARRAY_ELEM(parentElement, adjustedTransformation, suffixStr, recurseFunctionName, ...)                   \
                                                                                                                               \
        ELEM_CHILDREN_FOR_EACH(arrayElem, parentElement, "array") {                                                            \
            GET_ATTRIB_UINT(countAttr, arrayElem, "count")                                                                     \
            GLmat4 arrayBaseTransformAttr = Util::readTransformation(arrayElem);                                               \
            /* For each element along, accumulate arrayBaseTransform onto adjustedTransformation.                           */ \
            GLmat4 arrayTransformUpto = adjustedTransformation;                                                                \
            for (unsigned long int i = 0; i < countAttr; i++) {                                                                \
                /* Make sure the suffix string has the count appended to it, so names are unique.                           */ \
                recurseFunctionName(arrayElem, arrayTransformUpto, suffixStr + "-array" + std::to_string(i) __VA_ARGS__);      \
                /* Apply array transform, THEN overall adjusted transform.                                                  */ \
                arrayTransformUpto = arrayTransformUpto * arrayBaseTransformAttr;                                              \
            }                                                                                                                  \
        }
    
    /**
     * For each <transform> element within the provided parentElement XMLElement:
     * Extracts the baseTransform specified by the <transform> element, and does the following ONCE:
     *  > Builds a transformation matrix, comprising of:
     *    - the <transform> element's baseTransformation, THEN the provided adjustedTransformation.
     *    I.e: The <transform> element's baseTransformation is always applied first.
     *  > Recursively calls recurseFunctionName, with the extra arguments, (to recursively interpret the provided parentElement's children.
     *  > The suffix is not changed: It does not need to be as we only call this once.
     */
    #define INTERPRET_TRANSFORM_ELEM(parentElement, adjustedTransformation, suffixStr, recurseFunctionName, ...) \
                                                                                                                 \
        ELEM_CHILDREN_FOR_EACH(transformElem, parentElement, "transform") {                                      \
            GLmat4 baseTransformAttr = Util::readTransformation(transformElem);                                  \
            /* Apply transform element's transform, THEN overall adjusted transform.                          */ \
            GLmat4 innerTransform = adjustedTransformation * baseTransformAttr;                                  \
            /* Simply recursively call once, into the transform element.                                      */ \
            recurseFunctionName(transformElem, innerTransform, suffixStr __VA_ARGS__);                           \
        }
        
    void WorldManager::addAllInstancesFromXML(tinyxml2::XMLElement * parentElem, const GLmat4 & adjustedTransform, const std::string & suffix, const std::string & assetName) {
        // Add instances from each <instance> element.
        ELEM_CHILDREN_FOR_EACH(instanceElem, parentElem, "instance") {
            addInstanceFromXML(instanceElem, adjustedTransform, suffix, assetName);
        }
        // Recursively call for each array element, (use the asset name as an extra argument).
        INTERPRET_ARRAY_ELEM(parentElem, adjustedTransform, suffix, addAllInstancesFromXML, , assetName)
        
        // Recursively call for each transform element, (use the asset name as an extra argument).
        INTERPRET_TRANSFORM_ELEM(parentElem, adjustedTransform, suffix, addAllInstancesFromXML, , assetName)
    }
    
    void WorldManager::addAllLightsFromXML(tinyxml2::XMLElement * parentElem, const GLmat4 & adjustedTransform, const std::string & suffix) {
        // Add lights for each <light> element.
        ELEM_CHILDREN_FOR_EACH(lightElem, parentElem, "light") {
            addLightFromXML(lightElem, adjustedTransform, suffix);
        }
        
        // Recursively call for each array element, (we don't need any extra args).
        INTERPRET_ARRAY_ELEM(parentElem, adjustedTransform, suffix, addAllLightsFromXML)
        
        // Recursively call for each transform element, (we don't need any extra args).
        INTERPRET_TRANSFORM_ELEM(parentElem, adjustedTransform, suffix, addAllLightsFromXML)
    }
    
    void WorldManager::readFromDocument(void) {
        
        // Get base world element
        GET_ELEM(worldElem, loadedDocument, "world")
            GET_ATTRIB(worldVersionAttr, worldElem, "version")
            if (WORLD_XML_VERSION != worldVersionAttr) {
                PRINT_XML_ERROR("Wrong world XML version, found: " << worldVersionAttr << ", required: " << WORLD_XML_VERSION << '.')
            }
        
        // Set properties of lighting manager.
        {
        GET_ELEM(lightingManagerElem, worldElem, "lightingManager")
            GET_ELEM(gridSizeElem, lightingManagerElem, "gridSize")
                GET_ATTRIB_UINT(gridSizeXAttr, gridSizeElem, "x")
                GET_ATTRIB_UINT(gridSizeYAttr, gridSizeElem, "y")
                GET_ATTRIB_UINT(gridSizeZAttr, gridSizeElem, "z")
            GET_ELEM(gridCorner1Elem, lightingManagerElem, "gridCorner1")
                GET_ATTRIB_GLFLOAT(gridCorner1XAttr, gridCorner1Elem, "x")
                GET_ATTRIB_GLFLOAT(gridCorner1YAttr, gridCorner1Elem, "y")
                GET_ATTRIB_GLFLOAT(gridCorner1ZAttr, gridCorner1Elem, "z")
            GET_ELEM(gridCorner2Elem, lightingManagerElem, "gridCorner2")
                GET_ATTRIB_GLFLOAT(gridCorner2XAttr, gridCorner2Elem, "x")
                GET_ATTRIB_GLFLOAT(gridCorner2YAttr, gridCorner2Elem, "y")
                GET_ATTRIB_GLFLOAT(gridCorner2ZAttr, gridCorner2Elem, "z")
            GET_ELEM(ambientLightColourElem, lightingManagerElem, "ambientLightColour")
                GET_ATTRIB_GLFLOAT(ambientLightColourRedAttr,   ambientLightColourElem, "red")
                GET_ATTRIB_GLFLOAT(ambientLightColourGreenAttr, ambientLightColourElem, "green")
                GET_ATTRIB_GLFLOAT(ambientLightColourBlueAttr,  ambientLightColourElem, "blue")
            
            lightingManager->setGridSize(
                GLuvec3(gridSizeXAttr, gridSizeYAttr, gridSizeZAttr),
                GLvec3(gridCorner1XAttr, gridCorner1YAttr, gridCorner1ZAttr),
                GLvec3(gridCorner2XAttr, gridCorner2YAttr, gridCorner2ZAttr)
            );
            lightingManager->setAmbientLightColour(
                GLvec3(ambientLightColourRedAttr, ambientLightColourGreenAttr, ambientLightColourBlueAttr)
            );
        }
        
        // For each asset, add instances from it's child elements.
        {
        GET_ELEM(assetsElem, worldElem, "assets")
            ELEM_CHILDREN_FOR_EACH(assetElem, assetsElem, "asset") {
                GET_ATTRIB_STDSTRING(assetNameAttr, assetElem, "name")
                addAllInstancesFromXML(assetElem, GLmat4(1.0f), "", assetNameAttr);
            }
        }
        
        // Go through each light, add them.
        {
        GET_ELEM(lightsElem, worldElem, "lights")
            addAllLightsFromXML(lightsElem, GLmat4(1.0f), "");
        }
    }
        
    void WorldManager::loadFromFile(const std::string & filename) {
        if (isLoading) {
            std::cerr << "WARNING: WorldManager: Load of file " << filename << " requested while already loading.\n";
        } else {
            isLoading = true;
            // The time-consuming bit is reading in the file. So do that in a separate thread.
            loadJob = threadQueue->add(token, [this, filename](void) {
                loadedDocument = new tinyxml2::XMLDocument();
                if (loadedDocument->LoadFile(filename.c_str()) != tinyxml2::XML_SUCCESS) {
                    delete loadedDocument;
                    loadedDocument = NULL;
                    PRINT_XML_ERROR("Failed to read file: Check that it exists, we have permission to read it, and that it is valid XML.")
                }
            });
        }
    }
    
    void WorldManager::saveToFile(const std::string & filename) {
        
    }

    void WorldManager::step(void) {
        if (isLoading) {
            if (threadQueue->getJobStatus(loadJob) == threading::JobStatus::complete) {
                isLoading = false;
                // loadedDocument will be NULL if it failed to load.
                if (loadedDocument != NULL) {
                    eraseAllAssetsAndLights();
                    readFromDocument();
                    delete loadedDocument;
                    loadedDocument = NULL;
                }
            }
        }
        
        if (isSaving) {
            if (threadQueue->getJobStatus(loadJob) == threading::JobStatus::complete) {
                isSaving = false;
            }
        }
    }
    
    Instance * WorldManager::getInstance(const std::string & assetName, const std::string & instanceName) const {
        auto assetIter = instances.find(assetName);
        if (assetIter == instances.end()) return NULL;
        auto instanceIter = assetIter->second.find(instanceName);
        if (instanceIter == assetIter->second.end()) return NULL;
        return instanceIter->second;
    }
    
    Light * WorldManager::getLight(const std::string & lightName) const {
        auto lightIter = lights.find(lightName);
        if (lightIter == lights.end()) return NULL;
        return lightIter->second;
    }
    
    Instance * WorldManager::addInstance(const std::string & assetName, const std::string & instanceName, const GLmat4 & transformation) {
        std::unordered_map<std::string, Instance *> & instancesInAsset = instances[assetName];
        if (instancesInAsset.find(instanceName) == instancesInAsset.end()) {
            Instance * inst = new Instance(assetManager, physicsWorld, assetName, instanceName, transformation);
            instancesInAsset[instanceName] = inst;
            return inst;
        } else {
            std::cerr << "WARNING: WorldManager: Instance with name " << instanceName << " added twice. Refusing to add the second version: names must be unique.\n";
            return NULL;
        }
    }

    Light * WorldManager::addLight(const std::string & lightName, const GLvec3 & position, const GLvec3 & brightness, GLfloat radius, GLfloat falloff) {
        if (lights.find(lightName) == lights.end()) {
            Light * light = new Light(lightingManager, position, brightness, radius, falloff);
            lights[lightName] = light;
            return light;
        } else {
            std::cerr << "WARNING: WorldManager: Light with name " << lightName << " added twice. Refusing to add the second version: names must be unique.\n";
            return NULL;
        }
    }
}
