/*
 * FirstPersonPhysicsCamera.h
 *
 *  Created on: 20 Jul 2020
 *      Author: wilson
 */

#ifndef AWE_WORLD_FIRSTPERSONPHYSICSCAMERA_H_
#define AWE_WORLD_FIRSTPERSONPHYSICSCAMERA_H_

#include <bullet3/BulletCollision/CollisionShapes/btCapsuleShape.h>
#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <bullet3/LinearMath/btMotionState.h>

#include "DynamicCharacterController.h"
#include "../util/types/Types.h"
#include "BaseCamera.h"

namespace awe {
    class PhysicsWorld;

    /**
     *
     */
    class FirstPersonPhysicsCamera : public BaseCamera, public btMotionState {
    private:
        // --------- Fields that affect movement behaviour ---------------------
        
        /**
         * The mass of the player's rigid body. The forces used to push the
         * player around are scaled by this mass. The larger the player's mass,
         * the easier they will be able to push other dynamic rigid bodies
         * around.
         * 
         * This is set at the start in the constructor, but can be easily
         * changed. Its default value is 0.4.
         */
        GLfloat playerMass;
        
        /**
         * The height above the player's feet that their eyepoint is.
         * The eyepoint is always the position of the camera, so the collision
         * body extends this distance below the camera's position.
         * 
         * This is set at the start in the constructor, and cannot be (easily)
         * changed. Its default value is 1.7.
         */
        GLfloat playerEyeHeight;
        
        /**
         * The total height of the player, from the bottom of the collision
         * body to the top-most point of the player. This represents the
         * smallest vertical space that the player can walk through.
         * Note that this must be LARGER than the player's eye height.
         * 
         * This is set at the start in the constructor, and cannot be (easily)
         * changed. Its default value is 1.85.
         */
        GLfloat playerTotalHeight;
        
        /**
         * The radius of the spheres used in the player's capsule collision
         * body, i.e: Half the WIDTH of the player's collision body.
         * Players cannot fit through gaps which are smaller than double this
         * value horizontally.
         * Note that this must be less than half the player's total height.
         * 
         * This is set at the start in the constructor, and cannot be (easily)
         * changed. Its default value is 0.2.
         */
        GLfloat playerRadius;
    
        // ---------------------------------------------------------------------
    
        PhysicsWorld * physicsWorld;
        
        btCapsuleShapeZ capsuleShape;
        btRigidBody rigidBody;
        
        DynamicCharacterController characterController;
        
        // Don't allow copying - that would break pointers to us.
        FirstPersonPhysicsCamera(const FirstPersonPhysicsCamera & other);
        FirstPersonPhysicsCamera & operator = (const FirstPersonPhysicsCamera & other);
        
    public:
        FirstPersonPhysicsCamera(PhysicsWorld * physicsWorld, const GLvec3 & initialPosition, GLfloat initialHDir, GLfloat initialVDir,
                                 GLfloat playerMass = 0.4f,
                                 GLfloat playerEyeHeight = 1.7f,
                                 GLfloat playerTotalHeight = 1.85f,
                                 GLfloat playerRadius = 0.2f);
        virtual ~FirstPersonPhysicsCamera(void);
        
    protected:
        // Note that we don't actually update the position here, we just
        // interact with the character controller. This is because the position
        // is automatically updated by the motion state stuff.
        virtual void updatePosition(GLfloat elapsed) override;
            
    public:
    
        /**
         * Allows access to our character controller, so stuff like movement
         * speed, jump speed etc can be modified.
         */
        inline DynamicCharacterController & getCharacterController(void) {
            return characterController;
        }
    
        // ================== Motion state integration =========================
        
        virtual void getWorldTransform(btTransform & worldTrans) const override;
        virtual void setWorldTransform(const btTransform & worldTrans) override;
    };
}

#endif
