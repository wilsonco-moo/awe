/*
 * WorldManager.h
 *
 *  Created on: 24 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_WORLD_WORLDMANAGER_H_
#define AWE_WORLD_WORLDMANAGER_H_

#include <unordered_map>
#include <cstdint>
#include <string>

#include "../util/types/Types.h"

namespace threading {
    class ThreadQueue;
}

namespace tinyxml2 {
    class XMLDocument;
    class XMLElement;
}

namespace awe {
    class LightingManager;
    class AssetManager;
    class PhysicsWorld;
    class Instance;
    class Light;

    /**
     * WorldManager allows the convenient loading of scenes, (made of instances
     * and lights), from an xml file.
     */
    class WorldManager {
    public:
        const static std::string WORLD_XML_VERSION;
        
    private:
        threading::ThreadQueue * threadQueue;
        void * token;
        AssetManager * assetManager;
        LightingManager * lightingManager;
        PhysicsWorld * physicsWorld;

        std::unordered_map<std::string, std::unordered_map<std::string, Instance *>> instances;
        std::unordered_map<std::string, Light *> lights;

        bool isLoading,
             isSaving;
        tinyxml2::XMLDocument * loadedDocument;
        
        uint64_t loadJob, saveJob;

        // Don't allow copying: We hold raw resources.
        WorldManager(const WorldManager & other);
        WorldManager & operator = (const WorldManager & other);

    public:
        /**
         * WorldManager requires a threadQueue for asynchronous loading
         * operations, an assetManager for loading instances/assets, and a
         * lightingManager for controlling the scene's lighting.
         */
        WorldManager(threading::ThreadQueue * threadQueue, AssetManager * assetManager, LightingManager * lightingManager, PhysicsWorld * physicsWorld);
        
        /**
         * An alternate constructor which automatically runs loadFromFile,
         * using the provided filename.
         */
        WorldManager(threading::ThreadQueue * threadQueue, AssetManager * assetManager, LightingManager * lightingManager, PhysicsWorld * physicsWorld, const std::string & filename);
        
        virtual ~WorldManager(void);
        
    private:
        void eraseAllAssetsAndLights(void);
        void readFromDocument(void);
        
        // Internal helper methods for XML parsing
        
        // Adds a single instance from the provided instance XML node.
        void addInstanceFromXML(tinyxml2::XMLElement * element, const GLmat4 & adjustedTransform, const std::string & suffix, const std::string & assetName);
        // Adds a single instance from the provuided instance XML node.
        void addLightFromXML(tinyxml2::XMLElement * element, const GLmat4 & adjustedTransform, const std::string & suffix);
        
        // Adds a set of instances, from the children of the provided XML node.
        void addAllInstancesFromXML(tinyxml2::XMLElement * parentElem, const GLmat4 & adjustedTransform, const std::string & suffix, const std::string & assetName);
        // Adds a set of lights, from the children of the provided XML node.
        void addAllLightsFromXML(tinyxml2::XMLElement * parentElem, const GLmat4 & adjustedTransform, const std::string & suffix);
        
    public:
        
        /**
         * Loads the scene from the specified xml file. The file will be loaded
         * and parsed asynchronously, and loaded in at some point later when
         * step is called.
         */
        void loadFromFile(const std::string & filename);
        
        /**
         * Saves the scene to a file.
         * TODO: Implement this.
         */
        void saveToFile(const std::string & filename);
        
        /**
         * This should be called once per frame. This controls asynchronous
         * loading.
         */
        void step(void);
        
        /**
         * Accesses an instance (by name) from the scene. If the instance (or
         * it's asset) do not exist, then NULL is returned.
         */
        Instance * getInstance(const std::string & assetName, const std::string & instanceName) const;
        
        /**
         * Accesses a light from the scene (by name). If the light does not
         * exist, then NULL is returned.
         */
        Light * getLight(const std::string & lightName) const;
        
        /**
         * Returns true if we currently have an asynchronous job going on to
         * load a scene from an XML file.
         */
        inline bool getIsLoading(void) const {
            return isLoading;
        }
        
        /**
         * Returns true if we currently have an asynchronous job going on to
         * save a scene to an XML file.
         */
        inline bool getIsSaving(void) const {
            return isSaving;
        }
        
        /**
         * Adds an instance, with the specified asset filename and instance name, and base transform.
         * A pointer to the newly created instance is returned.
         * If an instance with that name already existed, within the specified asset, then NULL is
         * returned, and the instance is not added.
         */
        Instance * addInstance(const std::string & assetName, const std::string & instanceName, const GLmat4 & transformation = GLmat4(1.0f));
        
        /**
         * Adds an light to the scene, with the specified light name.
         * A pointer to the newly created light is returned.
         * If a light with that name already existed, within the world, then NULL is returned,
         * and the light is not added.
         */
        Light * addLight(const std::string & lightName, const GLvec3 & position, const GLvec3 & brightness, GLfloat radius, GLfloat falloff = 6.0f);
    };
}

#endif
