/*
 * BaseCamera.h
 *
 *  Created on: 29 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_WORLD_BASECAMERA_H_
#define AWE_WORLD_BASECAMERA_H_

#include <GL/gl.h>

#include "../util/types/Types.h"

namespace awe {

    /**
     *
     */
    class BaseCamera {
    public:
        const static GLfloat DEFAULT_MOVE_SPEED,
                             DEFAULT_MOUSE_SENSITIVITY,
                             DEFAULT_FOVX,
                             DEFAULT_NEAR_CLIP,
                             DEFAULT_FAR_CLIP,
                             DEFAULT_ZOOM_SPEED;
    private:
        GLmat4 projectionTransform,
               viewTransform,
               viewProjectionTransform;
                             
    protected:
        // Which direction the camera is currently moving, and whether it is locked.
        bool movingForward,
             movingBack,
             movingLeft,
             movingRight,
             movingUp,
             movingDown,
             locked,
             zoomingIn,
             zoomingOut;
        
        // Current camera position, horizontal/vertical look direction.
        GLvec3 position;
        GLfloat hDir, vDir;
        
        // Camera distance (for third person cameras). Note that cameraDistance
        // is calculated as 2^cameraDistanceExp, so ought not be changed.
        // By default, cameraDistanceExp is 0 and cameraDistance is 1.
        GLfloat cameraDistance, cameraDistanceExp;
        
        // The current size of the window.
        GLsizei windowWidth, windowHeight;

        // For mouse-look movement.
        int mouseDiffX, mouseDiffY;
        
    public:
        // Public configurable parameters. Note that field of view is
        // horizontal.
        GLfloat fovX, nearClip, farClip, moveSpeed, mouseSensitivity, zoomSpeed;
        
        
        BaseCamera(const GLvec3 & initialPosition, GLfloat initialHDir, GLfloat initialVDir);
        virtual ~BaseCamera(void);
        
    private:
        void lockMouse(void);
        void unlockMouse(void);
        
    protected:
        /**
         * This is run every time updateCameraPosition is called, whenever the
         * mouse is locked. This should update hDir and vDir from the current
         * values of mouseDiffX and mouseDiffY. This can be overridden to
         * customise mouse-looking.
         */
        virtual void updateDirectionFromMouseDiff(void);
        
        /**
         * This should update the current camera position, from the current
         * values of movingForward, movingBack, movingLeft and movingRight.
         * This is where a custom movement system should be implemented.
         */
        virtual void updatePosition(GLfloat elapsed) = 0;
        
        /**
         * This calculates the view transform, from the current values of
         * position, hDir and vDir. This can be overridden to customise the
         * behaviour.
         */
        virtual GLmat4 getViewTransform(void) const;
        
    public:
        void onKeyPress(unsigned char key);
        void onKeyRelease(unsigned char key);
        void onKeyPressSpecial(unsigned char key);
        void onKeyReleaseSpecial(unsigned char key);
        void onMouseMove(int x, int y);
        void onResize(GLsizei width, GLsizei height);

        /**
         * This calculates the eyepoint of the camera. Custom camera types
         * must override this to return the real eyepoint of the camera.
         * By default this returns the same as getPosition.
         */
        virtual GLvec3 calculateEyepoint(void) const;

        /**
         * Updates the camera position in the world.
         * Returns the new (current) view-projection transformation.
         */
        const GLmat4 & updateCameraPosition(GLfloat elapsed);
        
        /**
         * Returns true if the camera is currently mouse-locked.
         */
        inline bool isMouseLocked(void) const {
            return locked;
        }
        
        /**
         * Gets the current camera position.
         * This is either the eye position, or the position which the camera
         * is orbiting around (depending on camera type).
         */
        inline const GLvec3 & getPosition(void) const { return position; }
        
        /**
         * Allows accessing and changing the current camera horizontal and
         * vertical directions.
         */
        inline GLfloat getHDir(void) const { return hDir; }
        inline GLfloat getVDir(void) const { return vDir; }
        inline void setHDir(GLfloat newHdir) { hDir = newHdir; }
        inline void setVDir(GLfloat newVdir) { hDir = newVdir; }
        
        /**
         * Allows access to the mouseDiffX and mouseDiffY.
         * These store the change in mouse position, from the centre, since
         * updateCameraPosition was last called.
         * By accessing these, and resetting them to zero, before calling
         * updateCameraPosition, mouse looking can be redirected to so something
         * else, (like rotating an object, etc).
         */
        inline int getMouseDiffX(void) const { return mouseDiffX; }
        inline int getMouseDiffY(void) const { return mouseDiffY; }
        inline void setMouseDiffX(int newMouseDiffX) { mouseDiffX = newMouseDiffX; }
        inline void setMouseDiffY(int newMouseDiffY) { mouseDiffY = newMouseDiffY; }
        
        /**
         * Allows changing the (exp) camera distance.
         * This calculates the new camera distance as two to the power of
         * newCameraDistanceExp.
         */
        void setCameraDistanceExp(GLfloat newCameraDistanceExp);
        void addCameraDistanceExp(GLfloat cameraDistanceExpDiff);
        
        /**
         * Gets the current camera distance.
         */
        inline GLfloat getCameraDistance(void) const {
            return cameraDistance;
        }
        
        /**
         * Gets the current (exp) camera distance. Note that this is less useful
         * than getCameraDistance.
         */
        inline GLfloat getCameraDistanceExp(void) const {
            return cameraDistanceExp;
        }
        
        /**
         * Allows manually warping the mouse to the centre of the screen.
         * This is helpful when using mouse input for something else, while
         * the mouse is locked.
         */
        void warpMouseToCentre(void) const;
        
        /**
         * Calculates a normalised vector, representing the current
         * direction which the camera is looking.
         */
        GLvec3 calculateLookVector(void) const;
    };
}

#endif
