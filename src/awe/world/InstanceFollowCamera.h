/*
 * InstanceFollowCamera.h
 *
 *  Created on: 23 Jul 2020
 *      Author: wilson
 */

#ifndef AWE_WORLD_INSTANCEFOLLOWCAMERA_H_
#define AWE_WORLD_INSTANCEFOLLOWCAMERA_H_

#include "BaseCamera.h"

namespace awe {
    class Instance;

    /**
     * This camera follows around an instance in third person.
     */
    class InstanceFollowCamera : public BaseCamera {
    private:
        Instance * instance;
        
    public:
        /**
         * When created we require an instance to follow. If that is NULL,
         * our position is reset to 0,0,0.
         */
        InstanceFollowCamera(Instance * instance, GLfloat initialHDir, GLfloat initialVDir);
        virtual ~InstanceFollowCamera(void);
    
    protected:
        virtual void updatePosition(GLfloat elapsed) override;
        virtual GLmat4 getViewTransform(void) const override;
        
    public:
        virtual GLvec3 calculateEyepoint(void) const override;
    
        /**
         * Allows access to the instance which we are associated with.
         */
        inline Instance * getInstance(void) const {
            return instance;
        }
        
        /**
         * Allows changing the instance which we are associated with.
         * If NULL is provided, our position is reset to 0,0,0.
         */
        inline void setInstance(Instance * newInstance) {
            instance = newInstance;
        }
    };
}

#endif
