/*
 * BaseCamera.cpp
 *
 *  Created on: 29 Jan 2020
 *      Author: wilson
 */

#include "BaseCamera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <GL/freeglut.h>
#include <algorithm>
#include <cmath>

#include "../util/Util.h"

namespace awe {

    const GLfloat BaseCamera::DEFAULT_MOVE_SPEED = 2.0f,
                  BaseCamera::DEFAULT_MOUSE_SENSITIVITY = 0.006f,
                  BaseCamera::DEFAULT_FOVX = 90 * M_PI / 180.0f, // (90 degrees horizontal)
                  BaseCamera::DEFAULT_NEAR_CLIP = 0.1f,
                  BaseCamera::DEFAULT_FAR_CLIP = 1000.0f,
                  BaseCamera::DEFAULT_ZOOM_SPEED = 2.0f;
    
    BaseCamera::BaseCamera(const GLvec3 & initialPosition, GLfloat initialHDir, GLfloat initialVDir) :
        projectionTransform(1.0f),
        viewTransform(1.0f),
        viewProjectionTransform(1.0f),
    
        movingForward(false),
        movingBack(false),
        movingLeft(false),
        movingRight(false),
        movingUp(false),
        movingDown(false),
        locked(false),
        zoomingIn(false),
        zoomingOut(false),
        
        position(initialPosition),
        hDir(initialHDir),
        vDir(initialVDir),
        
        cameraDistance(1.0f),
        cameraDistanceExp(0.0f),
        
        windowWidth(800),
        windowHeight(600),

        mouseDiffX(0),
        mouseDiffY(0),
        
        fovX(DEFAULT_FOVX),
        nearClip(DEFAULT_NEAR_CLIP),
        farClip(DEFAULT_FAR_CLIP),
        moveSpeed(DEFAULT_MOVE_SPEED),
        mouseSensitivity(DEFAULT_MOUSE_SENSITIVITY),
        zoomSpeed(DEFAULT_ZOOM_SPEED) {
    }
    
    BaseCamera::~BaseCamera(void) {
    }
    
    void BaseCamera::lockMouse(void) {
        locked = true;
        glutWarpPointer(windowWidth / 2, windowHeight / 2);
        mouseDiffX = 0;
        mouseDiffY = 0;
    }
    void BaseCamera::unlockMouse(void) {
        locked = false;
    }
    
    void BaseCamera::updateDirectionFromMouseDiff(void) {
        hDir = fmod(hDir + ((GLfloat)mouseDiffX) * mouseSensitivity, M_PI * 2.0f);
        vDir = std::min((GLfloat)(0.5f * M_PI), std::max((GLfloat)(-0.5f * M_PI), vDir + ((GLfloat)mouseDiffY) * mouseSensitivity));
        if (mouseDiffX != 0 || mouseDiffY != 0) {
            glutWarpPointer(windowWidth / 2, windowHeight / 2);
        }
        mouseDiffX = 0;
        mouseDiffY = 0;
    }
    
    GLmat4 BaseCamera::getViewTransform(void) const {
        return glm::lookAt(
            awe::GLvec3(position.x, position.y, position.z),
            awe::GLvec3(position.x + cos(hDir) * cos(vDir), position.y - sin(hDir) * cos(vDir), position.z - sin(vDir)),
            awe::GLvec3(sin(vDir) * cos(hDir), -sin(vDir) * sin(hDir), cos(vDir))
        );
    }
    
    GLvec3 BaseCamera::calculateEyepoint(void) const {
        return position;
    }
    
    void BaseCamera::onKeyPress(unsigned char key) {
        switch(key) {
        case 'w': case 'W':
            movingForward = true;
            break;
        case 'a': case 'A':
            movingLeft = true;
            break;
        case 's': case 'S':
            movingBack = true;
            break;
        case 'd': case 'D':
            movingRight = true;
            break;
        case ' ':
            movingUp = true;
            break;
        case '+': case '=':
            zoomingIn = true;
            break;
        case '-': case '_':
            zoomingOut = true;
            break;
        default:
            break;
        }
    }
    
    void BaseCamera::onKeyRelease(unsigned char key) {
        switch(key) {
        case 'w': case 'W':
            movingForward = false;
            break;
        case 'a': case 'A':
            movingLeft = false;
            break;
        case 's': case 'S':
            movingBack = false;
            break;
        case 'd': case 'D':
            movingRight = false;
            break;
        case ' ':
            movingUp = false;
            break;
        case '+': case '=':
            zoomingIn = false;
            break;
        case '-': case '_':
            zoomingOut = false;
            break;
        default:
            break;
        }
    }
    
    void BaseCamera::onKeyPressSpecial(unsigned char key) {
        switch(key) {
        case 112: // SHIFT
            movingDown = true;
            break;
        default:
            break;
        }
    }
    
    void BaseCamera::onKeyReleaseSpecial(unsigned char key) {
        switch(key) {
        case 112: // SHIFT
            movingDown = false;
            break;
        case 114: case 115: // CTRL and RIGHT CTRL
            if (isMouseLocked()) {
                unlockMouse();
            } else {
                lockMouse();
            }
            break;
        default:
            break;
        }
    }
    
    void BaseCamera::onMouseMove(int x, int y) {
        if (locked) {
            if (std::abs(x - windowWidth/2) > std::abs(mouseDiffX)) {
                mouseDiffX = x - windowWidth/2;
            }
            if (std::abs(y - windowHeight/2) > std::abs(mouseDiffY)) {
                mouseDiffY = y - windowHeight/2;
            }
        }
    }
    
    void BaseCamera::onResize(GLsizei width, GLsizei height) {
        windowWidth = width;
        windowHeight = height;
        glViewport(0, 0, width, height);
        projectionTransform = Util::perspectiveX(fovX, width, height, nearClip, farClip);
    }

    const GLmat4 & BaseCamera::updateCameraPosition(GLfloat elapsed) {
        // Update hDir and vDir from mouse position if mouse is locked.
        if (locked) {
            updateDirectionFromMouseDiff();
        }

        // Update position, based on whether we are moving or not.
        updatePosition(elapsed);
        
        // Update the view transform, based on the direction we're looking.
        viewTransform = getViewTransform();
        
        // Update the view-projection transform and return it.
        viewProjectionTransform = projectionTransform * viewTransform;
        return viewProjectionTransform;
    }
    
    void BaseCamera::setCameraDistanceExp(GLfloat newCameraDistanceExp) {
        cameraDistanceExp = newCameraDistanceExp;
        cameraDistance = pow(2.0f, newCameraDistanceExp);
    }
    
    void BaseCamera::addCameraDistanceExp(GLfloat cameraDistanceExpDiff) {
        cameraDistanceExp += cameraDistanceExpDiff;
        cameraDistance = pow(2.0f, cameraDistanceExp);
    }
    
    void BaseCamera::warpMouseToCentre(void) const {
        glutWarpPointer(windowWidth / 2, windowHeight / 2);
    }
    
    GLvec3 BaseCamera::calculateLookVector(void) const {
        return GLvec3(cos(hDir) * cos(vDir),
                      -sin(hDir) * cos(vDir),
                      -sin(vDir));
    }
}
