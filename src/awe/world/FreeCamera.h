/*
 * FreeCamera.h
 *
 *  Created on: 29 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_WORLD_FREECAMERA_H_
#define AWE_WORLD_FREECAMERA_H_

#include "BaseCamera.h"

namespace awe {

    /**
     *
     */
    class FreeCamera : public BaseCamera {
    private:

    public:
        FreeCamera(const GLvec3 & initialPosition = GLvec3(0.0f, 0.0f, 1.7f), GLfloat initialHDir = 0.0f, GLfloat initialVDir = 0.0f);
        virtual ~FreeCamera(void);
        
    protected:
        virtual void updatePosition(GLfloat elapsed) override;
    };
}

#endif
