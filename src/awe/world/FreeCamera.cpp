/*
 * FreeCamera.cpp
 *
 *  Created on: 29 Jan 2020
 *      Author: wilson
 */

#include "FreeCamera.h"

namespace awe {

    FreeCamera::FreeCamera(const GLvec3 & initialPosition, GLfloat initialHDir, GLfloat initialVDir) :
        BaseCamera(initialPosition, initialHDir, initialVDir) {
    }
    
    FreeCamera::~FreeCamera(void) {
    }
    
    void FreeCamera::updatePosition(GLfloat elapsed) {
        GLfloat speed = moveSpeed * elapsed;
        if (movingForward) {
            position.x += speed * cos(hDir);
            position.y -= speed * sin(hDir);
        }
        if (movingBack) {
            position.x -= speed * cos(hDir);
            position.y += speed * sin(hDir);
        }
        if (movingLeft) {
            position.x += speed * sin(hDir);
            position.y += speed * cos(hDir);
        }
        if (movingRight) {
            position.x -= speed * sin(hDir);
            position.y -= speed * cos(hDir);
        }
        if (movingUp) {
            position.z += speed;
        }
        if (movingDown) {
            position.z -= speed;
        }
    }
}
