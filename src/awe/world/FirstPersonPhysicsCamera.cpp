/*
 * FirstPersonPhysicsCamera.cpp
 *
 *  Created on: 20 Jul 2020
 *      Author: wilson
 */

#include "FirstPersonPhysicsCamera.h"

#include <bullet3/BulletCollision/CollisionShapes/btCollisionShape.h>
#include <bullet3/LinearMath/btQuaternion.h>
#include <bullet3/LinearMath/btTransform.h>
#include <bullet3/LinearMath/btVector3.h>
#include <iostream>

#include "../util/types/ConversionUtil.h"
#include "../util/physics/PhysicsWorld.h"
#include "../util/Util.h"

namespace awe {
    
    FirstPersonPhysicsCamera::FirstPersonPhysicsCamera(PhysicsWorld * physicsWorld, const GLvec3 & initialPosition, GLfloat initialHDir, GLfloat initialVDir, GLfloat playerMass, GLfloat playerEyeHeight, GLfloat playerTotalHeight, GLfloat playerRadius) :
        BaseCamera(initialPosition, initialHDir, initialVDir),
        btMotionState(),
        
        // Set properties.
        playerMass(playerMass),
        playerEyeHeight(playerEyeHeight),
        playerTotalHeight(playerTotalHeight),
        playerRadius(playerRadius),
        
        physicsWorld(physicsWorld),
        
        capsuleShape(playerRadius, playerTotalHeight - 2.0f * playerRadius),
        rigidBody(playerMass, this, &capsuleShape, Util::getInertia(capsuleShape, playerMass)),
        
        characterController(Util::getAndAddToPhysics(physicsWorld, &rigidBody), &capsuleShape) {
            
        physicsWorld->getDynamicsWorld().addAction(&characterController);
    }
    
    FirstPersonPhysicsCamera::~FirstPersonPhysicsCamera(void) {
        physicsWorld->getDynamicsWorld().removeAction(&characterController);
        physicsWorld->getDynamicsWorld().removeRigidBody(&rigidBody);
    }
    
    void FirstPersonPhysicsCamera::updatePosition(GLfloat elapsed) {
        
        GLvec3 moveDirection(0.0f, 0.0f, 0.0f);
        if (movingForward) {
            moveDirection.x += cos(hDir);
            moveDirection.y -= sin(hDir);
        }
        if (movingBack) {
            moveDirection.x -= cos(hDir);
            moveDirection.y += sin(hDir);
        }
        if (movingLeft) {
            moveDirection.x += sin(hDir);
            moveDirection.y += cos(hDir);
        }
        if (movingRight) {
            moveDirection.x -= sin(hDir);
            moveDirection.y -= cos(hDir);
        }
        
        characterController.setMovementDirection(ConversionUtil::glmToBulletVec3(moveDirection));
        
        if (movingUp && characterController.canJump()) {
            characterController.jump();
        }
    }
    
    void FirstPersonPhysicsCamera::getWorldTransform(btTransform & worldTrans) const {
        // Work out Z offset from camera viewpoint (eyepoint) to capsule centre.
        GLfloat zOffset = playerEyeHeight - (0.5f * playerTotalHeight);
        
        // Set the world transform to current position minus the z offset.
        worldTrans.setOrigin(btVector3(position.x, position.y, position.z - zOffset));
        worldTrans.setRotation(btQuaternion::getIdentity());
    }
    
    void FirstPersonPhysicsCamera::setWorldTransform(const btTransform & worldTrans) {
        // Work out Z offset from camera viewpoint (eyepoint) to capsule centre.
        GLfloat zOffset = playerEyeHeight - (0.5f * playerTotalHeight);
        
        // Set current position from transform origin plus the z offset.
        const btVector3 & transOrig = worldTrans.getOrigin();
        position = GLvec3(transOrig.getX(), transOrig.getY(), transOrig.getZ() + zOffset);
    }
}
