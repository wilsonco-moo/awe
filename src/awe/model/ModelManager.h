/*
 * ModelManager.h
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_MODEL_MODELMANAGER_H_
#define AWE_MODEL_MODELMANAGER_H_

#include <unordered_map>
#include <unordered_set>
#include <string>
#include <list>

namespace threading {
    class ThreadQueue;
}

namespace awe {

    class Model;

    /**
     * ModelManager allows models to be conveniently loaded from files, without
     * creating duplicate models when the same file is loaded multiple times.
     * ModelManager also handles allocation of these models, so that when all
     * uses of a particular model file have been abandoned, the model is removed
     * from memory.
     * 
     * NOTE: Access to this class is not thread safe. Access should either be
     *       synchronised outside this class, or done from only one thread.
     */
    class ModelManager {
    public:
        /**
         * This should be used in the absence of any sensible model to load.
         * It is assumed that this will always cause the loading of the
         * "no model" model.
         */
        const static std::string NO_MODEL_FILENAME;
        
    private:
        threading::ThreadQueue * queue;
        std::unordered_map<std::string, Model *> models;
        std::unordered_set<Model *> disusedModels;
        std::list<std::string> modelsStillLoading;
        
        // Don't allow copying: We hold raw resources.
        ModelManager(const ModelManager & other);
        ModelManager & operator = (const ModelManager & other);

    public:
        /**
         * The constructor for ModelManager requires a ThreadQueue. This is so
         * that background (asynchronous) operations can be run.
         */
        ModelManager(threading::ThreadQueue * queue);
        virtual ~ModelManager(void);
        
        // ------------------------ Public API ---------------------------------
        
        /**
         * Requests the load of a model, from the specified filename.
         * The model will be created immediately, (and shared across all
         * requests using the same filename).
         * Note that the model's data will be loaded asynchronously. As a
         * result, the Model's data will not be available to use until it's
         * isLoaded() method returns true.
         */
        Model * requestModel(const std::string & filename);
        
        /**
         * Abandons the use of model. This MUST only be called ONCE per
         * call to requestModel. Once this has been called, the specified
         * model must NEVER be used again.
         * Once all uses of a model have been abandoned, the model will be
         * removed from memory next time step or waitForLoad is called.
         */
        void abandonModel(Model * model);
        
        /**
         * This should be called each step. This manages asynchronous loading
         * and unloading of models.
         * Returned is an estimate of the number of pending load operations.
         */
        unsigned int step(void);
        
        /**
         * Returns true if there are currently any background operations going
         * on to load models.
         */
        inline bool isLoadOngoing(void) const {
            return !modelsStillLoading.empty();
        }
        
        /**
         * Blocks until all pending load operations have finished.
         * After this call, isLoadOngoing will always return false, and it will
         * be safe to access all models loaded with this ModelManager.
         * 
         * NOTE: This is slow, and in a frame based application (like a game), a
         *       poor way to wait for models to load. A better way would be to
         *       call step() each frame, displaying the returned value from
         *       step() as a progress bar, and stop when that returns zero.
         * 
         * In something like a command-line application waitForLoad is fine to
         * use.
         */
        void waitForLoad(void);
    };
}

#endif
