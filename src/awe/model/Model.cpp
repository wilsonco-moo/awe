/*
 * Model.cpp
 *
 *  Created on: 12 Jan 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "Model.h"

#include <threading/ThreadQueue.h>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <GL/gl.h>

#include "../util/types/Types.h"

namespace awe {

    Model::Model(threading::ThreadQueue * queue, const std::string & filename) :
        queue(queue),
        token(queue->createToken()),
        vertices(),
        filename(filename),
        internalLoaded(false),
        uses(0),
        vertexBuffer(0) {
            
        // Load the model asynchronously. When this job finishes, we know that loading has finished.
        loadJobId = queue->add(token, [this](void) {
            vertices = readModelFile(this->filename);
        });
    }
    
    Model::~Model(void) {
        queue->deleteToken(token);
        if (vertexBuffer != 0) glDeleteBuffers(1, &vertexBuffer);
    }
    
    bool Model::isLoaded(void) {
        if (!internalLoaded) {
            internalLoaded = (queue->getJobStatus(loadJobId) == threading::JobStatus::complete);
        }
        return internalLoaded;
    }
    
    void Model::waitForLoad(void) {
        queue->waitForJobToComplete(loadJobId);
    }
    
    GLuint Model::getVertexBuffer(void) {
        // If we created a vertex buffer already, return it.
        if (vertexBuffer != 0) {
            return vertexBuffer;
        }
        
        // Generate and bind the vertex buffer.
        glGenBuffers(1, &vertexBuffer);
        if (vertexBuffer == 0) { std::cerr << "WARNING: Failed to generate vertex buffer for model.\n"; return 0; }
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        
        // Allocate memory for the vertex buffer, copy the vertex data into it, unbind it, and return it.
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return vertexBuffer;
    }
    
    std::vector<Vertex> Model::generateNoModelModel(void) {
        // As a default model, return a kind of pyramid thing.
        return {
            {GLvec3(1,-1,0), GLvec2(-1,1)},  {GLvec3(-1,-1,0), GLvec2(1,1)}, {GLvec3(1,1,0),  GLvec2(-1,-1)}, // base 1
            {GLvec3(1,1,0),  GLvec2(-1,-1)}, {GLvec3(-1,-1,0), GLvec2(1,1)}, {GLvec3(-1,1,0), GLvec2(1,-1)},  // base 2
            
            {GLvec3(0,0,1),  GLvec2(0.5,0)}, {GLvec3(1,-1,0),  GLvec2(1,1)}, {GLvec3(1,1,0),   GLvec2(0,1)},  // right
            {GLvec3(0,0,1),  GLvec2(0.5,0)}, {GLvec3(-1,-1,0), GLvec2(1,1)}, {GLvec3(1,-1,0),  GLvec2(0,1)},  // top
            {GLvec3(0,0,1),  GLvec2(0.5,0)}, {GLvec3(-1,1,0),  GLvec2(1,1)}, {GLvec3(-1,-1,0), GLvec2(0,1)},  // left
            {GLvec3(0,0,1),  GLvec2(0.5,0)}, {GLvec3(1,1,0),   GLvec2(1,1)}, {GLvec3(-1,1,0),  GLvec2(0,1)},  // bottom
        };
    }
    
    #define LINE_WARNING(warning)                                                                                    \
        std::cerr << "WARNING: Failed to load model " << filename << ": (will use default model instead)\n" <<       \
                     filename << ": line " << lineNum << ": " << warning << '\n';                                    \
        return generateNoModelModel();
    
    #define OVERALL_WARNING(warning)                                                                                 \
        std::cerr << "WARNING: Failed to load model " << filename << ": (will use default model instead)\n" <<       \
                     warning << '\n';                                                                                \
        return generateNoModelModel();
    
    std::vector<Vertex> Model::readModelFile(const std::string & filename) {
        std::vector<Vertex> finalVertices;
        std::vector<GLvec3> vertexPositions;
        std::vector<GLvec2> vertexUvMap;
        std::ifstream file(filename);
        std::string line;
        
        bool reachedFaces = false,
             started = false;
        unsigned long long lineNum = 1;
        while(std::getline(file, line)) {
            // Ignore empty lines or commented lines.
            if (line.empty()) continue;
            
            switch(line[0]) {
            case '#':
                // Comment line: Do nothing.
                break;
            case 's':
                // Surface line (ignore).
                break;
            case 'v':
                // Vertex line.
                if (line.length() < 3) { LINE_WARNING("Vertex" << " line only consists of " << line.length() << " characters.") }
                switch(line[1]) {
                    case ' ': {
                        // Vertex position
                        const char * xNum = &line[2], *yNum, *zNum, *endPtr;
                        GLfloat x = (GLfloat)strtod(xNum, (char **)&yNum);
                        if (xNum == yNum) { LINE_WARNING("No (or invalid) " << 'x' << " coordinate in vertex position.") }
                        GLfloat y = (GLfloat)strtod(yNum, (char **)&zNum);
                        if (yNum == zNum) { LINE_WARNING("No (or invalid) " << 'y' << " coordinate in vertex position.") }
                        GLfloat z = (GLfloat)strtod(zNum, (char **)&endPtr);
                        if (zNum == endPtr || *endPtr != '\0') { LINE_WARNING("No (or invalid) " << 'z' << " coordinate in vertex position.") }
                        vertexPositions.emplace_back(x, y, z);
                        break;
                    }
                    case 't': {
                        // Vertex texture coordinate (uv). Note that the y coordinate must be flipped.
                        const char * xNum = &line[2], *yNum, *endPtr;
                        GLfloat x = (GLfloat)strtod(xNum, (char **)&yNum);
                        if (xNum == yNum) { LINE_WARNING("No (or invalid) " << 'x' << " value in UV map coordinate.") }
                        GLfloat y = 1.0f - (GLfloat)strtod(yNum, (char **)&endPtr);
                        if (yNum == endPtr || *endPtr != '\0') { LINE_WARNING("No (or invalid) " << 'y' << " value in UV map coordinate.") }
                        vertexUvMap.emplace_back(x, y);
                        break;
                    }
                    default:
                        LINE_WARNING("Unknown vertex command [" << line.substr(0, 2) << "].")
                }

                break;
            
            case 'f':
                // Face line.
                if (line.length() < 3) { LINE_WARNING("Face" << " line only consists of " << line.length() << " characters.") }
                const char * upTo = &line[2];
                // Read each vertex in the face.
                for (unsigned int i = 0; i < 3; i++) {
                    // Parse the stuff, check for issues.
                    const char * endPtr;
                    unsigned long int vertexId = strtoul(upTo, (char **)&endPtr, 0);
                    if (upTo == endPtr) { LINE_WARNING("Face vertex " << i << ": " << "Invalid or missing vertex ID.") }
                    if (*endPtr != '/') { LINE_WARNING("Face vertex " << i << ": " << "Unknown character between vertex ID and UV map coordinate ID.") }
                    if (vertexId == 0 || vertexId > vertexPositions.size()) { LINE_WARNING("Face vertex " << i << ": " << "Unknown vertex ID " << vertexId << '.') }
                    endPtr++; upTo = endPtr;
                    unsigned long int uvMapId = strtoul(upTo, (char **)&endPtr, 0);
                    if (upTo == endPtr) { LINE_WARNING("Face vertex " << i << ": " << "Invalid or missing UV map coordinate ID.") }
                    if (*endPtr != ' ' && *endPtr != '\0') { LINE_WARNING("Face vertex " << i << ": " << "Make sure normal information is NOT included in this model:\nUnknown character after UV map coordinate ID.") }
                    if (uvMapId == 0 || uvMapId > vertexUvMap.size()) { LINE_WARNING("Face vertex " << i << ": " << "Unknown UV map coordinate ID " << uvMapId << '.') }
                    upTo = endPtr;
                    // Store the vertex. Note that .obj files store indices as 1-based for whatever reason.
                    finalVertices.push_back({vertexPositions[vertexId - 1], vertexUvMap[uvMapId - 1]});
                }
                if (*upTo != '\0') { LINE_WARNING("Make sure this model consists only of triangles, i.e: No faces with more than 3 vertices:\nUnknown data after third face vertex.") }
                reachedFaces = true;
                break;
            }
            started = true;
            lineNum++;
        }
        
        if (!started) {
            OVERALL_WARNING("Model file either doesn't exist, or is empty.")
        }
        if (!reachedFaces) {
            // Don't generate a no model model: The user might? have intentionally given us an empty model file.
            // Make sure we print a warning though.
            std::cerr << "WARNING: No faces contained within model file " << filename << ".\n";
        }

        return finalVertices;
    }
}
