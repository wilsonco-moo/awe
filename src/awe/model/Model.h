/*
 * Model.h
 *
 *  Created on: 12 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_MODEL_MODEL_H_
#define AWE_MODEL_MODEL_H_

#include <cstddef>
#include <GL/gl.h>
#include <vector>
#include <string>

#include "../util/geometry/Vertex.h"

namespace threading {
    class ThreadQueue;
}

namespace awe {
    class ModelManager;

    /**
     * Model is a class which encapsulates loading models from .obj files.
     * The .obj file is loaded asynchronously, using a provided ThreadQueue.
     */
    class Model {
    private:
        friend class ModelManager; // This is so ModelManager can count uses.
        threading::ThreadQueue * queue;
        void * token;
        std::vector<Vertex> vertices;
        std::string filename;
        uint64_t loadJobId;
        // Set to true after the first time we check loadJobId and it has completed.
        // Saves asking the ThreadQueue *every* time, as that requires mutexes, map lookups, etc.
        bool internalLoaded;
        // This is so ModelManager can count uses. We set this initially to zero,
        // and never actually change it.
        size_t uses;
        // This represents a VBO storing our vertex data. This is accessed
        // through the getVertexBuffer method. It is not created until the first
        // time getVertexBuffer is run.
        GLuint vertexBuffer;
        // Disable copying: We perform asynchronous actions.
        Model(const Model & other);
        Model & operator = (const Model & other);
        
    public:
        /**
         * Creating a model requires a thread queue, and a filename from which
         * to load a model from.
         */
        Model(threading::ThreadQueue * queue, const std::string & filename);
        virtual ~Model(void);
        
        /**
         * Returns true if the model has finished loading, and false otherwise.
         */
        bool isLoaded(void);
        
        /**
         * Blocks until the model has finished loading. If the model has already
         * loaded, this does nothing.
         * After this call, isLoaded will always return true, and it will always
         * be safe to access the model.
         */
        void waitForLoad(void);
        
        /**
         * Returns the vertices contained within this model, without
         * modification.
         * This must be done after the model is loaded, (isLoaded must return
         * false), otherwise the result is undefined.
         */
        inline const std::vector<Vertex> & getVertices(void) const {
            return vertices;
        }
        
        /**
         * Returns the number of vertices contained within this mode.
         * This must be done after the model is loaded, (isLoaded must return
         * false), otherwise the result is undefined.
         */
        inline size_t getVertexCount(void) const {
            return vertices.size();
        }
        
        /**
         * Allows access to the filename from which this model was created.
         */
        inline const std::string & getFilename(void) const {
            return filename;
        }
    
        /**
         * Accesses our vertices as a vertex buffer object (VBO). The VBO is
         * not created until the first time this method is run.
         * Note:
         *  - This method MUST ONLY BE RUN once the model is loaded, i.e:
         *    isLoaded returns true.
         *  - This method MUST BE RUN from a thread with an OpenGL context.
         */
        GLuint getVertexBuffer(void);
    
        /**
         * Generates a placeholder model to use in absence of an available model.
         */
        static std::vector<Vertex> generateNoModelModel(void);
        
        /**
         * Reads the provided model file, returning a vector of vertices. If the
         * file could not be loaded, complaints are printed to the console, and
         * the "No model" model is loaded instead.
         * 
         * Blender export options, applicable for this model format:
         * 
         *  > Use Y Forward
         *  > Use Z Up
         *  > Untick all boxes except the following:
         *    - Apply modifiers
         *    - Include UVs
         *    - Triangulate Faces
         */
        static std::vector<Vertex> readModelFile(const std::string & filename);
    };
}

#endif
