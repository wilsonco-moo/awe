/*
 * ModelManager.cpp
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#include "ModelManager.h"

#include "Model.h"

namespace awe {

    const std::string ModelManager::NO_MODEL_FILENAME = "noModel.obj";

    ModelManager::ModelManager(threading::ThreadQueue * queue) :
        queue(queue),
        models(),
        disusedModels(),
        modelsStillLoading() {
    }
    
    ModelManager::~ModelManager(void) {
        // These are all dynamically allocated: delete them.
        for (const std::pair<std::string, Model *> & pair : models) {
            delete pair.second;
        }
    }
    
    Model * ModelManager::requestModel(const std::string & filename) {
        auto iter = models.find(filename);
        if (iter == models.end()) {
            // If we don't have a model for this filename, create and return it.
            Model * model = new Model(queue, filename);
            models[filename] = model;
            modelsStillLoading.push_back(filename);
            model->uses++;
            return model;
        } else {
            // If we *do* have a model for this filename, increment it's uses
            // and return it. Make sure it is no longer disused.
            disusedModels.erase(iter->second);
            iter->second->uses++;
            return iter->second;
        }
    }
    
    void ModelManager::abandonModel(Model * model) {
        // Decrement it's uses, and insert it into the set of disused models.
        model->uses--;
        if (model->uses == 0) {
            disusedModels.insert(model);
        }
    }
    
    unsigned int ModelManager::step(void) {
        // Delete any disused models.
        for (Model * model : disusedModels) {
            models.erase(model->getFilename());
            delete model;
        }
        disusedModels.clear();
        
        // Remove loaded models from modelsStillLoading.
        // Go through modelsStillLoading from the front, until we find one
        // which exists and hasn't loaded yet. Each time, remove the front of
        // the list.
        while(!modelsStillLoading.empty()) {
            const std::string & modelName = modelsStillLoading.front();
            auto iter = models.find(modelName);
            if (iter != models.end() && !iter->second->isLoaded()) {
                break;
            } else {
                modelsStillLoading.pop_front();
            }
        }
        return modelsStillLoading.size();
    }
    
    void ModelManager::waitForLoad(void) {
        // Make sure any disused models are removed (we don't want to wait for them).
        step();
        // Wait for all currently loading models to load, and empty modelsStillLoading.
        for (const std::string & modelName : modelsStillLoading) {
            auto iter = models.find(modelName);
            if (iter != models.end()) {
                iter->second->waitForLoad();
            }
        }
        modelsStillLoading.clear();
    }
}
