#define SHADER_VERSION #version 140
SHADER_VERSION
#undef SHADER_VERSION

// Inputs (vertex attributes).
in vec2 inPosition;
in vec4 inColour;

// Outputs (to fragment shader).
out vec4 vfColour;

// Uniforms
uniform mat3 screenToClipMatrix;

void main(void) {
    vec3 pos = screenToClipMatrix * vec3(inPosition, 1.0f);
    gl_Position = vec4(pos.xy, 0.0f, 1.0f);
    vfColour = inColour;
}
