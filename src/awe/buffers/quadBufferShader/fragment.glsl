#define SHADER_VERSION #version 140
SHADER_VERSION
#undef SHADER_VERSION

// Inputs (from fragment shader)
in vec4 vfColour;

// Outputs
out vec4 outColour;

void main(void) {
    outColour = vfColour;
}
