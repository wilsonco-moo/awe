/*
 * QuadBuffer.h
 *
 *  Created on: 27 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_BUFFERS_QUADBUFFER_H_
#define AWE_BUFFERS_QUADBUFFER_H_

#include <GL/gl.h>
#include <cstddef>
#include <vector>
#include <array>

#include "../util/colour/ColourStatus.h"
#include "../util/types/Types.h"

namespace awe {

    /**
     * Represents a vertex within QuadBuffer: A position consisting of two
     * floats, and a colour consisting of four bytes. This is used internally
     * within QuadBuffer, and is needed by the vertexData method.
     */
    class BufferVertexInfo {
    public:
        GLvec2 position;
        std::array<GLubyte, 4> colour;
    };

    /**
     * This class allows rendering of simple 2D (convex) quads, where each
     * corner has an RGBA colour and a 2D screen position. This is most helpful
     * for drawing user interfaces over the top of a scene - closely emulating
     * the functionality of legacy fixed pipeline direct draw quads.
     * 
     * The benefit of QuadBuffer is that stuff can be drawn to it once, then
     * re-drawn many times with very little overhead.
     * 
     * Note that for colour support, QuadBuffer uses ColourStatus (see
     * ColourStatus.h).
     * 
     * Note that internally each quad is drawn (and stored in buffers) as
     * two triangles - which *does* duplicate two vertices per quad. Other
     * ways to do this would be an indexed vertex buffer, which due to the
     * small size of BufferVertexInfo does not actually create any memory
     * savings. An indexed vertex buffer using triangle fans and primitive
     * restart index potentially saves 4 bytes per quad, although its additional
     * complexity likely does not warrant such a meager saving.
     */
    class QuadBuffer : public ColourStatus {
    private:
        std::vector<BufferVertexInfo> vertices;
        GLuint vertexBuffer, vao, shaderProgram;
        size_t drawVertexCount;
        
    public:
        /**
         * A shader program is required, for accessing offsets for vertex
         * array object attributes.
         */
        QuadBuffer(GLuint shaderProgram);
        virtual ~QuadBuffer(void);
        
        /**
         * Writes a vertex using an x, y position (as float values).
         * This is equivalent to the vertexv method.
         * Note that this is screen coordinates, measured from the top-left.
         */
        inline void vertex2f(GLfloat x, GLfloat y) {
            vertexv(GLvec2(x, y));
        }
        
        /**
         * Writes a vertex using a GLM vector position.
         * Note that this is screen coordinates, measured from the top-left.
         */
        void vertexv(GLvec2 position);
        
        /**
         * Adds the provided count of vertices to the buffer. The vertices
         * should be ordered as quads, in the same was as should be used with
         * the vertex2f or vertexv methods. Note that the size of the vertex
         * data should *ideally* be a multiple of four vertices, as otherwise
         * this will affect other draw calls.
         * Note that this does not affect our current colour: the colour data is
         * contained within the vertex data.
         */
        void vertexData(const BufferVertexInfo * data, size_t count);
        inline void vertexData(const std::vector<BufferVertexInfo> & data) {
            vertexData(&data[0], data.size());
        }
        
        /**
         * Copies and resets all drawn vertices to the graphics card, ready to
         * be used next time draw is called.
         * This must be called before draw, for vertices drawn using vertex2f
         * or vertexv to have any effect. 
         */
        void finalise(void);
        
        /**
         * This must be called before draw is run, and each time the view area
         * changes. This sets up the screen space to clip space transformation
         * matrix, appropriate to the view area. Note that alternatively, a
         * custom matrix can be provided using setViewToClipMatrix.
         */
        void onChangeViewArea(GLfloat width, GLfloat height);
        
        /**
         * Sets a custom view to clip matrix. This is helpful if rotation or
         * scaling, different from that which is provided by onChangeViewArea,
         * is required.
         * This sets the uniform in the shader by calling glUseProgram.
         */
        void setViewToClipMatrix(const GLmat3 & matrix);
        
        /**
         * Draws, to the screen, all vertices written to the buffer.
         * Note that for this to do anything, finalise must have been called
         * at least once.
         * Note that this assumes that the same shader specified in the
         * constructor, is currently in use.
         * Also note that depth and backface culling should be disabled before
         * running this, or 2D graphics will not display correctly.
         */
        void draw(void);
        
        /**
         * Allows access to the current number of vertices in our buffer,
         * (i.e: the current number of vertices which we will draw when our
         * draw method is called).
         */
        inline size_t getDrawVertexCount(void) const {
            return drawVertexCount;
        }
        
        /**
         * Allows access to the current number of triangles in our buffer,
         * (i.e: the current number of triangles which we will draw when our
         * draw method is called).
         * Note that each triangle is drawn by drawing three vertices.
         */
        inline size_t getDrawTriangleCount(void) const {
            return drawVertexCount / 3;
        }
        
        /**
         * Allows access to the current number of quads in our buffer,
         * (i.e: the current number of quads which we will draw when our
         * draw method is called).
         * Note that each quad is drawn by drawing two triangles.
         */
        inline size_t getDrawQuadCount(void) const {
            return drawVertexCount / 6;
        }
    };
}

#endif
