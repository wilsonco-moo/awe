/*
 * QuadBuffer.cpp
 *
 *  Created on: 27 Aug 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "QuadBuffer.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/common.hpp>
#include <iostream>

#include "../util/types/Mat3Transform.h"
#include "../util/Util.h"

namespace awe {

    QuadBuffer::QuadBuffer(GLuint shaderProgram) :
        ColourStatus(),
        vertices(),
        shaderProgram(shaderProgram),
        drawVertexCount(0) {
        
        // Make sure the compiler hasn't done anything funny with the layout
        // of BufferVertexInfo, since we need it for communicating with the
        // graphics card.
        static_assert(sizeof(GLvec2) == 8 &&
                      sizeof(std::array<GLubyte, 4>) == 4 &&
                      sizeof(BufferVertexInfo) == 12 &&
                      offsetof(BufferVertexInfo, position) == 0 &&
                      offsetof(BufferVertexInfo, colour) == 8);
        
        // Generate vertex buffer and vertex array.              
        glGenBuffers(1, &vertexBuffer);
        if (vertexBuffer == 0) { std::cerr << "WARNING: QuadBuffer: " << "Failed to generate vertex buffer.\n"; }
        
        glGenVertexArrays(1, &vao);
        if (vao == 0) { std::cerr << "WARNING: QuadBuffer: " << "Failed to generate vertex array object.\n"; }
        
        // Set up vertex array, using the shader. Note that this only has to
        // be done once: re-specifying the storage of the vertex buffer does
        // not affect associated vertex array objects.
        glBindVertexArray(vao);
        GLuint positionLoc = glGetAttribLocation(shaderProgram, "inPosition"),
               colourLoc   = glGetAttribLocation(shaderProgram, "inColour");
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glVertexAttribPointer(positionLoc, 2, GL_FLOAT,         GL_FALSE, sizeof(BufferVertexInfo), (void *)offsetof(BufferVertexInfo, position));
        glVertexAttribPointer(colourLoc,   4, GL_UNSIGNED_BYTE, GL_TRUE,  sizeof(BufferVertexInfo), (void *)offsetof(BufferVertexInfo, colour));
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glEnableVertexAttribArray(positionLoc);
        glEnableVertexAttribArray(colourLoc);
        glBindVertexArray(0);
    }
    
    QuadBuffer::~QuadBuffer(void) {
        glDeleteBuffers(1, &vertexBuffer);
        glDeleteVertexArrays(1, &vao);
    }
    
    void QuadBuffer::vertexv(GLvec2 position) {
        vertices.push_back({position, getCurrentColour()});
        size_t vertexID = vertices.size() % 6;
        
        // For third vertex, duplicate it. This starts the second triangle
        // of the quad with the last vertex of the first triangle.
        if (vertexID == 3) {
            vertices.push_back({position, getCurrentColour()});
        
        // For last vertex, follow it with a duplicate of the first vertex in
        // the quad. This ends the second triangle with the first vertex of the
        // first triangle.
        } else if (vertexID == 5) {
            vertices.push_back(vertices[vertices.size() - 5]);
        }
    }
    
    void QuadBuffer::vertexData(const BufferVertexInfo * data, size_t count) {
        for (size_t i = 0; i < count; i++) {
            vertices.push_back(data[i]);
            
            // Duplicate vertices appropriately (see vertexv).
            size_t vertexID = vertices.size() % 6;
            if (vertexID == 3) {
                vertices.push_back(data[i]);
            } else if (vertexID == 5) {
                vertices.push_back(vertices[vertices.size() - 5]);
            }
        }
    }
    
    void QuadBuffer::finalise(void) {
        // Round down vertex count to nearest multiple of six: Don't draw
        // any incomplete quads, even if the user left a quad incomplete.
        drawVertexCount = (vertices.size() / 6) * 6;
        
        // Only do stuff with the buffer if we actually have some vertices.
        if (drawVertexCount != 0) {
            size_t bufferSize = drawVertexCount * sizeof(BufferVertexInfo);
            
            // Re-specify the buffer storage, using the new vertex data. Note that
            // when streaming vertex data, this is actually a fast way to do it,
            // since it does not introduce any implicit synchronisation points.
            // It also means that the OpenGL implementation can take care of
            // memory allocation and sizing for us. See the following:
            // https://www.khronos.org/opengl/wiki/Buffer_Object_Streaming
            // http://web.archive.org/web/20200501181851/https://www.khronos.org/opengl/wiki/Buffer_Object_Streaming
            glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
            glBufferData(GL_ARRAY_BUFFER, bufferSize, &vertices[0], GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        
        vertices.clear();
    }
    
    void QuadBuffer::onChangeViewArea(GLfloat width, GLfloat height) {
        // Assuming coordinates lie 0-screenSize, first scale so they lie
        // within 0-2, then translate so they lie within -1 - +1.
        // Note that clip coordinate 0,0 is bottom left (inverted). This means
        // we scale and translate negatively in the y direction.
        setViewToClipMatrix(
            Mat3Transform::translate(-1, 1) *
            Mat3Transform::scale(2.0f / width, -2.0f / height)
        );
    }

    void QuadBuffer::setViewToClipMatrix(const GLmat3 & matrix) {
        glUseProgram(shaderProgram);
        GLuint matrixLoc = Util::getUniformLocationChecked(shaderProgram, "Quad buffer", "screenToClipMatrix");
        glUniformMatrix3fv(matrixLoc, 1, GL_FALSE, glm::value_ptr(matrix));
        glUseProgram(0);
    }
    
    void QuadBuffer::draw(void) {
        if (drawVertexCount != 0) {
            glBindVertexArray(vao);
            glDrawArrays(GL_TRIANGLES, 0, drawVertexCount);
            glBindVertexArray(0);
        }
    }
}
