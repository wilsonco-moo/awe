/*
 * TextureManager.h
 *
 *  Created on: 8 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_TEXTURE_TEXTUREMANAGER_H_
#define AWE_TEXTURE_TEXTUREMANAGER_H_

#include <unordered_map>
#include <unordered_set>
#include <GL/gl.h>
#include <cstddef>
#include <vector>
#include <string>
#include <atomic>

#include "MipMapTexture.h"

namespace threading {
    class ThreadQueue;
}

namespace awe {

    /**
     * TextureManager is a class which aims to convenient manage the loading
     * and storage of textures. Access is allowed to these, by assembling a
     * mipmapped array texture, containing all of these textures.
     * The step function must be called each frame, before anything is drawn
     * using this system. Textures are automatically bound to
     * GL_TEXTURE_2D_ARRAY.
     * 
     * Textures are loaded by filename, and all processing thereof is handled
     * by TextureManager.
     * 
     * All textures are stored in an UNSIGNED_BYTE RGBA format.
     * 
     * NOTE: There is no provided way to wait for all textures to finish
     *       loading. This is intentional: It is assumed that the program should
     *       continue running while loading textures asynchronously.
     *       While they are loading, TextureManager will automatically allow
     *       placeholder textures to be used instead,
     *       through the use of TextureManager::getTextureLayer.
     * 
     * NOTE: Access to this class is not thread safe. Access should either be
     *       synchronised outside this class, or done from only one thread.
     */
    class TextureManager {
    public:
        /**
         * This represents the number of channels we use. 4 for RGBA, 3 for RGB etc.
         */
        static const unsigned int CHANNELS;
        /**
         * The default size for "no texture" textures.
         */
        static const unsigned int DEFAULT_NO_TEXTURE_TEXTURE_SIZE;
            
        /**
         * This should be used as a filename if there is no applicable texture
         * to load. It is assumed that this filename will always return a
         * "no texture" texture.
         */
        static const std::string NO_TEXTURE_FILENAME;
        
    private:
        
        // A thread queue for executing background operations.
        threading::ThreadQueue * queue;
        // The size of all textures we hold. Any non-conforming textures will
        // be scaled accordingly.
        unsigned int baseTexSize;
        
        // Stores all textures that are currently active, by their texture names.
        std::unordered_map<std::string, MipMapTexture *> internalTextures;
        
        // Stores all texture names which have no uses.
        std::unordered_set<std::string> disusedTextures;
        
        // Our current texture ID, (the one in general use), and the prospective
        // texture ID, (the one we are writing to alongside).
        // textureId will be zero until the first load completes.
        // prospectiveTextureId will be non-zero whenever loading, then zero
        // when loading has completed, (when it has copied over to textureId).
        GLuint textureId,
               prospectiveTextureId;
               
        // resetLoading causes loading to be reset next time step is
        // called. loading is true while textures are loading.
        bool resetLoading, loading;
        
        // The order in which textures are ordered for load operations.
        std::vector<MipMapTexture *> prospectiveOrder;
        // Representations of where we are up to in loading process.
        // textureStoreUpTo and textureStoreMipmapUpTo respectively represent
        // the next texture and mipmap level to copy to the graphics card.
        unsigned int textureStoreUpTo, textureStoreMipmapUpTo;

        // This is simply an estimate of where we are upto in the load progress.
        // It is incremented each time we add a MipMapTexture, and
        // decremented each time a MipMapTexture finishes.
        std::atomic<unsigned int> loadRemaining;

        // Don't allow copying: We hold raw resources.
        TextureManager(const TextureManager & other);
        TextureManager & operator = (const TextureManager & other);

    public:
        /**
         * A Thread queue is required for background jobs.
         * A base texture size is also required. If this is not a power of two, we complain and exit.
         */
        TextureManager(threading::ThreadQueue * queue, unsigned int baseTexSize);
        virtual ~TextureManager(void);
    
    private:
        // ------------------ Internal methods ---------------------------------
        
        // Removes from memory all disused textures, and requests a texture
        // rebuild if necessary.
        void removeDisusedTextures(void);
        
        // Should be called by step if resetLoading is set.
        void doResetLoading(void);
        // Should be called by step if loading, and prospectiveTextureId
        // is 0.
        void initLoading(void);
        // Should be run by step during loading.
        void doLoading(unsigned int maxTime);
        // Erases the current textureId if necessary, swaps it for prospective
        // texture, sets layer of all internal textures, and sets prospective
        // texture id to zero.
        void runLoadCompletion(void);
        
    public:
        // ------------ Utility methods (In TextureManagerUtil.cpp) ------------
        
        /**
         * Returns true only if the number is a power of two.
         */
        static bool isNumberPowerOfTwo(unsigned long long value);
        
        /**
         * Assuming the provided number is a power of two, returns log base 2 of
         * the provided number. For example:
         *   simpleLog(1) = 0
         *   simpleLog(32) = 5
         *   simpleLog(256) = 8
         */
        static unsigned int simpleLog(unsigned int value);
        
        /**
         * For the specified base texture size, (assumed to be a power of two),
         * returns the required number of mipmap levels.
         */
        inline static unsigned int getMipMapLevels(unsigned int baseTexSize) {
            return simpleLog(baseTexSize) + 1;
        }
        
        /**
         * Returns, for a texture of the specified size, the number of bytes
         * required to store it.
         */
        static inline size_t getSingleTexAllocSize(unsigned int textureSize) {
            return ((size_t)textureSize) * ((size_t)textureSize) * ((size_t)CHANNELS);
        }
        
        /**
         * Returns, for the specified texture size, (assumed to be a power of
         * two), the amount of data that must be allocated to store all mipmap
         * levels, assuming CHANNELS bytes per pixel.
         */
        static size_t getMipMapTexAllocSize(unsigned int baseTexSize);
        
        /**
         * Returns the offsets, for each mipmap level, of the start of the data
         * to store each image, within an allocation assumed to be of size
         * getMipMapTexAllocSize(baseTexSize).
         */
        static std::vector<size_t> getMipMapOffsets(unsigned int baseTexSize);
        
        /**
         * Returns the sizes of the textures at each mipmap level.
         */
        static std::vector<unsigned int> getMipMapTextureSizes(unsigned int baseTexSize);
        
        /**
         * Returns the size of the specified mipmap level. If this is called in a loop,
         * use getMipMapTextureSizes instead as it is faster.
         */
        static unsigned int getMipMapTextureSize(unsigned int baseTexSize, unsigned int level);
        
        /**
         * Scales an image, reading from input and writing to output.
         * If the sizes are not powers of two, this complains and does nothing.
         */
        static void scaleImage(unsigned char * output, unsigned int outputSize, unsigned char * input, unsigned int inputSize);
        
        /**
         * Generates a "no texture" texture of a specific size.
         */
        static unsigned char * generateNoTextureTexture(unsigned int size);
        
        // ------------------------ Public API ---------------------------------
        
        /**
         * Requests the load of the specified texture filename. This returns a
         * texture token, which can be later abandoned, or used to access the
         * texture ID. This should be called exactly once for each object
         * using the texture.
         * Note that the texture token *is* in fact just a MipMapTexture.
         * Access to the token is purposefully opaque, since it must only be
         * modified internally by the TextureManager. This also provides
         * flexibility for TextureManager to use a different system in the
         * future, if required.
         */
        void * requestTexture(const std::string & filename);
        
        /**
         * Abandons the use of the texture. This MUST only be called ONCE per
         * call to requestTexture. Once this has been called, the specified
         * token must NEVER be used again.
         * Once all uses of a texture have been abandoned, the texture will be
         * removed from memory next time step is called.
         */
        void abandonTexture(void * textureToken);
        
        /**
         * This must be called each frame that the texture is used. This returns
         * the layer within the currently bound array texture, represented by
         * the provided texture token.
         * This value can change frame-to-frame as the texture is loaded, or
         * more textures are loaded and the array texture is rebuilt on the fly.
         */
        inline static GLuint getTextureLayer(void * textureToken) {
            return ((MipMapTexture *)textureToken)->getTextureLayer();
        }
        
        /**
         * This should be called each frame.
         * After this operation, a texture will always be bound to
         * GL_TEXTURE_2D_ARRAY. This updates the loaded textures, and deals with
         * GPU data accordingly, but never does too much at once.
         * 
         * Provided is the maximum time (milliseconds) that this operation
         * should be allowed to take. Returned is an estimate of the number of
         * pending load operations.
         */
        unsigned int step(unsigned int maxTime);
        
        /**
         * Returns true are there is currently any background operations going
         * on to load textures.
         */
        inline bool isLoadOngoing(void) const {
            return loading;
        }
    };
}

#endif
