/*
 * TextureManager.cpp
 *
 *  Created on: 8 Jan 2020
 *      Author: wilson
 */

#include "TextureManager.h"

#include <wool/texture/lodepng/lodepng.h>
#include <iostream>
#include <cstdlib>
#include <cstring>

#include "../util/colour/Channels.h"

namespace awe {

    const unsigned int TextureManager::CHANNELS = 4;
    const unsigned int TextureManager::DEFAULT_NO_TEXTURE_TEXTURE_SIZE = 32;
    const std::string TextureManager::NO_TEXTURE_FILENAME = "noTexture.png";

    bool TextureManager::isNumberPowerOfTwo(unsigned long long value) {
        int onesFound = 0;
        for (unsigned long long mask = 1; mask != 0; mask <<= 1) {
            if ((value & mask) != 0) {
                onesFound++;
            }
        }
        return onesFound == 1;
    }
    
    unsigned int TextureManager::simpleLog(unsigned int value) {
        unsigned int count = 0;
        while(value > 1) {
            value >>= 1;
            count++;
        }
        return count;
    }
    
    size_t TextureManager::getMipMapTexAllocSize(unsigned int baseTexSize) {
        size_t allocationSize = 0;
        for (size_t levelTexSize = 1; levelTexSize <= baseTexSize; levelTexSize <<= 1) {
            allocationSize += getSingleTexAllocSize(levelTexSize);
        }
        return allocationSize;
    }
    
    std::vector<size_t> TextureManager::getMipMapOffsets(unsigned int baseTexSize) {
        // Work out levels, reserve appropriately.
        std::vector<size_t> offsets;
        unsigned int levels = getMipMapLevels(baseTexSize);
        offsets.reserve(levels);
        // Go through each level. Each time increment offset by current texture allocation size.
        size_t offset = 0;
        unsigned int currentTexSize = baseTexSize;
        for (unsigned int i = 0; i < levels; i++) {
            offsets.push_back(offset);
            offset += getSingleTexAllocSize(currentTexSize);
            currentTexSize >>= 1;
        }
        return offsets;
    }
    
    std::vector<unsigned int> TextureManager::getMipMapTextureSizes(unsigned int baseTexSize) {
        // Work out levels, reserve appropriately.
        std::vector<unsigned int> sizes;
        unsigned int levels = getMipMapLevels(baseTexSize);
        sizes.reserve(levels);
        // Go through each level, populate vector.
        unsigned int currentTexSize = baseTexSize;
        for (unsigned int i = 0; i < levels; i++) {
            sizes.push_back(currentTexSize);
            currentTexSize >>= 1;
        }
        return sizes;
    }
    
    unsigned int TextureManager::getMipMapTextureSize(unsigned int texSize, unsigned int level) {
        for (unsigned int i = 0; i < level; i++) {
            texSize >>= 1;
        }
        return texSize;
    }

    void TextureManager::scaleImage(unsigned char * output, unsigned int outputSize, unsigned char * input, unsigned int inputSize) {
        if (!isNumberPowerOfTwo(outputSize)) {
            std::cerr << "WARNING: TextureManager: Non power of two scale output size " << outputSize << " requested.\n";
            return;
        }
        if (!isNumberPowerOfTwo(inputSize)) {
            std::cerr << "WARNING: TextureManager: Non power of two scale input size " << inputSize << " requested.\n";
            return;
        }
        
        // If no scaling is needed, just copy data.
        if (inputSize == outputSize) {
            memcpy(output, input, outputSize * outputSize * CHANNELS);
        
        } else if (outputSize > inputSize) {
            // If scaling up (making image bigger).
            unsigned char * inputPos = input;
            unsigned int multiplier = outputSize / inputSize;
            // Iterate through each input pixel.
            for (unsigned int inputY = 0; inputY < inputSize; inputY++) {
                for (unsigned int inputX = 0; inputX < inputSize; inputX++) {
                    // Offsets to generate right and below pixels: Don't go off edge.
                    unsigned int xOff = (inputX == inputSize - 1 ? 0 : 1) * CHANNELS,
                                 yOff = (inputY == inputSize - 1 ? 0 : 1) * CHANNELS * inputSize;
                    // Find 4 pixels (one per channel).
                    #define X(channelName)                                     \
                        unsigned int channelName[4] = {                        \
                            *(inputPos + Channels::channelName),               \
                            *(inputPos + Channels::channelName + xOff),        \
                            *(inputPos + Channels::channelName + yOff),        \
                            *(inputPos + Channels::channelName + xOff + yOff)  \
                        };
                    CHANNELS_CHANNEL_NAMES_DEF
                    #undef X
                    inputPos += CHANNELS;
                    // Iterate through each output pixel which will be generated.
                    unsigned int outputXBase = inputX * multiplier,
                                 outputYBase = inputY * multiplier;
                    for (unsigned int offY = 0; offY < multiplier; offY++) {
                        for (unsigned int offX = 0; offX < multiplier; offX++) {
                            // Work out output location.
                            unsigned int outputX = outputXBase + offX,
                                         outputY = outputYBase + offY;
                            unsigned char * outputLoc = output + (outputY * outputSize + outputX) * CHANNELS;
                            // For each channel, work out weighted average of 4 pixels, and write to output.
                            #define X(channelName) {                                                                                       \
                                unsigned int topAvg    = ((channelName[0] * (multiplier - offX)) + (channelName[1] * offX)) / multiplier,  \
                                             bottomAvg = ((channelName[2] * (multiplier - offX)) + (channelName[3] * offX)) / multiplier;  \
                                *(outputLoc + Channels::channelName) = ((topAvg * (multiplier - offY)) + (bottomAvg * offY)) / multiplier; \
                            }
                            CHANNELS_CHANNEL_NAMES_DEF
                            #undef X
                        }
                    }
                }
            }
        } else {
            // If scaling down (making image smaller).
            unsigned char * outputPos = output;
            unsigned int divisor = inputSize / outputSize,
                         divisorSqr = divisor * divisor;
            // Iterate through each output pixel.
            for (unsigned int outputY = 0; outputY < outputSize; outputY++) {
                for (unsigned int outputX = 0; outputX < outputSize; outputX++) {
                    // Declare variables for each channel's total.
                    #define X(channelName) unsigned long long channelName = 0;
                    CHANNELS_CHANNEL_NAMES_DEF
                    #undef X
                    // Iterate through each source input pixel.
                    unsigned int inputXBase = outputX * divisor,
                                 inputYBase = outputY * divisor;
                    for (unsigned int offY = 0; offY < divisor; offY++) {
                        for (unsigned int offX = 0; offX < divisor; offX++) {
                            // Work out output location.
                            unsigned int inputX = inputXBase + offX,
                                         inputY = inputYBase + offY;
                            unsigned char * inputLoc = input + (inputY * inputSize + inputX) * CHANNELS;
                            // Add each channel onto total.
                            #define X(channelName) channelName += *(inputLoc + Channels::channelName);
                            CHANNELS_CHANNEL_NAMES_DEF
                            #undef X
                        }
                    }
                    // Write the average for each channel.
                    #define X(channelName) *(outputPos + Channels::channelName) = (unsigned char)(channelName / divisorSqr);
                    CHANNELS_CHANNEL_NAMES_DEF
                    #undef X
                    outputPos += CHANNELS;
                }
            }
        }
    }
    
    unsigned char * TextureManager::generateNoTextureTexture(unsigned int size) {
        unsigned char * textureData = (unsigned char *)malloc(getSingleTexAllocSize(size));
        if (textureData == NULL) {
            std::cerr << "ERROR: Failed to allocate no texture texture.\n";
            exit(EXIT_FAILURE);
        }
        // Populate it with a grid pattern "no texture" texture.
        unsigned char * textureDataUpTo = textureData;
        for (unsigned int iy = 0; iy < size; iy++) {
            for (unsigned int ix = 0; ix < size; ix++) {
                if ((iy % 8 < 4 && ix % 8 < 4) || (iy % 8 >= 4 && ix % 8 >= 4)) {
                    #define X(channelName, colour1, colour2) *(textureDataUpTo + Channels::channelName) = colour1;
                    CHANNELS_NO_TEXTURE_TEXTURE_COLOURS
                    #undef X
                } else {
                    #define X(channelName, colour1, colour2) *(textureDataUpTo + Channels::channelName) = colour2;
                    CHANNELS_NO_TEXTURE_TEXTURE_COLOURS
                    #undef X
                }
                textureDataUpTo += CHANNELS;
            }
        }
        return textureData;
    }
}
