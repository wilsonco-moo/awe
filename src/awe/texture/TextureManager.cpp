/*
 * TextureManager.cpp
 *
 *  Created on: 8 Jan 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "TextureManager.h"

#include <wool/texture/lodepng/lodepng.h>
#include <threading/ThreadQueue.h>
#include <iostream>
#include <cstdlib>
#include <chrono>

namespace awe {
    
    TextureManager::TextureManager(threading::ThreadQueue * queue, unsigned int baseTexSize) :
        queue(queue),
        baseTexSize(baseTexSize),
        internalTextures(),
        disusedTextures(),
        textureId(0),
        prospectiveTextureId(0),
        resetLoading(false),
        loading(false),
        prospectiveOrder(),
        textureStoreUpTo(0),
        textureStoreMipmapUpTo(0),
        loadRemaining(0) {
        
        if (!isNumberPowerOfTwo(baseTexSize)) {
            std::cerr << "ERROR: TextureManager constructor: " << baseTexSize << " is NOT a power of two.\n";
            exit(EXIT_FAILURE);
        }
    }
    
    TextureManager::~TextureManager(void) {
        // Delete prospective and normal texture IDs if they exist.
        if (prospectiveTextureId != 0) glDeleteTextures(1, &prospectiveTextureId);
        if (textureId != 0) glDeleteTextures(1, &textureId);
        // Delete internal textures.
        for (const std::pair<std::string, MipMapTexture *> & pair : internalTextures) {
            delete pair.second;
        }
    }
    
    void TextureManager::removeDisusedTextures(void) {
        // Do nothing and return if there are no disused textures.
        if (disusedTextures.empty()) return;
        // Delete and erase from data structures, all disused textures.
        for (const std::string & name : disusedTextures) {
            auto iter = internalTextures.find(name);
            delete iter->second;
            internalTextures.erase(iter);
        }
        // Clear disused textures set, and reset loading, since textures have changed.
        disusedTextures.clear();
        resetLoading = true;
    }

    void TextureManager::doResetLoading(void) {
        resetLoading = false;
        // Delete prospective texture if it exists.
        if (prospectiveTextureId != 0) {
            glDeleteTextures(1, &prospectiveTextureId);
            prospectiveTextureId = 0;
        }
        // Clear load progress.
        prospectiveOrder.clear();
        textureStoreUpTo = 0;
        textureStoreMipmapUpTo = 0;
        // Set to loading if there are internal textures.
        loading = !internalTextures.empty();
    }

    void TextureManager::initLoading(void) {
        // Generate prospective order.
        for (const std::pair<std::string, MipMapTexture *> & pair : internalTextures) {
            prospectiveOrder.push_back(pair.second);
        }
        
        // Generate a texture name.
        glGenTextures(1, &prospectiveTextureId);
        // Initialise the texture as a 2d texture array, and bind.
        glBindTexture(GL_TEXTURE_2D_ARRAY, prospectiveTextureId);
        
        // Set texture parameters.
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        
        // Allocate memory for the texture array. Repeat this for each mipmap level.
        unsigned int level = 0;
        for (unsigned int mipMapSize : getMipMapTextureSizes(baseTexSize)) {
            glTexImage3D(
                GL_TEXTURE_2D_ARRAY,      // Target
                level,                    // Mipmap level to specify.
                GL_RGBA8,                 // Internal format
                mipMapSize, mipMapSize,   // Size of this mipmap level (width, height)
                prospectiveOrder.size(),  // Depth (number of textures in array): One for each texture we have.
                0,                        // Border (nobody knows what it does, but must be 0).
                GL_RGBA,                  // General format of pixel data (layout)
                GL_UNSIGNED_BYTE,         // Data type of pixel data
                NULL                      // Initial data (specify as null since we provide the data later with glTexSubImage3D.
            );
            level++;
        }
        
        // It no longer needs to be bound.
        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    }
    
    void TextureManager::doLoading(unsigned int maxTime) {
        // Bind the texture so we know what to load data into.
        glBindTexture(GL_TEXTURE_2D_ARRAY, prospectiveTextureId);
        // Iterate until we run out of textures or exceed max time.
        unsigned int levels = getMipMapLevels(baseTexSize);
        std::chrono::time_point<std::chrono::steady_clock> startTime = std::chrono::steady_clock::now();
        while(textureStoreUpTo < prospectiveOrder.size() && std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startTime).count() <= maxTime) {
            
            // Get texture at this position, end loop if it isn't loaded yet.
            MipMapTexture * tex = prospectiveOrder[textureStoreUpTo];
            if (!tex->isLoaded()) break;
            
            // Tell the texture to load the mipmap level into the gpu.
            tex->loadMipMapLevelIntoGpu(textureStoreUpTo, textureStoreMipmapUpTo);
            
            // Increment upto variables, (iterate textureStoreMipmapUpTo, when that reaches end loop around and increment textureStoreUpTo).
            if (textureStoreMipmapUpTo == levels - 1) {
                textureStoreMipmapUpTo = 0;
                textureStoreUpTo++;
            } else {
                textureStoreMipmapUpTo++;
            }
        }
        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    }
    
    void TextureManager::runLoadCompletion(void) {
        // Delete old texture if it exists.
        if (textureId != 0) {
            glDeleteTextures(1, &textureId);
        }
        // Use prospectiveTextureId as new texture.
        textureId = prospectiveTextureId;
        prospectiveTextureId = 0;
        // Set to false since loading completed.
        loading = false;
        // Set the texture layer id of each internal MipMapTexture.
        unsigned int textureLayerId = 0;
        for (MipMapTexture * tex : prospectiveOrder) {
            tex->setTextureLayer(textureLayerId);
            textureLayerId++;
        }
    }

    void * TextureManager::requestTexture(const std::string & filename) {
        auto iter = internalTextures.find(filename);
        if (iter == internalTextures.end()) {
            // If the texture does not exist, create it, add it to internal textures, and return it.
            MipMapTexture * tex = new MipMapTexture(queue, filename, baseTexSize, &loadRemaining);
            internalTextures[filename] = tex;
            resetLoading = true; // Reset loading since a new texture has been requested.
            return tex;
        } else {
            // If the texture already exists, add a use and return it. Make sure the texture is
            // no longer disused.
            disusedTextures.erase(filename);
            iter->second->addUse();
            return iter->second;
        }
    }
    
    void TextureManager::abandonTexture(void * textureToken) {
        MipMapTexture * texture = (MipMapTexture *)textureToken;
        texture->removeUse();
        if (!texture->isUsed()) {
            // If the texture is disused, add it to the set of disused textures.
            disusedTextures.insert(texture->getTextureName());
        }
    }
    
    unsigned int TextureManager::step(unsigned int maxTime) {
        removeDisusedTextures();
        if (resetLoading) {
            doResetLoading();
        }
        if (loading) {
            if (prospectiveTextureId == 0) {
                initLoading();
            }
            doLoading(maxTime);
            if (textureStoreUpTo == prospectiveOrder.size()) {
                runLoadCompletion();
            }
        }
        glBindTexture(GL_TEXTURE_2D_ARRAY, textureId);
        return loadRemaining.load(std::memory_order_acquire) + (loading ? 1 : 0);
    }
}
