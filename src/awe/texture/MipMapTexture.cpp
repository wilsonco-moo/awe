/*
 * MipMapTexture.cpp
 *
 *  Created on: 16 Jul 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "MipMapTexture.h"

#include <wool/texture/lodepng/lodepng.h>
#include <threading/ThreadQueue.h>
#include <functional>
#include <iostream>
#include <cstdlib>

#include "TextureManager.h"

namespace awe {

    MipMapTexture::MipMapTexture(threading::ThreadQueue * queue, const std::string textureName, unsigned int baseTexSize, std::atomic<unsigned int> * loadRemaining) :
        loadRemaining(loadRemaining),
        queue(queue),
        token(queue->createToken()),
        baseTexSize(baseTexSize),
        allocation(NULL),
        levels(),
        textureName(textureName),
        internalLoaded(false),
        uses(1),
        layer(0) {
        
        // Make sure the base texture size is a power of two.
        if (!TextureManager::isNumberPowerOfTwo(baseTexSize)) {
            std::cerr << "ERROR: MipMapTexture constructor: " << baseTexSize << " is NOT a power of two.\n";
            exit(EXIT_FAILURE);
        }
        
        // Increment the number of remaining to load (if an atomic int was supplied
        // by the user), since we're starting our load operation.
        if (loadRemaining != NULL) {
            loadRemaining->fetch_add(1, std::memory_order_release);
        }
        
        // Perform loading in the background.
        loadJobId = queue->add(token, [this](void) {
            performLoad();
        });
    }
    
    MipMapTexture::~MipMapTexture(void) {
        queue->deleteToken(token);
        free(allocation);
    }
    
    void MipMapTexture::performLoad(void) {
        
        // Load texture.
        unsigned char * loadedTextureData = NULL;
        unsigned int loadedTextureSize;
        
        {
            unsigned int loadedTextureHeight;
            unsigned int error = lodepng_decode32_file(&loadedTextureData, &loadedTextureSize, &loadedTextureHeight, textureName.c_str());
            
            // If there was an error print a complaint.
            if (error != 0 || loadedTextureData == NULL) {
                std::cerr << "WARNING: Failed to load texture: " << textureName << ".\n";
            
            // If image is not square, we have an error.
            } else if (loadedTextureSize != loadedTextureHeight) {
                std::cerr << "WARNING: Texture " << textureName << " is not square: (" << loadedTextureSize << 'x' << loadedTextureHeight << ")\n";
                error = 1;
            
            // If image size is not a power of two, it is also an error.
            } else if (!TextureManager::isNumberPowerOfTwo(loadedTextureSize)) {
                std::cerr << "WARNING: Texture " << textureName << " has dimensions which are not powers of two: (" << loadedTextureSize << 'x' << loadedTextureHeight << ")\n";
                error = 1;
            }
            
            // If an error happened, use the "no texture" texture.
            if (error != 0 || loadedTextureData == NULL) {
                free(loadedTextureData);
                // Use the "no texture" texture.
                loadedTextureSize = TextureManager::DEFAULT_NO_TEXTURE_TEXTURE_SIZE;
                loadedTextureData = TextureManager::generateNoTextureTexture(loadedTextureSize);
            }
        }
        
        // After this point, we assume that we have a valid, power of two, square texture,
        // of size loadedTextureSize.
        // Allocate an internal block of data for the texture.
        unsigned char * internalAllocation = (unsigned char *)malloc(TextureManager::getMipMapTexAllocSize(baseTexSize));
        if (internalAllocation == NULL) {
            std::cerr << "ERROR: Failed to allocate memory for texture " << textureName << ".\n";
            exit(EXIT_FAILURE);
        }
        // Populate a vector of pointers to appropriate places within this allocation.
        std::vector<unsigned char *> internalLevels;
        internalLevels.reserve(TextureManager::getMipMapLevels(baseTexSize));
        for (size_t offset : TextureManager::getMipMapOffsets(baseTexSize)) {
            internalLevels.push_back(internalAllocation + offset);
        }
        // Populate the allocation with scaled versions of our original image.
        unsigned int level = 0;
        for (unsigned int textureSize : TextureManager::getMipMapTextureSizes(baseTexSize)) {
            // If first level, or making image bigger, scale image from original loaded texture.
            if (textureSize >= loadedTextureSize || level == 0) {
                TextureManager::scaleImage(internalLevels[level], textureSize, loadedTextureData, loadedTextureSize);
            // Otherwise, scale image from previous level, as that is faster to do.
            } else {
                TextureManager::scaleImage(internalLevels[level], textureSize, internalLevels[level - 1], textureSize * 2);
            }
            level++;
        }
        // We don't need the original allocation any more.
        free(loadedTextureData);
        // Store results.
        allocation = internalAllocation;
        levels = std::move(internalLevels);
        // Decrement the number of remaining to load (if an atomic int was supplied
        // by the user), since we're finishing loading.
        if (loadRemaining != NULL) {
            loadRemaining->fetch_sub(1, std::memory_order_release);
        }
        
        std::cout << "Loaded " << textureName << "\n";
    }
    
    void MipMapTexture::loadMipMapLevelIntoGpu(unsigned int textureLayer, unsigned int mipMapLevel) {
        unsigned int size = TextureManager::getMipMapTextureSize(baseTexSize, mipMapLevel);
        glTexSubImage3D(
            GL_TEXTURE_2D_ARRAY, // Texture target.
            mipMapLevel,         // Detail level for mipmapping.
            0, 0,                // Offset (x,y).
            textureLayer,        // Offset (z): Texture layer to load into.
            size, size,          // Size (width, height).
            1,                   // Depth of data to add (1 since we only do one mipmap level at once.
            GL_RGBA,             // Pixel format
            GL_UNSIGNED_BYTE,    // Pixel type
            levels[mipMapLevel]  // Pixel data
        );
    }
    
    void MipMapTexture::loadAllMipMapLevelsIntoGpu(unsigned int textureLayer) {
        unsigned int levels = TextureManager::getMipMapLevels(baseTexSize);
        for (unsigned int i = 0; i < levels; i++) {
            loadMipMapLevelIntoGpu(textureLayer, i);
        }
    }
    
    bool MipMapTexture::isLoaded(void) {
        if (!internalLoaded) {
            internalLoaded = (queue->getJobStatus(loadJobId) == threading::JobStatus::complete);
        }
        return internalLoaded;
    }
}
