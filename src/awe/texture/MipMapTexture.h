/*
 * MipMapTexture.h
 *
 *  Created on: 16 Jul 2020
 *      Author: wilson
 */

#ifndef AWE_TEXTURE_MIPMAPTEXTURE_H_
#define AWE_TEXTURE_MIPMAPTEXTURE_H_

#include <GL/gl.h>
#include <cstdint>
#include <cstddef>
#include <atomic>
#include <vector>
#include <string>

namespace threading {
    class ThreadQueue;
}

namespace awe {

    /**
     * This class manages asynchronous loading and mipmap calculation of a
     * single texture (layer), for use within a 2D texture array. If loading
     * fails, a "no texture" texture is generated. This class is used internally
     * within TextureManager, but can be easily used on its own.
     * 
     * For convenience within TextureManager, this tracks the number of uses of
     * the texture. NOTE THIS STARTS AT 1, since we assume one use is needed for
     * a texture to be created in the first place.
     */
    class MipMapTexture {
    private:
        // This exists so that users can keep track of how many textures have
        // finished loading. Note that this is optional: It will only be used
        // if a non-NULL value is supplied.
        std::atomic<unsigned int> * loadRemaining;
        // Our related thread queue (used for asynchronous load operations).
        threading::ThreadQueue * queue;
        // Our thread queue token.
        void * token;
        // The base texture size (width and height of largest mipmap level).
        unsigned int baseTexSize;
        // Our RAM based texture allocation.
        unsigned char * allocation;
        // Pointers to places within our allocation, where each mipmap level resides (for convenience).
        std::vector<unsigned char *> levels;
        // The name of our texture (stored for loading).
        std::string textureName;
        // The ID of our thread queue load job, so we can check whether we have loaded.
        uint64_t loadJobId;
        // Set to true after the first time we check loadJobId and it has completed.
        // Saves asking the ThreadQueue *every* time, as that requires mutexes, map lookups, etc.
        bool internalLoaded;
        // Texture use count (this is used by TextureManager, and only accessed/modified with inline methods below).
        size_t uses;
        // Texture layer ID (this is used by TextureManager, and only accessed/modified with inline methods below).
        GLuint layer;
        // Don't allow copying: We hold raw resources.
        MipMapTexture(const MipMapTexture & other);
        MipMapTexture & operator = (const MipMapTexture & other);
    public:
        
        /**
         * The main constructor for MipMapTexture.
         *  - A thread queue is provided, since MipMapTexture performs load
         *    operations and texture scaling asynchronously.
         *  - The provided name should be the filename to load the texture from.
         *  - The base texture size should be the expected width and height of
         *    the texture. If the loaded texture does not match this, a
         *    "no texture" texture is used instead.
         *  - A pointer to an atomic unsigned int can be optionally supplied.
         *    This is incremented when we begin our asynchronous loading
         *    operation, and decremented (from a separate thread) when we
         *    finish. This allows a user to keep track of how many textures
         *    are remaining to load. This is not used if NULL is provided.
         */
        MipMapTexture(threading::ThreadQueue * queue, const std::string textureName,
                      unsigned int baseTexSize, std::atomic<unsigned int> * loadRemaining = NULL);
        ~MipMapTexture(void);
        
    private:
        // Our asynchronous loading procedure - we run this in the thread queue.
        // This performs all allocations and scaling operations, then saves
        // them and decrements loadRemaining (if supplied).
        void performLoad(void);
        
    public:
        /**
         * Copies the texture data from our mipmap data of the specified level,
         * to the specified texture layer of the currently bound 2D texture
         * array (GL_TEXTURE_2D_ARRAY). The ability to load each mipmap level
         * individually is provided, so users of this class can spread load
         * operations across multiple frames, in order to reduce freezing.
         * 
         * Note that this method MUST only be used AFTER we have finished
         * loading.
         */
        void loadMipMapLevelIntoGpu(unsigned int textureLayer, unsigned int mipMapLevel);
        
        /**
         * Loads all of our mipmap levels into the currently bound 2D texture
         * array (GL_TEXTURE_2D_ARRAY), into the specified texture array layer.
         * This is equivalent to calling loadMipMapLevelIntoGpu for all valid
         * mipmap levels.
         */
        void loadAllMipMapLevelsIntoGpu(unsigned int textureLayer);
        
        /** 
         * Returns true if the texture has completed loading. This is done by checking
         * to see if our load job has finished.
         */
        bool isLoaded(void);
        
        /**
         * Accesses the name of the texture.
         */
        inline const std::string & getTextureName(void) const {
            return textureName;
        }
        
        /**
         * Adds an additional use, used internally by TextureManager.
         */
        inline void addUse(void) {
            uses++;
        }
        
        /**
         * Removes a use, used internally by TextureManager.
         */
        inline void removeUse(void) {
            uses--;
        }
        
        /**
         * Returns true if this texture is used by at least one thing,
         * used internally by TextureManager.
         */
        inline bool isUsed(void) const {
            return uses > 0;
        }
        
        /**
         * Sets the layer within the array texture, used internally by
         * TextureManager.
         */
        inline void setTextureLayer(unsigned int textureLayer) {
            layer = textureLayer;
        }
        
        /**
         * Gets the assigned layer within the array texture, used
         * internally by TextureManager.
         */
        inline GLuint getTextureLayer(void) const {
            return layer;
        }
    };
}

#endif
