/*
 * OpenALContext.h
 *
 *  Created on: 20 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_AUDIO_OPENALCONTEXT_H_
#define AWE_AUDIO_OPENALCONTEXT_H_

#include <AL/alc.h>
#include <cstddef>
#include <cstdint>

namespace awe {

    /**
     * OpenALContext provides a convenient wrapper for creating an
     * ALCdevice and ALCcontext, and assigning the context. The
     * existence of an OpenALContext is all that is required to interact
     * with OpenAL.
     */
    class OpenALContext {
    private:
        ALCdevice * device;
        ALCcontext * context;
        
        OpenALContext(const OpenALContext & other);
        OpenALContext & operator = (const OpenALContext & other);

    public:
        /**
         * For information about devicename and attrlist, see OpenAL
         * documentation for alcOpenDevice and alcCreateContext
         * respectively. Leaving these as NULL (default) is fine for
         * most purposes.
         */
        OpenALContext(const ALCchar * devicename = NULL, ALCint * attrlist = NULL);
        ~OpenALContext(void);
        
    private:
        // Frees our resources.
        void destroy(void);
    
    public:
    
        /**
         * Returns true if we successfully loaded the device and
         * context.
         */
        inline bool loadedSuccessfully(void) const {
            return device != NULL;
        }
    
        /**
         * Allows access to the OpenAL device, or NULL if we failed to
         * load.
         */
        inline ALCdevice * getDevice(void) const {
            return device;
        }
        
        /**
         * Allows access to the OpenAL context, or NULL if we failed to
         * load.
         */
        inline ALCcontext * getContext(void) const {
            return context;
        }
        
        /**
         * Makes our context current. Calling this is not necessary
         * unless using multiple OpenAL device/contexts (multiple
         * OpenALContext instances) are in use.
         */
        void makeCurrent(void) const;
        
        
        // ------------------- Static utility methods ------------------
        
        /**
         * Runs OpenAL's alGetError function. If this returns an error,
         * true is returned, and an error is printed to the log.
         * Otherwise, false is returned.
         */
        static bool hasError(void);
        
        /**
         * Populates the specified data with a mono sine wave sound.
         * This is helpful for placeholder audio - for example where
         * loading a file failed. Default values for wavelength and
         * volume are optionally provided.
         * size:       The size of the data, in 16 bit sample count.
         * wavelength: The wavelength of the sine wave, in 16 bit sample
         *             count.
         * volume:     The amplitude of the wave, where 0 is nothing,
         *             and 1 is the maximum amplitude which can fit in
         *             16 bit data.
         */
        static void makeSineWave(int16_t * data, size_t size, size_t wavelength = 128, float volume = 0.2f);
        
        /**
         * Loads sound data from the specified file. If the file
         * is too small, or the file cannot be read, makeSineWave is
         * used to generate placeholder data and a complaint is printed
         * to the log.
         * Note that "size" is the size of the sound file, measured in
         * 16 bit sample count.
         */
        static void loadSoundData(int16_t * data, size_t size, const char * filename);
    };
}

#endif
