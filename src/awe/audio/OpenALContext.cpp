/*
 * OpenALContext.cpp
 *
 *  Created on: 20 Sep 2020
 *      Author: wilson
 */

#include "OpenALContext.h"

#include <iostream>
#include <AL/al.h>
#include <iomanip>
#include <fstream>
#include <limits>
#include <cmath>
#include <ios>

namespace awe {

    OpenALContext::OpenALContext(const ALCchar * devicename, ALCint * attrlist) :
        device(NULL),
        context(NULL) {
        
        // Open openal device, complain if doing so failed.
        device = alcOpenDevice(devicename);
        if (device == NULL) {
            std::cerr << "WARNING: OpenALContext: " << "Failed to open OpenAL device, audio will not play.\n";
        }
        
        // Create context, complain if doing so failed. Make the context current and reset errors.
        context = alcCreateContext(device, attrlist);
        if (context == NULL) {
            std::cerr << "WARNING: OpenALContext: " << "Failed to create OpenAL context, audio will not play.\n";
            destroy();
        } else {
            alcMakeContextCurrent(context);
            alGetError();
        }
    }
    
    OpenALContext::~OpenALContext(void) {
        destroy();
    }
    
    void OpenALContext::destroy(void) {
        if (context != NULL) {
            // Always check errors before destroying the context, for logging purposes.
            hasError();
            alcMakeContextCurrent(NULL);
            alcDestroyContext(context);
            context = NULL;
        }
        if (device != NULL) {
            alcCloseDevice(device);
            device = NULL;
        }
    }
    
    void OpenALContext::makeCurrent(void) const {
        alcMakeContextCurrent(context);
    }
    
    bool OpenALContext::hasError(void) {
        ALenum error = alGetError();
        if (error == AL_NO_ERROR) {
            return false;
        }
        std::cerr << "WARNING: OpenALContext: " << "OpenAL error: ";
        switch(error) {
        case AL_INVALID_NAME:
            std::cerr << "AL_INVALID_NAME";
            break;
        case AL_INVALID_ENUM:
            std::cerr << "AL_INVALID_ENUM";
            break;
        case AL_INVALID_VALUE:
            std::cerr << "AL_INVALID_VALUE";
            break;
        case AL_INVALID_OPERATION:
            std::cerr << "AL_INVALID_OPERATION";
            break;
        case AL_OUT_OF_MEMORY:
            std::cerr << "AL_OUT_OF_MEMORY";
            break;
        default:
            std::cerr << "Other error";
            break;
        }
        std::ios_base::fmtflags flags = std::cerr.flags();
        std::cerr << " (0x" << std::hex << error << ").\n";
        std::cerr.flags(flags);
        return true;
    }
    
    void OpenALContext::makeSineWave(int16_t * data, size_t size, size_t wavelength, float volume) {
        float upto = 0.0f,
              step = (M_PI * 2.0f) / wavelength,
              mult = volume * std::numeric_limits<int16_t>::max();
        for (size_t i = 0; i < size; i++) {
            data[i] = (int16_t)(std::sin(upto) * mult);
            upto = std::fmod(upto + step, M_PI * 2.0f);
        }
    }
    
    void OpenALContext::loadSoundData(int16_t * data, size_t size, const char * filename) {
        std::ifstream stream(filename, std::ios::binary);
        if (!stream || !stream.read((char *)data, size * sizeof(int16_t))) {
            std::cerr << "WARNING: OpenALContext: " << "Failed to read sound file " << filename << ", check that it exists and contains enough data (" << size << " 16 bit samples).\n";
            makeSineWave(data, size);
        }
    }
}
