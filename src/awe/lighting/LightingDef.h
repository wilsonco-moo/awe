/*
 * LightingDef.h
 * C/GLSL header file
 * 
 *  Created on: 9 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_LIGHTING_LIGHTINGDEF_H_
#define AWE_LIGHTING_LIGHTINGDEF_H_

#include "../util/types/SizeDef.h"

/**
 * This header file contains constants for use both within the lighting shader,
 * and outside of it.
 */



/**
 * A special value representing "no light source".
 */
#define LIGHTING_NO_SOURCE 255u

/**
 * The maximum number of light sources available in the scene.
 */
#define LIGHTING_MAX_SOURCES 256u

/**
 * The size of each light source in the light sources buffer, in vec4s.
 */
#define LIGHTING_SOURCE_SIZE_VEC4 2u

/**
 * The size of each light source in the light sources buffer, in bytes.
 */
#define LIGHTING_SOURCE_SIZE_BYTES (LIGHTING_SOURCE_SIZE_VEC4 * SIZEOF_VEC4)

/**
 * The total size of the light sources buffer, in vec4s.
 */
#define LIGHTING_SOURCES_BUFFER_SIZE_VEC4 (LIGHTING_MAX_SOURCES * LIGHTING_SOURCE_SIZE_VEC4)

/**
 * The total size of the light sources buffer, in bytes.
 */
#define LIGHTING_SOURCES_BUFFER_SIZE_BYTES (LIGHTING_MAX_SOURCES * LIGHTING_SOURCE_SIZE_BYTES)




/**
 * The size of the light grid buffer, in uvec4s.
 */
#define LIGHTING_GRID_BUFFER_SIZE_UVEC4 (SIZEOF_MAX_UNIFORM / SIZEOF_UVEC4)

/**
 * The size of the light grid buffer, in uints/GLuints.
 */
#define LIGHTING_GRID_BUFFER_SIZE_UINT (LIGHTING_GRID_BUFFER_SIZE_BYTES / SIZEOF_UINT)

/**
 * The size of the light grid buffer, in bytes.
 */
#define LIGHTING_GRID_BUFFER_SIZE_BYTES (LIGHTING_GRID_BUFFER_SIZE_UVEC4 * SIZEOF_UVEC4)




#endif
