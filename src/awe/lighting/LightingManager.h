/*
 * LightingManager.h
 *
 *  Created on: 22 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_LIGHTING_LIGHTINGMANAGER_H_
#define AWE_LIGHTING_LIGHTINGMANAGER_H_

// Include the glm hash system. Disable messages since this is an experimental feature.
#include <glm/detail/setup.hpp>
#define GLM_MESSAGES GLM_DISABLE
#include <glm/gtx/hash.hpp>
#undef GLM_MESSAGES

#include <unordered_set>
#include <GL/gl.h>
#include <stack>

#include "../util/shader/ShaderResource.h"
#include "../util/types/Types.h"

namespace awe {

    class Light;

    /**
     * LightingManager controls the lighting of the scene. It consists of an
     * AXIS ALIGNED fixed size grid cell structure. Each grid cell can have up
     * to 4 lights overlapping within it. There can be up to 256 lights overall.
     * LightingManager also provides a SINGLE coloured directional light (sun).
     * NOTE: Outside the grid cell structure, lighting is not effective.
     */
    class LightingManager : public ShaderResource {
    private:
        friend class Light;
    
        GLvec3 ambientLightColour;
        GLuvec3 gridSize;
        GLvec3 originPos, cornerPos; // Note that originPos is always component wise smaller than cornerPos.
        GLvec3 sunDirectionInv, sunColour;
        GLmat4 worldToGridTransform,
               gridToWorldTransform;
        GLuint lightSourcesBuffer,
               lightGridBuffer;
        
        std::stack<GLuint> lightIndices;

        // Disable copying: We hold raw OpenGL resources.
        LightingManager(const LightingManager & other);
        LightingManager & operator = (const LightingManager & other);
        
    public:
        /**
         * Creates the lighting manager.
         * gridSize specified the dimensions (in grid cells) of the AXIS ALIGNED
         * grid cell structure.
         * gridCorner1 specified the position, in the world, of one corner of
         * the grid cell structure.
         * gridCorner2 specified the position, in the world, of the second
         * corner of the grid cell structure.
         * ambientLightColour specified the colour of the ambient light, i.e:
         * the colour used when an object is not illuminated.
         */
        LightingManager(const GLuvec3 & gridSize, const GLvec3 & gridCorner1, const GLvec3 & gridCorner2,
                        const GLvec3 & ambientLightColour = GLvec3(0.2, 0.2, 0.2),
                        const GLvec3 & sunDirection       = GLvec3(-0.5, -0.9, -1),
                        const GLvec3 & sunColour          = GLvec3(0.9, 0.9, 0.9));
        ~LightingManager(void);
        
    private:
        // Registers and unregisters light sources respectively. This manages the IDs of light sources, using a stack.
        GLuint registerLight(Light * light);
        void unregisterLight(Light * light);
        
        // Updates the data stored in the light sources buffer, about the specified light source.
        void updateLightSourceData(Light * light);
        
        // Adds an removes light indices from the appropriate places in the light grid buffer.
        void addToGridcell(void * mappedGridBuffer, const glm::uvec3 & gridCell, GLubyte lightIndex);
        void removeFromGridcell(void * mappedGridBuffer, const glm::uvec3 & gridCell, GLubyte lightIndex);
        
        // Converts between world positions, and gridcell coordinates, (always rounding down).
        GLuvec3 worldPosToGridcell(const GLvec3 & worldPos) const;
        GLvec3 gridcellToWorldPos(const GLuvec3 & gridCell) const;
        
        // Returns true if the specified light (and radius) intersects the specified gridcell.
        bool doesGridCellIntersectLight(const GLuvec3 & gridCell, const GLvec3 & lightPosition, GLfloat lightRadius) const;
        
        // Finds the cube area within the grid cell array which contains all the grid cells which might be intersecting
        // with the light source.
        // corner1 is inclusive minimum
        // corner2 is exclusive maximum
        void findIntersectingGridArea(GLuvec3 & corner1, GLuvec3 & corner2, const GLvec3 & lightPosition, GLfloat lightRadius) const;
        
    protected:
        // Here we set up all uniforms and buffer bindings for the new shader, and complain
        // if doing so fails.
        virtual void onRegisterShader(GLuint shaderProgram) override;
        
        // Here we actually do nothing, since uniform buffer objects do not
        // actually need unbinding from a shader. However, after unregistering
        // the shader from LightingManager, make sure its uniform buffers
        // are re-bound before re-using it.
        virtual void onUnregisterShader(GLuint shaderProgram) override;
        
    public:
        /**
         * Resets the lighting grid, and changes the size of the lighting grid.
         * This must only be done if there are no registered lights.
         * This MUST be called from an OpenGL context.
         */
        void setGridSize(const GLuvec3 & gridSize, const GLvec3 & gridCorner1, const GLvec3 & gridCorner2);
        
        /**
         * Changes the ambient light colour.
         * This MUST be called from an OpenGL context.
         */
        void setAmbientLightColour(const GLvec3 & ambientLightColour);
        
        /**
         * Updates the direction and colour of the sun. Note that the direction
         * is automatically normalised and inverted.
         */
        void setSunProperties(const GLvec3 & newSunDirection, const GLvec3 & newSunColour);
    };
    
    // =========================================================================
    
    /**
     * A light which can be associated with a lighting manager.
     * Each light has a position, a brightness/colour, a radius, and a falloff.
     * For more information about what these parameters adjust, see the
     * documentation at the top of LightingShader.glh.
     */
    class Light {
    private:
        LightingManager * lightingManager;
        GLvec3 position;
        GLvec3 brightness;
        GLfloat radius, falloff;
        GLuint index;
        std::unordered_set<GLuvec3> gridCells;
        
        // Disable copying: We reference our lighting manager.
        Light(const Light & light);
        Light & operator = (const Light & other);
        
    public:
        Light(LightingManager * lightingManager, const GLvec3 & position, const GLvec3 & brightness, GLfloat radius, GLfloat falloff = 6.0f);
        ~Light(void);
        
        /**
         * Methods to access various light properties.
         */
        inline const GLvec3 & getPosition(void) const { return position; }
        inline const GLvec3 & getBrightness(void) const { return brightness; }
        inline GLfloat getRadius(void) const { return radius; }
        inline GLfloat getFalloff(void) const { return falloff; }
        inline GLuint getIndex(void) const { return index; }
        
        /**
         * Methods to set various light properties. After changing any of these,
         * the update method must be called (from an OpenGL context), for it to
         * take any visual effect.
         */
        inline void setPosition(const GLvec3 & newPosition) { position = newPosition; }
        inline void setBrightness(const GLvec3 & newBrightness) { brightness = newBrightness; }
        inline void setRadius(GLfloat newRadius) { radius = newRadius; }
        inline void setFalloff(GLfloat newFalloff) { falloff = newFalloff; }
        
        /**
         * Updates our properties and affecting grid cells, in the shader
         * uniforms. This must be called after changing any properties.
         */
        void update(void);
    };
}

#endif
