/*
 * LightingShader.glh
 * GLSL header file
 * 
 *  Created on: 9 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_LIGHTING_LIGHTINGSHADER_GLH_
#define AWE_LIGHTING_LIGHTINGSHADER_GLH_

#include "LightingDef.h"

/**
 * This header file contains all of the uniform declarations and
 * helper functions to work with the grid-based lighting system.
 * 
 * Light sources have a defined radius and falloff.
 *  > The radius specifies the size of the area which the light affects: At
 *    distance=radius from the light, its contribution is always zero.
 *  > The falloff represents the shape of the distance function, and
 *    (for sensible results) should be -1 or larger.
 *     - Very small falloff values (close or equal to -1) create a light source
 *       with a near constant light output, until the edge of the radius,
 *       where there is a sudden drop to zero.
 *     - Medium falloff values (around 6 or so) create a light source with
 *       a falloff vaguely similar to a gaussian curve.
 *     - Very large falloff values (larger than around 20) create a light with
 *       a bright spot in the centre, which very quickly diminishes as you go
 *       further out.
 * 
 * See the following desmos graph for a visual representation of the light
 * falloff curve used: (Adjust the "f" parameter to adjust falloff).
 * https://www.desmos.com/calculator/9dywpjdns9
 * 
 * 
 * Once this header file is included, the total incident light at a world
 * position and normal can be calculated as follows:
 * 
 * vec3 incidentLight = lighting_ambientColour +
 *                      lighting_getDirectionalContribution(normal) +
 *                      lighting_getTotalSourceContribution(worldPos, normal);
 * 
 * This is all bundled into a single utility method:
 * 
 * vec3 incidentLight = lighting_getTotal(worldPos, normal);
 */


/**
 * This uniform defines the grid size of the lighting grid.
 */
uniform uvec3 lighting_gridSize;

/**
 * This uniform defines the transformation from world coordinates to lighting
 * grid coordinates.
 */
uniform mat4 lighting_worldToGridTransform;

/**
 * This uniform defines the current ambient light colour.
 */
uniform vec3 lighting_ambientColour;

/**
 * This uniform defines the colour of the directional light (sun).
 */
uniform vec3 lighting_sunColour;

/**
 * This uniform defines the (normalised and inverted) direction of the
 * directional light (sun).
 */
uniform vec3 lighting_sunDirectionInv;


/**
 * The uniform buffer for light sources. This is an array of vec4s to get around
 * the vec4 alignment thing.
 */
layout (std140) uniform lighting_sourcesBlock {
    vec4 lighting_sources[LIGHTING_SOURCES_BUFFER_SIZE_VEC4];
};

/**
 * The uniform buffer for the light grid. This is an array of vec4s to get
 * around the vec4 alignment thing.
 */
layout (std140) uniform lighting_gridBlock {
    uvec4 lighting_grid[LIGHTING_GRID_BUFFER_SIZE_UVEC4];
};

/**
 * Converts a world position to a light grid coordinate.
 */
uvec3 lighting_getGridPos(vec3 worldPos) {
    return uvec3(clamp(
        floor(
            (lighting_worldToGridTransform * vec4(worldPos, 1.0f)).xyz
        ),
        vec3(0,0,0), vec3(lighting_gridSize) - vec3(1,1,1)
    ));
}

/**
 * Converts a light grid coordinate to an index within the light grid buffer.
 */
uint lighting_getGridCellIndex(uvec3 gridPos) {
    return gridPos.x +
           (gridPos.y * lighting_gridSize.x) +
           (gridPos.z * lighting_gridSize.x * lighting_gridSize.y);
}

/**
 * Gets the light contribution of a single light source, for the specified
 * point.
 * Note that this never returns values below zero, but *can* return values above
 * one.
 */
vec3 lighting_getSingleSourceContribution(uint lightIndex, vec3 worldPos, vec3 normal) {
    // Find the position and brightness of the light.
    vec4 lightPosition   = lighting_sources[lightIndex * LIGHTING_SOURCE_SIZE_VEC4];
    vec4 lightBrightness = lighting_sources[lightIndex * LIGHTING_SOURCE_SIZE_VEC4 + 1u];
    // Read the radius, falloff and work out distance.
    float lightRadius = lightPosition.w;
    float lightFalloff  = lightBrightness.w;
    vec3 lightRelativePos = lightPosition.xyz - worldPos;
    float lightDistance = length(lightRelativePos);
    
    // Return zero contribution if the light is too far away.
    if (lightDistance >= lightRadius) {
         return vec3(0,0,0);
    }
    
    // Find distance in terms of radius (0-1), then square it.
    float inDist = lightDistance / lightRadius;
    inDist *= inDist;
    
    // Work out the lighting power, using using falloff.
    float power =  (1.0f - inDist) / (lightFalloff * inDist + 1);
    
    // Use dot product of the normal to decrease contribution depending on angle.
    // Clamp to avoid falling below zero.
    power *= max(dot(normal, normalize(lightRelativePos)), 0.0f);
    
    // Multiply the brightness (light colour) by this final light power.
    return lightBrightness.xyz * power;
}

/**
 * Returns the total light contribution from all affecting light sources, for
 * the specified point and normal.
 * Note that this never returns values below zero, but *can* return values above
 * one.
 */
vec3 lighting_getTotalSourceContribution(vec3 worldPos, vec3 normal) {
    
    // Get grid position, and index in grid.
    uvec3 gridPos = lighting_getGridPos(worldPos);
    uint index = lighting_getGridCellIndex(gridPos);
    
    // Get the light sources at our grid cell.
    uint lightSources = lighting_grid[index/4u][index%4u];
    
    // Don't bother doing anything at all if no lights affect this point.
    if (lightSources == 0xffffffffu) {
        return vec3(0.0f, 0.0f, 0.0f);
    }
    
    // Add incident light from each of the four possible light sources.
    vec3 incidentLight = vec3(0.0f, 0.0f, 0.0f);
        
    uint lightIndex = (lightSources >> 24u) & 255u;
    if (lightIndex != LIGHTING_NO_SOURCE) incidentLight += lighting_getSingleSourceContribution(lightIndex, worldPos, normal);
    
    lightIndex = (lightSources >> 16u) & 255u;
    if (lightIndex != LIGHTING_NO_SOURCE) incidentLight += lighting_getSingleSourceContribution(lightIndex, worldPos, normal);
    
    lightIndex = (lightSources >> 8u) & 255u;
    if (lightIndex != LIGHTING_NO_SOURCE) incidentLight += lighting_getSingleSourceContribution(lightIndex, worldPos, normal);
    
    lightIndex = lightSources & 255u;
    if (lightIndex != LIGHTING_NO_SOURCE) incidentLight += lighting_getSingleSourceContribution(lightIndex, worldPos, normal);
    
    return incidentLight;
}

/**
 * Returns the lighting contribution from the single directional light (sun).
 * Note that this never returns values below zero, but *can* return values above
 * one.
 */
vec3 lighting_getDirectionalContribution(vec3 normal) {
    return max(dot(normal, lighting_sunDirectionInv), 0.0f) * lighting_sunColour;
}

/**
 * Returns the total lighting contribution from ambient light, directional
 * light and all light sources, for the specified position and normal.
 * Note that this never returns values below zero, but *can* return values above
 * one.
 */
vec3 lighting_getTotal(vec3 worldPos, vec3 normal) {
    return lighting_ambientColour +
           lighting_getDirectionalContribution(normal) +
           lighting_getTotalSourceContribution(worldPos, normal);
}


#endif
