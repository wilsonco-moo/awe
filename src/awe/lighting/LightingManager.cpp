/*
 * LightingManager.cpp
 *
 *  Created on: 22 Jan 2020
 *      Author: wilson
 */
#include <GL/gl3w.h> // Include first

#include "LightingManager.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <cstring>
#include <vector>

#include "../asset/AssetManager.h"
#include "../util/Util.h"
#include "LightingDef.h"

namespace awe {
    
    /**
     * Finds the index within the grid cell array, of a gridcell coordinate (GLuvec3).
     */
    #define GET_GRID_CELL_INDEX(gridCellVector)             \
        ((gridCellVector).x +                               \
         ((gridCellVector).y * gridSize.x) +                \
         ((gridCellVector).z * (gridSize.x * gridSize.y)))

    /**
     * Allows convenient iteration through a range of gridcells (GLuvec3).
     * Iteration is done inclusive from corner1, and exclusive from corner2.
     * Assumes that corner1 is always component wise less than or equal to corner2.
     */
    #define ITERATE_GRIDCELL_RANGE(gridCellName, corner1, corner2)                                          \
        { GLuvec3 gridCellName;                                                                             \
        for (gridCellName.z = (corner1).z; gridCellName.z < (corner2).z; gridCellName.z++) {                \
            for (gridCellName.y = (corner1).y; gridCellName.y < (corner2).y; gridCellName.y++) {            \
                for (gridCellName.x = (corner1).x; gridCellName.x < (corner2).x; gridCellName.x++) {
    #define ITERATE_GRIDCELL_RANGE_END }}}}



    LightingManager::LightingManager(const GLuvec3 & requestedGridSize, const GLvec3 & gridCorner1, const GLvec3 & gridCorner2, const GLvec3 & requestedAmbientLightColour, const GLvec3 & requestedSunDirection, const GLvec3 & requestedSunColour) :
        ShaderResource(),
    
        lightIndices() {
        
        // Initialise the light indices. Don't put the last light source index in there (NO_LIGHT_SOURCE).
        for (GLuint i = 0; i < LIGHTING_MAX_SOURCES - 1; i++) {
            lightIndices.push(i);
        }
        
        // ------------------- Create and allocate the buffers -----------------
        
        // Allocate appropriately sized buffer for light sources.
        // It does not need to be initialised.
        glGenBuffers(1, &lightSourcesBuffer);
        if (lightSourcesBuffer == 0) { std::cerr << "WARNING: LightingManager: " << "Failed to generate buffer for light sources.\n"; return; }
        glBindBuffer(GL_ARRAY_BUFFER, lightSourcesBuffer);
        glBufferData(GL_ARRAY_BUFFER, LIGHTING_SOURCES_BUFFER_SIZE_BYTES, NULL, GL_STATIC_DRAW);
        
        // Allocate appropriately sized buffer for light grid.
        // This *does* need to be initialised with NO_LIGHT_SOURCE, but this is done in setGridSize, not here.
        glGenBuffers(1, &lightGridBuffer);
        if (lightGridBuffer == 0) { std::cerr << "WARNING: LightingManager: " << "Failed to generate buffer for light grid.\n"; return; }
        glBindBuffer(GL_ARRAY_BUFFER, lightGridBuffer);
        glBufferData(GL_ARRAY_BUFFER, LIGHTING_GRID_BUFFER_SIZE_BYTES, NULL, GL_STATIC_DRAW);
        
        // Unbind buffer.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // Set the grid size and the ambient light colour.
        setGridSize(requestedGridSize, gridCorner1, gridCorner2);
        setAmbientLightColour(requestedAmbientLightColour);
        setSunProperties(requestedSunDirection, requestedSunColour);
    }
    
    LightingManager::~LightingManager(void) {
    }
    
    GLuint LightingManager::registerLight(Light * light) {
        GLuint index = lightIndices.top();
        lightIndices.pop();
        return index;
    }
    
    void LightingManager::unregisterLight(Light * light) {
        lightIndices.push(light->getIndex());
    }
    
    void LightingManager::updateLightSourceData(Light * light) {
        // Map the buffer.
        GLuint index = light->getIndex();
        glBindBuffer(GL_ARRAY_BUFFER, lightSourcesBuffer);
        void * mappedBuffer = glMapBufferRange(GL_ARRAY_BUFFER, index * LIGHTING_SOURCE_SIZE_BYTES, LIGHTING_SOURCE_SIZE_BYTES, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_RANGE_BIT);
        if (mappedBuffer == NULL) { std::cerr << "WARNING: LightingManager: " << "Failed to map light sources buffer when updating light.\n"; return; }
        GLvec4 * bufferData = (GLvec4 *)mappedBuffer;
        
        // Copy the data into the buffer.
        *(bufferData++) = GLvec4(light->getPosition(), light->getRadius());
        *(bufferData)   = GLvec4(light->getBrightness(), light->getFalloff());
        
        // Unmap and unbind the buffer, now we are done.
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    void LightingManager::addToGridcell(void * mappedGridBuffer, const glm::uvec3 & gridCell, GLubyte lightIndex) {
        // Find the appropriate place in the buffer.
        GLubyte * data = ((GLubyte *)mappedGridBuffer) + (GET_GRID_CELL_INDEX(gridCell) * sizeof(GLuint));
        
        // Put our light into the first available of the 4 slots.
        for (unsigned int i = 0; i < sizeof(GLuint); i++) {
            if (data[i] == LIGHTING_NO_SOURCE) {
                data[i] = lightIndex;
                return;
            }
        }
        std::cerr << "WARNING: LightingManager: " << "More than 4 light sources attempted to be put in one light grid cell.\n";
    }
    
    void LightingManager::removeFromGridcell(void * mappedGridBuffer, const glm::uvec3 & gridCell, GLubyte lightIndex) {
        // Find the appropriate place in the buffer.
        GLubyte * data = ((GLubyte *)mappedGridBuffer) + (GET_GRID_CELL_INDEX(gridCell) * sizeof(GLuint));
        
        // Remove our light from the first of the 4 slots which contained it.
        for (unsigned int i = 0; i < sizeof(GLuint); i++) {
            if (data[i] == lightIndex) {
                data[i] = LIGHTING_NO_SOURCE;
                return;
            }
        }
        std::cerr << "WARNING: LightingManager: " << "Light tried to be removed from gridcell which did not contain it.\n";
    }
    
    GLuvec3 LightingManager::worldPosToGridcell(const GLvec3 & worldPos) const {
        return GLuvec3(glm::clamp(
            GLivec3(glm::floor(    // Round down, then clamp, then convert to unsigned.
                GLvec3(worldToGridTransform * GLvec4(worldPos, 1.0f))
            )),
            GLivec3(0,0,0), GLivec3(gridSize - GLuvec3(1,1,1))
        ));
    }
    
    GLvec3 LightingManager::gridcellToWorldPos(const GLuvec3 & gridCell) const {
        return GLvec3(gridToWorldTransform * GLvec4(gridCell, 1.0f));
    }
    
    bool LightingManager::doesGridCellIntersectLight(const GLuvec3 & gridCell, const GLvec3 & lightPosition, GLfloat lightRadius) const {
        GLvec3 corner1 = gridcellToWorldPos(gridCell),
               corner2 = gridcellToWorldPos(gridCell + GLuvec3(1,1,1));
        return Util::doesCubeIntersectSphere(corner1, corner2, lightPosition, lightRadius);
    }
    
    void LightingManager::findIntersectingGridArea(GLuvec3 & corner1, GLuvec3 & corner2, const GLvec3 & lightPosition, GLfloat lightRadius) const {
        
        GLvec3 corner1World = lightPosition - GLvec3(lightRadius, lightRadius, lightRadius);
        corner1 = GLuvec3(glm::clamp(
            GLivec3(glm::floor(    // Round down, then clamp, then convert to unsigned. 
                GLvec3(worldToGridTransform * GLvec4(corner1World, 1.0f))
            )),
            GLivec3(0,0,0), GLivec3(gridSize) // Clamp to gridSize not gridSize-1 so we iterate nothing if light is off edge.
        ));
        
        GLvec3 corner2World = lightPosition + GLvec3(lightRadius, lightRadius, lightRadius);
        corner2 = GLuvec3(glm::clamp(
            GLivec3(glm::ceil(    // Round up, then clamp, then convert to unsigned.
                GLvec3(worldToGridTransform * GLvec4(corner2World, 1.0f))
            )),
            GLivec3(0,0,0), GLivec3(gridSize) // Clamp to gridSize not gridSize-1 so we iterate nothing if light is off edge.
        ));
    }
    
    void LightingManager::onRegisterShader(GLuint shaderProgram) {
        // We are dealing with bindings in the shader program. Make sure the shader program is bound for the
        // duration of this.
        glUseProgram(shaderProgram);
        
        // Setup uniform block bindings in the shader.
        GLuint lightSourcesIndex = Util::getUniformBlockIndexChecked(shaderProgram, "LightingManager::onRegisterShader", "lighting_sourcesBlock"),
               lightGridIndex    = Util::getUniformBlockIndexChecked(shaderProgram, "LightingManager::onRegisterShader", "lighting_gridBlock");
        glUniformBlockBinding(shaderProgram, lightSourcesIndex, AssetManager::BufferBindings::lightSources);
        glUniformBlockBinding(shaderProgram, lightGridIndex,    AssetManager::BufferBindings::lightGrid);
        
        // Bind the light sources and light grid buffers, to these uniform block bindings.
        glBindBufferBase(GL_UNIFORM_BUFFER, AssetManager::BufferBindings::lightSources, lightSourcesBuffer);
        glBindBufferBase(GL_UNIFORM_BUFFER, AssetManager::BufferBindings::lightGrid,    lightGridBuffer);
        
        // Set uniforms in the shader program for the grid size, and the transformation from world
        // coordinates to grid coordinates.
        GLint lightGridSizeLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::onRegisterShader", "lighting_gridSize"),
              gridTransformLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::onRegisterShader", "lighting_worldToGridTransform");
        glUniform3uiv(lightGridSizeLocation, 1, glm::value_ptr(gridSize));
        glUniformMatrix4fv(gridTransformLocation, 1, GL_FALSE, glm::value_ptr(worldToGridTransform));
        
        // Set uniform for ambient light colour.
        GLint ambientLightColourLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::onRegisterShader", "lighting_ambientColour");
        glUniform3fv(ambientLightColourLocation, 1, glm::value_ptr(ambientLightColour));
        
        // Set uniforms for sun properties.
        GLint sunDirectionInvLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::onRegisterShader", "lighting_sunDirectionInv"),
                    sunColourLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::onRegisterShader", "lighting_sunColour");
        glUniform3fv(sunDirectionInvLocation, 1, glm::value_ptr(sunDirectionInv));
        glUniform3fv(sunColourLocation,       1, glm::value_ptr(sunColour));
        
        // This no longer needs to be bound.
        glUseProgram(0);
    }
    
    void LightingManager::onUnregisterShader(GLuint shaderProgram) {
    }
    
    void LightingManager::setGridSize(const GLuvec3 & requestedGridSize, const GLvec3 & gridCorner1, const GLvec3 & gridCorner2) {
        
        // Complain and do nothing if there are still light sources active.
        if (lightIndices.size() != LIGHTING_MAX_SOURCES - 1) {
            std::cerr << "ERROR: LightingManager: " << "Tried to change grid size of lighting manager, while " << ((LIGHTING_MAX_SOURCES - 1) - lightIndices.size()) << " light sources are still active.\n";
            return;
        }
        
        // Check that the size of the requested grid is not too big or too small.
        // It is an error, (so EXIT) if it is too big, but only warn if it is too small.
        unsigned long long gridVolume = requestedGridSize.x * requestedGridSize.y * requestedGridSize.z;
        if (gridVolume > LIGHTING_GRID_BUFFER_SIZE_UINT) {
            std::cerr << "ERROR: LightingManager: " << "Requested grid size of " << gridVolume << ", which is larger than maximum supported grid size of " << LIGHTING_GRID_BUFFER_SIZE_UINT << ".\n";
            exit(EXIT_FAILURE);
        }
        if (gridVolume < LIGHTING_GRID_BUFFER_SIZE_UINT) {
            std::cerr << "WARNING: LightingManager: " << "Underutilised lighting grid. Only requested " << gridVolume << " cells, out of a maximum of " << LIGHTING_GRID_BUFFER_SIZE_UINT << " grid cells.\n";
        }
        
        // Set grid size and origin to the component-wise min of the two corners, and corner to the component-wise max of the two corners.
        gridSize = requestedGridSize;
        originPos = glm::min(gridCorner1, gridCorner2);
        cornerPos = glm::max(gridCorner1, gridCorner2);
        
        // Set world to grid transformation.
        worldToGridTransform = glm::translate( // Outer transformation done first (inner * outer)   (translate then scale)
                                   glm::scale(
                                       awe::GLmat4(1.0f),                             /**  To go from world coordinate to grid:                                             */
                                       GLvec3(gridSize) / (cornerPos - originPos)     /**  gridX = (worldX - cornerPos.x) * (gridSize.x / (cornerPos.x - originPos.x))      */
                                   ),                                                 /**                                                                                   */
                                   -originPos                                         /**  Translate by       -originPos.x                                                  */
                               );                                                     /**  Then scale by      (gridSize.x / (cornerPos.x - originPos.x))                    */
        
        // Set grid to world transformation.
        gridToWorldTransform = glm::scale( // Outer transformation done first (inner * outer)   (scale then translate)
                                   glm::translate(
                                       awe::GLmat4(1.0f),                             /**  To go from grid coordinate to world coordinate                                   */
                                       originPos                                      /**  gridX = (worldX * ((cornerPos.x - originPos.x) / gridSize.x)) + originPos.x      */
                                   ),                                                 /**                                                                                   */
                                   (cornerPos - originPos) / GLvec3(gridSize)         /**  Scale by           ((cornerPos.x - originPos.x) / gridSize.x)                    */
                               );                                                     /**  Translate by       originPos.x                                                   */
        
        // Bind, map, then fill the light grid buffer with NO_LIGHT_SOURCE. Then unmap and unbind it.
        glBindBuffer(GL_ARRAY_BUFFER, lightGridBuffer);
        void * mappedBuffer = glMapBufferRange(GL_ARRAY_BUFFER, 0, LIGHTING_GRID_BUFFER_SIZE_BYTES, GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_RANGE_BIT);
        if (mappedBuffer == NULL) { std::cerr << "WARNING: LightingManager: " << "Failed to map light grid buffer.\n"; return; }
        memset(mappedBuffer, LIGHTING_NO_SOURCE, LIGHTING_GRID_BUFFER_SIZE_BYTES);
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        
        // Update the uniforms in all shader programs, for grid size and transformations.
        for (GLuint shaderProgram : getShaders()) {
            glUseProgram(shaderProgram);
            GLint lightGridSizeLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::setGridSize", "lighting_gridSize"),
                  gridTransformLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::setGridSize", "lighting_worldToGridTransform");
            glUniform3uiv(lightGridSizeLocation, 1, glm::value_ptr(gridSize));
            glUniformMatrix4fv(gridTransformLocation, 1, GL_FALSE, glm::value_ptr(worldToGridTransform));
        }
        // Unbind the shader at the end.
        glUseProgram(0);
    }
    
    void LightingManager::setAmbientLightColour(const GLvec3 & newAmbientLightColour) {
        // Set new ambient light colour.
        ambientLightColour = newAmbientLightColour;
        
        // Update the uniforms in all shader programs, for ambient light colour.
        for (GLuint shaderProgram : getShaders()) {
            glUseProgram(shaderProgram);
            GLint ambientLightColourLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::setAmbientLightColour", "lighting_ambientColour");
            glUniform3fv(ambientLightColourLocation, 1, glm::value_ptr(ambientLightColour));
        }
        // Unbind the shader at the end.
        glUseProgram(0);
    }
    
    void LightingManager::setSunProperties(const GLvec3 & newSunDirection, const GLvec3 & newSunColour) {
        // Update our internal copy of direction and colour.
        sunDirectionInv = glm::normalize(newSunDirection * -1.0f);
        sunColour       = newSunColour;
        
        // Update the uniforms in all shader programs, for ambient light colour.
        for (GLuint shaderProgram : getShaders()) {
            glUseProgram(shaderProgram);
            GLint sunDirectionInvLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::setSunProperties", "lighting_sunDirectionInv"),
                        sunColourLocation = Util::getUniformLocationChecked(shaderProgram, "LightingManager::setSunProperties", "lighting_sunColour");
            glUniform3fv(sunDirectionInvLocation, 1, glm::value_ptr(sunDirectionInv));
            glUniform3fv(sunColourLocation,       1, glm::value_ptr(sunColour));
        }
        // Unbind the shader at the end.
        glUseProgram(0);
    }
    
    // =========================================================================
    
    Light::Light(LightingManager * lightingManager, const GLvec3 & position, const GLvec3 & brightness, GLfloat radius, GLfloat falloff) :
        lightingManager(lightingManager),
        position(position),
        brightness(brightness),
        radius(radius),
        falloff(falloff),
        index(lightingManager->registerLight(this)),
        gridCells() {
        
        update();
    }
    
    Light::~Light(void) {
        lightingManager->unregisterLight(this);
    }
    
    void Light::update(void) {
        // First update our source data.
        lightingManager->updateLightSourceData(this);
        
        // Map the light grid buffer, since we will be extensively modifying it.
        // Set GL_MAP_READ_BIT, and don't set GL_MAP_INVALIDATE_RANGE_BIT, since
        // we need to read from it also.
        glBindBuffer(GL_ARRAY_BUFFER, lightingManager->lightGridBuffer);
        void * mappedBuffer = glMapBufferRange(GL_ARRAY_BUFFER, 0, LIGHTING_GRID_BUFFER_SIZE_BYTES, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT);
        if (mappedBuffer == NULL) { std::cerr << "WARNING: LightingManager: " << "Failed to map light grid buffer, when updating light source.\n"; return; }
        
        // First remove any of our existing cells, which are no longer intersecting our light area of influence.
        {
            std::vector<GLuvec3> toRemove;
            for (const GLuvec3 & gridCell : gridCells) {
                if (!lightingManager->doesGridCellIntersectLight(gridCell, position, radius)) {
                    toRemove.push_back(gridCell);
                }
            }
            for (const GLuvec3 & gridCell : toRemove) {
                gridCells.erase(gridCell);
                lightingManager->removeFromGridcell(mappedBuffer, gridCell, index);
            }
        }
        
        // Then ensure that all grid cells intersecting our light area of influence get added.
        {
            // Iterate through all grid cells in influence area.
            GLuvec3 influenceCorner1, influenceCorner2;
            lightingManager->findIntersectingGridArea(influenceCorner1, influenceCorner2, position, radius);
            ITERATE_GRIDCELL_RANGE(gridCell, influenceCorner1, influenceCorner2)
                // If the cell intersects, try to add it to our gridCells. If it wasn't there already,
                // add it to the light grid buffer.
                if (lightingManager->doesGridCellIntersectLight(gridCell, position, radius)) {
                    if (gridCells.insert(gridCell).second) {
                        lightingManager->addToGridcell(mappedBuffer, gridCell, index);
                    }
                }
            ITERATE_GRIDCELL_RANGE_END
        }
        
        // Unmap and unbind the buffer, now we are done.
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}
