/*
 * TerrainScatter.cpp
 *
 *  Created on: 22 Aug 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "TerrainScatter.h"

#include <wool/texture/lodepng/lodepng.h>
#include <glm/gtc/matrix_transform.hpp>
#include <threading/ThreadQueue.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <random>

#include "../texture/TextureManager.h"
#include "../asset/AssetManager.h"
#include "../asset/Asset.h"
#include "HeightMap.h"

namespace awe {

    TerrainScatter::TerrainScatter(threading::ThreadQueue * threadQueue, AssetManager * assetManager, const std::string & assetFilename, HeightMap * heightmap, const std::string & scatterTexFilename, GLfloat minScale, GLfloat maxScale, GLfloat zOffset, bool randomiseRotations, size_t particleCount, unsigned long long seed, unsigned int texChannel) :
        threadQueue(threadQueue),
        token(threadQueue->createToken()),
        assetManager(assetManager),
        heightmap(heightmap),
        scatterTexFilename(scatterTexFilename),
        texChannel(texChannel),
        seed(seed),
        asset(assetManager->requestAsset(assetFilename)),
        minScale(minScale),
        maxScale(maxScale),
        zOffset(zOffset),
        randomiseRotations(randomiseRotations),
        particleCount(particleCount),
        
        loadStage(LoadStages::texAndTerrain),
        cumulativeScatterDensity(NULL),
        instanceTransforms(NULL),
        
        vao(0),
        instanceBuffer(0) {
        
        // Initially load scatter density: We can do this while the heightmap
        // is still loading.
        loadJob = threadQueue->add(token, [this](void) {
            loadScatterDensity();
        });
    }
    
    TerrainScatter::~TerrainScatter(void) {
        threadQueue->deleteToken(token);
        assetManager->abandonAsset(asset);
        delete[] cumulativeScatterDensity;
        delete[] instanceTransforms;
        glDeleteVertexArrays(1, &vao); // Note: always safe to do since these OpenGL methods silently ignore zeroes.
        glDeleteBuffers(1, &instanceBuffer);
    }
    
    void TerrainScatter::loadScatterDensity(void) {
        // Load scatter texture. Upon failure, or discovery of non-square
        // texture, free result and use no texture texture of size 64. Otherwise
        // set scatterSize to size of loaded texture.
        unsigned char * scatterDensityRawTex = NULL;
        {
            unsigned int width, height,
                         error = lodepng_decode32_file(&scatterDensityRawTex, &width, &height, scatterTexFilename.c_str());
            
            if (error != 0 || width != height) {
                std::cerr << "Warning: TerrainScatter: " << "Failed to load texture " << scatterTexFilename << ", it is either not square or missing.\n";
                free(scatterDensityRawTex);
                scatterSize = 64;
                scatterDensityRawTex = TextureManager::generateNoTextureTexture(scatterSize);
            } else {
                scatterSize = width;
            }
        }
        
        // Convert the channel specified by texChannel to cumulative scatter
        // density.
        cumulativeScatterDensity = new uint64_t[scatterSize * scatterSize];
        uint64_t scatterUpto = 0;
        for (size_t iy = 0; iy < scatterSize; iy++) {
            for (size_t ix = 0; ix < scatterSize; ix++) {
                scatterUpto += scatterDensityRawTex[(ix + iy * scatterSize) * TextureManager::CHANNELS + texChannel];
                cumulativeScatterDensity[ix + iy * scatterSize] = scatterUpto;
            }
        }
        maxCumulativeScatter = scatterUpto;
        free(scatterDensityRawTex);
    }
    
    void TerrainScatter::setupScatterInstances(void) {
        
        instanceTransforms = new GLmat4[particleCount];
        const GLmat4 & baseTransform = asset->getBaseTransform();
        
        // Multiplier from our internal grid coordinates to the terrain's real world coordinates.
        GLfloat terrainScaling = heightmap->getRealVisibleMapSize() / scatterSize;
        
        // Set up random generator using user-specified seed.
        std::seed_seq seedSeq({seed});
        std::mt19937 generator(seedSeq);
        std::uniform_int_distribution<uint64_t> randInt(0, maxCumulativeScatter - 1);
        std::uniform_real_distribution<float> rand01(0.0f, 1.0f),
                                              randScale(minScale, maxScale);
        std::normal_distribution<float> randNormalDist(0.0f); // Normal distribution with mean 0.
        
        for (size_t i = 0; i < particleCount; i++) {
            
            // Pick random uniform integer upto maximum cumulative value,
            // and use std::lower_bound and pointer difference to find index
            // within cumulative array. This is random grid cell weighted by
            // density at each grid cell.
            size_t index = std::lower_bound(cumulativeScatterDensity,
                                           cumulativeScatterDensity + scatterSize * scatterSize,
                                           randInt(generator)) - cumulativeScatterDensity;
                                           
            // Add an offset within grid cell for truly random position.
            // Multiply by terrainScaling to convert to real world coordinates.
            // Get z from terrain and use random scale. Multiply z offset by
            // the scale so z offset changes with particle scale.
            GLfloat scale = randScale(generator),
                        x = ((index % scatterSize) + rand01(generator)) * terrainScaling,
                        y = ((index / scatterSize) + rand01(generator)) * terrainScaling,
                        z = heightmap->getRealInterpHeight(x, y) + zOffset * scale;
            
            GLvec3 location(x, y, z);
            
            // Outer transformation done first (inner * outer)
            GLmat4 transform = glm::scale(GLmat4(1.0f), GLvec3(scale, scale, scale)) * baseTransform;
            
            // Generate random rotation by building quaternion from 4 values taken
            // from a normal distribution, then normalise it.
            // See https://en.wikipedia.org/wiki/Rotation_matrix#Uniform_random_rotation_matrices
            //     http://web.archive.org/web/20200807160540/https://en.wikipedia.org/wiki/Rotation_matrix#Uniform_random_rotation_matrices
            GLquat rotation;
            if (randomiseRotations) {
                rotation = GLquat(randNormalDist(generator), randNormalDist(generator), randNormalDist(generator), randNormalDist(generator));
                rotation = glm::normalize(rotation);
                transform = glm::mat4_cast(rotation) * transform;
            } else {
                rotation = glm::identity<GLquat>();
            }
            
            // Apply translation last, and store in instance transforms array.
            instanceTransforms[i] = glm::translate(GLmat4(1.0f), location) * transform;
            
            onSetupScatterInstance(baseTransform, scale, rotation, location);
        }
    }
    
    void TerrainScatter::onSetupScatterInstance(const GLmat4 & baseTransform, GLfloat scale, const GLquat & rotation, const GLvec3 & location) {
    }
    
    void TerrainScatter::onCompleteScatterSetup(void) {
    }
    
    unsigned int TerrainScatter::step(void) {
        
        switch(loadStage) {
            
        // For first load stage: Once background job is done AND heightmap is loaded,
        // AND asset is loaded, run setupScatterInstances in background and progress
        // to next load stage.
        case LoadStages::texAndTerrain:
            if (heightmap->getIsLoaded() && threadQueue->getJobStatus(loadJob) == threading::JobStatus::complete && asset->isLoaded()) {
                loadJob = threadQueue->add(token, [this](void) {
                    setupScatterInstances();
                });
                loadStage = LoadStages::scatterSetup;
            }
            break;
        
        // For second load stage: Once setupScatterInstances is done, setup
        // OpenGL resources and free now un-needed allocations, then move to
        // "done" load stage.
        case LoadStages::scatterSetup:
            if (threadQueue->getJobStatus(loadJob) == threading::JobStatus::complete) {
                
                // Allocate and populate instance buffer with instance transformations.
                glGenBuffers(1, &instanceBuffer);
                if (instanceBuffer == 0) { std::cerr << "WARNING: TerrainScatter: " << "Failed to generate instance buffer.\n"; }
                glBindBuffer(GL_ARRAY_BUFFER, instanceBuffer);
                glBufferData(GL_ARRAY_BUFFER, particleCount * sizeof(GLmat4), instanceTransforms, GL_STATIC_DRAW);
                glBindBuffer(GL_ARRAY_BUFFER, 0);
                
                // Create VAO and setup using method from Asset, to make it compatible for drawing assets.
                // Use our custom instance buffer, but the asset's vertex buffer.
                glGenVertexArrays(1, &vao);
                if (vao == 0) { std::cerr << "WARNING: TerrainScatter: " << "Failed to generate vertex array object.\n"; }
                Asset::setupVertexArrayAttributes(vao, assetManager->getShaderProgram(), asset->getVertexBuffer(), instanceBuffer);
                
                delete[] cumulativeScatterDensity;
                cumulativeScatterDensity = NULL;
                delete[] instanceTransforms;
                instanceTransforms = NULL;
                loadStage = LoadStages::done;
                onCompleteScatterSetup();
                std::cout << "Completed setup of terrain scatter (" << scatterTexFilename << ").\n";
            }
            break;
        
        // In "done" load stage, simply draw the terrain scatter.
        case LoadStages::done:
            glBindVertexArray(vao);
            asset->drawCustomVAO(particleCount);
            glBindVertexArray(0);
            break;
        }
        
        return loadStage;
    }
}
