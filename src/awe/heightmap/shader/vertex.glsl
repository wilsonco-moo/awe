#define SHADER_VERSION #version 330 core
SHADER_VERSION
#undef SHADER_VERSION

// The size (in terms of texture UV mapping) of each grid cell.
#define GRID_CELL_TEXTURE_SIZE 0.25f

// Inputs (vertex attributes).
in vec3 inNormal;
in float inHeight;
in float inMix;
in uint inTextureId1;
in uint inTextureId2;

// Outputs (to fragment shader).
out float vfMix;
flat out uint vfTextureId1;
flat out uint vfTextureId2;
out vec2 vfTexturePos;
out vec3 vfWorldPosition;

out vec3 vfTangentX;
out vec3 vfTangentY;
out vec3 vfNormalZ;

// Uniforms
uniform mat4 viewProjectionTransform;
uniform int mapSize;
uniform float mapHeight;
uniform float gridSpacing;

void main(void) {
    
    // Get grid coordinate from vertex ID.
    // Note that the vertex ID is the index within the vertex array, and the
    // vertex shader is not run multiple times for each index, even if that
    // index is repeated in the elements buffer.
    vec2 gridPos = vec2(gl_VertexID % mapSize, gl_VertexID / mapSize);
    
    // World position is grid position times spacing. Work out vertex position
    // using the view projection transformation.
    vec3 worldPos = vec3(gridPos * gridSpacing, inHeight * mapHeight);
    gl_Position = viewProjectionTransform * vec4(worldPos, 1.0f);
    
    // Work out the other two tangent vectors using cross product. We can get
    // away with doing this it in the vertex shader, since the three vectors
    // can just be interpolated independently to each other, with reasonable
    // results.
    vec3 tangentY = cross(inNormal, vec3(1, 0, 0)),
         tangentX = cross(tangentY, inNormal);
    
    // Provide other parameters to fragment shader. The texture position is the
    // world position multiplied by the size (in UV map) of each grid cell.
    vfMix = inMix;
    vfTextureId1 = inTextureId1;
    vfTextureId2 = inTextureId2;
    vfTexturePos = gridPos * GRID_CELL_TEXTURE_SIZE;
    vfWorldPosition = worldPos;
    vfTangentX = tangentX;
    vfTangentY = tangentY;
    vfNormalZ = inNormal;
}
