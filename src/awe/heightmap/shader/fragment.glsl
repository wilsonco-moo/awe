#define SHADER_VERSION #version 330 core
SHADER_VERSION
#undef SHADER_VERSION

#include "../../lighting/LightingShader.glh"

// Inputs (from vertex shader)
in float vfMix;
flat in uint vfTextureId1;
flat in uint vfTextureId2;
in vec2 vfTexturePos;
in vec3 vfWorldPosition;

in vec3 vfTangentX;
in vec3 vfTangentY;
in vec3 vfNormalZ;

// Outputs
out vec4 outColour;

// Texture
uniform sampler2DArray tex;

// Uniforms
uniform uint normalMapOffset;
uniform vec3 cameraPos;

// ------------------------- Quality settings ----------------------------------

/**
 * If enabled, mipmaps are all taken using textureGrad. This results in higher
 * quality parallax with fewer artifacts, but is much slower (especially on
 * intel graphics) - so should be used for highest quality setting.
 * 
 * Doing this results in non distorted mipmap derivatives, see the following: ("A note on filtering")
 * https://developer.nvidia.com/gpugems/gpugems2/part-i-geometric-complexity/chapter-8-pixel-displacement-mapping-distance-functions
 * http://web.archive.org/web/20200528125249/https://developer.nvidia.com/gpugems/gpugems2/part-i-geometric-complexity/chapter-8-pixel-displacement-mapping-distance-functions
 */
//#define HIGH_QUALITY_MIPMAP

/**
 * If defined, this disables parallax completely. This should be used for the
 * lowest quality setting.
 */
//#define DISABLE_PARALLAX


// Maximum height of parallax detail.
#ifndef PARALLAX_HEIGHT
    #define PARALLAX_HEIGHT   0.08f
#endif

// Number of ray trace (more is more accurate but slower).
#ifndef RAY_TRACE_SAMPLES
    #define RAY_TRACE_SAMPLES 4
#endif

// Step size between each sample (should be decreased as RAY_TRACE_SAMPLES is increased).
#ifndef RAY_TRACE_STEP
    #define RAY_TRACE_STEP    (4.0f / 512.0f)
#endif

// Offset of first ray position. Set to either 0 or 1. Using 1 allows better results
// with very few samples, although does introduce some artifacts (which are less noticable
// when HIGH_QUALITY_MIPMAP is disabled).
#ifndef RAY_START_OFFSET
    #define RAY_START_OFFSET 0.0f
#endif

// Parallax effect is interpolated with normal flat terrain between these two distances.
// Beyond PARALLAX_DIST_B, no parallax effect is used at all.
#ifndef PARALLAX_DIST_A
    #define PARALLAX_DIST_A   12.0f
#endif
#ifndef PARALLAX_DIST_B
    #define PARALLAX_DIST_B   16.0f
#endif

// This is used for texture access, depending on quality options.
#ifdef HIGH_QUALITY_MIPMAP
    #define TEXTURE_ACCESS(vec) textureGrad(tex, vec, dx, dy)
#else
    #define TEXTURE_ACCESS(vec) texture(tex, vec)
#endif

#ifndef DISABLE_PARALLAX
    /**
     * Parallax mapping using ray trace occlusion.
     * Inspired by:
     * http://cowboyprogramming.com/2007/01/05/parallax-mapped-bullet-holes/comment-page-1/
     * http://web.archive.org/web/20200726214412/http://cowboyprogramming.com/2007/01/05/parallax-mapped-bullet-holes/comment-page-1/s
     * 
     * And this:
     * https://web.archive.org/web/20150419215321/http://sunandblackcat.com/tipFullView.php?l=eng&topicid=28
     */
    #ifdef HIGH_QUALITY_MIPMAP
        vec2 getParallaxPos(vec3 lookVector, vec2 dx, vec2 dy) {
    #else
        vec2 getParallaxPos(vec3 lookVector) {
    #endif
        // Work out step between ray positions, and ray starting position.
        vec3 rayStep = lookVector * RAY_TRACE_STEP;
        vec3 rayPos = vec3(vfTexturePos, 0.0f) - rayStep * float(RAY_TRACE_SAMPLES + RAY_START_OFFSET);
        
        // Stores the depth for the previous and current sample.
        float depth = -PARALLAX_HEIGHT;
        float lastDepth;
        
        // Iterate until the ray goes underneath the terrain, or we run out of samples.
        // Move the ray position as the first operation, so we don't modify the ray
        // position at the end of the loop.
        for (int i = 0; i < RAY_TRACE_SAMPLES && depth < 0.0f; i++) {
            rayPos += rayStep;
            
            // Alpha channel of normal map texture is depth. Use textureGrad for improved performance.
            float depth1 = TEXTURE_ACCESS(vec3(rayPos.xy, vfTextureId1 + normalMapOffset)).a,
                  depth2 = TEXTURE_ACCESS(vec3(rayPos.xy, vfTextureId2 + normalMapOffset)).a;
            
            // Record previous depth, work out new depth. Depth is relative height of terrain compared to ray.
            lastDepth = depth;
            depth = mix(depth1, depth2, vfMix) * PARALLAX_HEIGHT - rayPos.z;
        }
        
        // Offset previous and current ray position by the look vector times the depth, to do parallax
        // effect. Interpolate previous and current by ray position.
        return rayPos.xy - mix(rayStep.xy + lookVector.xy * lastDepth,
                               lookVector.xy * depth,
                               (-lastDepth) / (depth - lastDepth));
    }
#endif

void main(void) {
    // Work out matrix representing rotation of terrain at this point.
    mat3 terrainRot = mat3(normalize(vfTangentX), normalize(vfTangentY), normalize(vfNormalZ));
    vec2 offsetTexPos = vfTexturePos;
    
    #ifndef DISABLE_PARALLAX
        // Find relative position of camera and normalised camera direction (transformed into tangent space).
        vec3 cameraRelPos = vfWorldPosition - cameraPos;
        vec3 lookVector = transpose(terrainRot) * normalize(cameraRelPos);
        
        // Get texture derivatives if needed.
        #ifdef HIGH_QUALITY_MIPMAP
            vec2 dx = dFdx(vfTexturePos);
            vec2 dy = dFdy(vfTexturePos);
        #endif
        
        // Only use parallax effect for regions close to the camera.
        float distanceFactor = clamp((length(cameraRelPos) - PARALLAX_DIST_A) * (1.0f / (PARALLAX_DIST_B - PARALLAX_DIST_A)), 0, 1);
        if (distanceFactor < 1.0f) {
            offsetTexPos = mix(
                #ifdef HIGH_QUALITY_MIPMAP
                    getParallaxPos(lookVector, dx, dy),
                #else
                    getParallaxPos(lookVector),
                #endif
                offsetTexPos, distanceFactor);
        }
    #endif
    
    // Work out texture colour.
    vec3 col1      = TEXTURE_ACCESS(vec3(offsetTexPos, vfTextureId1)).rgb,
         col2      = TEXTURE_ACCESS(vec3(offsetTexPos, vfTextureId2)).rgb,
         normal1   = TEXTURE_ACCESS(vec3(offsetTexPos, vfTextureId1 + normalMapOffset)).rgb - 0.5f,
         normal2   = TEXTURE_ACCESS(vec3(offsetTexPos, vfTextureId2 + normalMapOffset)).rgb - 0.5f,
         texCol    = mix(col1, col2, vfMix),
         texNormal = normalize(mix(normal1, normal2, vfMix));
    
    // Rotate the texture normal by the terrain's normal, by building a rotation
    // matrix from the normal and two tangents.
    vec3 normal = terrainRot * texNormal;
    
    // Use normal and position to work out lighting, use it for output colour.
    vec3 incidentLight = lighting_getTotal(vfWorldPosition, /*normalize(vfNormal)*/ /*texNormal*/ normal);
    outColour = vec4(min(texCol * incidentLight, 1.0f), 1.0f);
}
