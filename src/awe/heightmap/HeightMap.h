/*
 * HeightMap.h
 *
 *  Created on: 16 Jul 2020
 *      Author: wilson
 */

#ifndef AWE_HEIGHTMAP_HEIGHTMAP_H_
#define AWE_HEIGHTMAP_HEIGHTMAP_H_

#include <rascUI/util/EmplaceVector.h>
#include <unordered_map>
#include <cstddef>
#include <GL/gl.h>
#include <cstdint>
#include <atomic>
#include <string>
#include <vector>

#include "../util/types/Types.h"

namespace threading {
    class ThreadQueue;
}

namespace awe {
    class MipMapTexture;

    /**
     * Heightmap provides a convenient to use heightmap renderer.
     * It requires two images:
     *  > The height texture:
     *    This stores the height at each point, as a 16 bit value, using the
     *    RED channel as the most significant bit, and the GREEN channel as
     *    the least significant bit.
     *  > The terrain texture:
     *    The RED channel stores the texture mix as an 8 bit unsigned value,
     *    (0 means 100% texture 1, 255 means 100% texture 2). The GREEN and
     *    BLUE channels should store the texture IDs of texture1 and texture2
     *    respectively. The colours from each texture are linearly interpolated
     *    based on the mix.
     * 
     * This allows a heightmap, where at each point two textures can be
     * arbitrarily mixed, from a choice of upto 255 textures. This makes the
     * shaders and heightmap system very fast/simple, but can make designing the
     * map very difficult.
     * 
     * Note that the first half of the textures should be for colours, and the
     * second half should be for normal maps.
     * Assume you wanted to create a heightmap with 256 different textures
     * (the maximum). Each of these has an associated normal map, meaning
     * 512 textures will need to be loaded. The first 256 textures should be
     * the colours, and the next 256 textures should be the normal maps.
     * Internal texture IDs do not exceed 255. To work out the texture ID of a
     * normal map from a texture, 256 is added to the texture ID.
     * As a result of all of this, the texture count must be an even number.
     * If that is not true, a warning is printed in the constructor.
     * 
     * Note that HeightMap does not have any physics integration. For that,
     * see our subclass: PhysicsHeightMap.
     */
    class HeightMap {
    public:
    
        /**
         * The internal range of heights which our internal short-based
         * representation of height can store.
         */
        #define HEIGHTMAP_INTERNAL_HEIGHT_RANGE (((int)std::numeric_limits<short>::max()) - ((int)std::numeric_limits<short>::min()))
    
        /**
         * One of these is stored for each vertex, to allow outside access of
         * textures at each point.
         */
        class VertexTextureInfo {
        public:
            GLubyte mix,
                    textureId1,
                    textureId2;
        };
        
    private:
        // This class is used to represent a single vertex, in the format used
        // by the vertex buffer. An array of these is kept temporarily in
        // tempVertexBuffer. This is packed to make sure the compiler does not
        // add padding, which would mess up positioning.
        class VertexBufferVInfo {
        public:
            GLushort height;
            GLubyte mix,
                    textureId1,
                    textureId2;
            GLbyte normal[3];
        } __attribute__ ((__packed__));
        
        // -------------- OpenGL resources ---------------------
        
        // Data and index buffers for vertices.
        GLuint vertexDataBuffer, vertexIndexBuffer;
    
        // Our vertex array object, linked to the vertex data buffer.
        GLuint vao;
        
        // Our texture array. This is assigned once textures finish loading.
        GLuint textureArr;
        
        // -----------------------------------------------------
        
        // Thread queue and a token.
        threading::ThreadQueue * threadQueue;
        void * token;
        
        // Size of the map (in grid cells).
        size_t mapSize;
        
        // Grid spacing and map height (see documentation in constructor).
        GLfloat gridSpacing, mapHeight;
        
        // The size of the textures which the heightmap uses.
        GLsizei baseTexSize;
        
        // Number of vertex indices (for drawing, calculated from map size).
        GLsizei vertexIndices;
        
        // Used for keeping track of texture and vertex loading.
        std::atomic<unsigned int> loadRemaining;
        
        // Map image IDs to texture IDs map, count of textures and actual textures.
        std::unordered_map<unsigned int, unsigned int> textureIdMap;
        unsigned int textureCount;        
        rascUI::EmplaceVector<MipMapTexture> terrainTextures;
        
        // Temporary RAM copies of our vertex and index buffers. These are
        // loaded in the background thread, then copied to the GPU in
        // setupVertexBuffers, then immediately freed from memory as they're
        // no longer needed.
        VertexBufferVInfo * tempVertexBuffer;
        GLuint * tempIndexBuffer;
        
        // Permanent RAM copies of the texture info at each vertex, and height
        // at each vertex. These are kept so queries can be done about terrain
        // types and heights. The heights are kept as shorts (-32768 - +32767),
        // for integration with bullet physics. These are also loaded in the
        // background thread.
        VertexTextureInfo * textureInfo;
        short * heightInfo;
        
        // Used in manageLoad to split loading operations across multiple
        // frames. This is initially zero.
        unsigned int loadStep;
        
        // Stores true once we finish loading.
        bool isLoaded;
        
        // The shader program provided to us in the most recent "step" call.
        // Before drawing, we compare the current one to this. If they're
        // different, we update uniforms to make sure the current shader knows
        // stuff like the size of the terrain.
        GLuint lastShaderProgram;
        
        // Don't allow copying: We hold raw OpenGL resources.
        HeightMap(const HeightMap & other);
        HeightMap & operator = (const HeightMap & other);
        
    public:
        /**
         * The main constructor for heightmap.
         * 
         *        threadQueue: A thread queue for asynchronous loading.
         *            mapSize: The width and height, in grid cells, of the map.
         *                     It is expected that heightmap and terrain map textures
         *                     map this resolution. This is passed to the shader
         *                     as a uniform.
         *        gridSpacing: The size (in the world) of a single grid cell. The total
         *                     width and height of the heightmap is
         *                     mapSize * gridSpacing. This is passed to the shader
         *                     as a uniform.
         *          mapHeight: The total height of the heightmap, in world coordinates.
         *                     This is the z coordinate of the maximum height level.
         *                     This is passed to the shader as a uniform.
         *        baseTexSize: The resolution for all textures used for displaying the
         *                     terrain. If they do not match this, they will be scaled
         *                     when loaded.
         *  heightTexFilename: The filename for the image file storing terrain height
         *                     at each point. The resolution of this is expected to
         *                     be mapSize X mapSize, (otherwise a "no texture" texture
         *                     is generated.
         * terrainTexFilename: The filename for the image file storing terrain type
         *                     data, at each point. The resolution of this is expected
         *                     to be mapSize X mapSize, (otherwise a "no texture"
         *                     texture is generated.
         *       textureIdMap: An unordered_map which specifies the appropriate
         *                     consecutive internal texture ID (id within
         *                     textureFilenames), for each texture ID from
         *                     terrainTexFilename. Mapping are required for each colour
         *                     texture, but not for each normal map texture.
         *   textureFilenames: Filenames for each texture to be used for the heightmap.
         *                     The size of this must not exceed 512.
         */
        HeightMap(threading::ThreadQueue * threadQueue,
                  size_t mapSize, GLfloat gridSpacing, GLfloat mapHeight,
                  GLsizei baseTexSize, std::string heightTexFilename,
                  std::string terrainTexFilename,
                  const std::unordered_map<unsigned int, unsigned int> & textureIdMap,
                  const std::vector<std::string> & textureFilenames);
        virtual ~HeightMap(void);
        
        
    private:
        // ================== Internal helper methods ==========================
    
        // Converts a texture ID from an ID used by the terrain texture, to a real
        // texture ID. This uses textureIdMap.
        // If the texture ID cannot be found, zero is returned as a default
        // texture ID.
        unsigned int convertTextureId(unsigned int fromTextureImage) const;
    
        // Run from a separate thread, loads vertices from texture files.
        // Decrements loadRemaining when done.
        void loadVertices(const std::string & heightTexFilename, const std::string & terrainTexFilename);
        
        // Run from OpenGL thread, sets up buffers from RAM copy of vertices.
        // This must be called AFTER loadVertices completes.
        void setupVertexBuffers(void);
        
        // Creates and sets up our VAO, from our vertex buffer and shader program.
        // The shader program is only used for accessing offsets, and does not need to be bound.
        // This must be called AFTER setupVertexBuffers.
        void setupVertexArrayObject(GLuint shaderProgram);
        
        // Creates our texture array.
        void createTextureArray(void);
        
        // Sets up uniforms for mapSize, gridSpacing and mapHeight within the shader.
        void setupUniforms(GLuint shaderProgram) const;
        
        // Called each frame, once all asynchronous load jobs have finished,
        // until isLoaded is reported as true. The job of manageLoad is to
        // load OpenGL resources.
        void manageLoad(unsigned int maxTime, GLuint shaderProgram);
        
    protected:
        /**
         * This is called ONCE, within the render thread, immediately after
         * loading completes, (i.e: the first point in time when getIsLoaded()
         * returns true). This can be used by subclasses to initialise systems
         * which require completed loading to use.
         * By default this does nothing.
         */
        virtual void onLoaded(GLuint shaderProgram);
        
    public:
        // ==================== Static utility methods =========================
        
        /**
         * Uses cross product to calculate the normal at the specified position
         * in the heightmap.
         */
        static GLvec3 calculateTerrainNormal(size_t mapSize, GLfloat gridSpacing, GLfloat mapHeight, short * heightData, size_t x, size_t y);

        /**
         * Given the provided vector, which is assumed to already be normalised,
         * this stores it into three bytes.
         */
        static void compressNormal(GLbyte * output, const GLvec3 & normal);

        // ========================== Public API ===============================
    
        /**
         * Manages asynchronous loading, and draws once loading is done.
         * Note that the provided shader program should be in use, for drawing
         * purposes.
         * 
         * Provided is the maximum time (milliseconds) that loading operations
         * should be allowed to take. Returned is an estimate of the number of
         * pending load operations.
         * 
         * Note that the provided shader program can be changed from frame to
         * frame. If it is different to the shader program of the previous
         * frame, before we draw uniforms are updated.
         */
        unsigned int step(unsigned int maxTime, GLuint shaderProgram);
        
        
        // ======================== Accessor methods ===========================
        
        /**
         * Returns true once we have finished loading.
         */
        inline bool getIsLoaded(void) const {
            return isLoaded;
        }
        
        /**
         * Allows access to the map size.
         */
        inline size_t getMapSize(void) const {
            return mapSize;
        }
        
        /**
         * Allows access to the grid spacing.
         */
        inline GLfloat getGridSpacing(void) const {
            return gridSpacing;
        }
        
        /**
         * Accesses the real visible map size, (i.e: map size minus one, times
         * grid spacing). Note that this does not include the last row and
         * column, since they are not shown visibly.
         */
        inline GLfloat getRealVisibleMapSize(void) const {
            return (getMapSize() - 1) * getGridSpacing();
        }
        
        /**
         * Allows access to the map height.
         */
        inline GLfloat getMapHeight(void) const {
            return mapHeight;
        }
        
        /**
         * Allows access to the base texture size.
         */
        inline GLsizei getBaseTexSize(void) const {
            return baseTexSize;
        }
        
        /**
         * Accesses texture info for the specified vertex.
         * NOTE: This is only defined once we are loaded (after getIsLoaded
         *       returns true).
         */
        inline const VertexTextureInfo & getVertexTextureInfo(unsigned int x, unsigned int y) const {
            return textureInfo[x + y * mapSize];
        }
        
        /**
         * Allows const access to the array of vertex texture info.
         * NOTE: This is only defined once we are loaded (after getIsLoaded
         *       returns true).
         */
        inline const VertexTextureInfo * getVertexTextureInfo(void) const {
            return textureInfo;
        }
        
        /**
         * Allows const access to the array of height info. This is helpful
         * for physics integration.
         * NOTE: This is only defined once we are loaded (after getIsLoaded
         *       returns true).
         */
        inline const short * getHeightInfo(void) const {
            return heightInfo;
        }
        
        /**
         * Accesses the raw height, as a short value, at the specified integer
         * coordinates. The coordinates must exist within the heightmap.
         * NOTE: This is only defined once we are loaded (after getIsLoaded
         *       returns true).
         */
        inline short getRawHeight(unsigned int x, unsigned int y) const {
            return heightInfo[x + y * mapSize];
        }
        
        /**
         * Accesses the true height, as a float value, taking into account map
         * height, at the specified integer coordinates. The coordinates must
         * exist within the heightmap.
         * NOTE: This is only defined once we are loaded (after getIsLoaded
         *       returns true).
         */
        GLfloat getRealHeight(unsigned int x, unsigned int y) const;
        
        /**
         * Accesses the interpolated true height, as a float value, taking into
         * account map height AND GRID SPACING, at the specified floating point
         * coordinates. When these coordinates are exact multiples of grid
         * spacing AND valid coordinates, this returns the same as an equivalent
         * call to getRealHeight. For non-multiple (of gridSpacing) valid
         * coordinates, this returns the triangle-interpolated true height for
         * that point on the terrain's surface. For floating point coordinates
         * outside the terrain, this returns either an invented or nonsense
         * height value which is sometimes the height of the closest point on
         * the terrain, (note that this is a LEGAL and NOT UNDEFINED operation
         * for ANY pair of float values). 
         */
        GLfloat getRealInterpHeight(GLfloat x, GLfloat y) const;
    };
}

#endif
