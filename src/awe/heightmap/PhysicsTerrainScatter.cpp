/*
 * PhysicsTerrainScatter.cpp
 *
 *  Created on: 2 Sep 2020
 *      Author: wilson
 */

#include "PhysicsTerrainScatter.h"

#include <bullet3/BulletCollision/CollisionShapes/btCollisionShape.h>
#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <bullet3/LinearMath/btTransform.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

#include "../util/physics/InstanceCollisionShapeDef.h"
#include "../util/physics/PhysicsWorld.h"
#include "../asset/Asset.h"

namespace awe {
    
    PhysicsTerrainScatter::PhysicsTerrainScatter(threading::ThreadQueue * threadQueue, AssetManager * assetManager, const std::string & assetFilename, HeightMap * heightmap, const std::string & scatterTexFilename, GLfloat minScale, GLfloat maxScale, GLfloat zOffset, bool randomiseRotations, size_t particleCount, PhysicsWorld * physicsWorld, unsigned long long seed, unsigned int texChannel) :
        TerrainScatter(threadQueue, assetManager, assetFilename, heightmap, scatterTexFilename, minScale, maxScale, zOffset, randomiseRotations, particleCount, seed, texChannel),
        
        physicsWorld(physicsWorld),
        transforms(),
        collisionShapes(),
        rigidBodies(getParticleCount()) {
        
        transforms.reserve(getParticleCount());
    }
    
    PhysicsTerrainScatter::~PhysicsTerrainScatter(void) {
        for (btRigidBody & rigidBody : rigidBodies) {
            physicsWorld->getDynamicsWorld().removeRigidBody(&rigidBody);
        }
        for (btCollisionShape * collisionShape : collisionShapes) {
            delete collisionShape;
        }
    }
    
    void PhysicsTerrainScatter::onSetupScatterInstance(const GLmat4 & baseTransform, GLfloat scale, const GLquat & rotation, const GLvec3 & location) {
        transforms.push_back({scale, rotation, location});
    }
    
    void PhysicsTerrainScatter::onCompleteScatterSetup(void) {
        
        // Do nothing if the number of scatter instances is wrong.
        if (transforms.size() != getParticleCount()) {
            std::cerr << "PhysicsTerrainScatter: Wrong number of scatter instances.\n";
            return;
        }
        
        for (const InternalTransform & internalTransform : transforms) {
            GLvec3 scale = GLvec3(internalTransform.scale, internalTransform.scale, internalTransform.scale);
            
            // Get collision shape and transformations using InstanceCollisionShapeDef.
            GLfloat totalMass = 0.0f;
            GLmat4 physicsToWorldTransform, worldToPhysicsTransform;
            btCollisionShape * primaryShape = InstanceCollisionShapeDef::populateCollisionShapes(
                                                 collisionShapes, totalMass, physicsToWorldTransform, 
                                                 worldToPhysicsTransform, getAsset()->getCollisionShapeDef(), scale);
            
            // For initial world transform, apply world->physics transform, then scale then rotate then translate.
            // Note that scaling is necessary since the world->physics transform has an inverted scale baked into it.
            btRigidBody::btRigidBodyConstructionInfo cInfo(0.0f, NULL, primaryShape);
            cInfo.m_startWorldTransform.setFromOpenGLMatrix(glm::value_ptr(
                glm::translate(GLmat4(1.0f), internalTransform.location) *
                glm::mat4_cast(internalTransform.rotation) *
                glm::scale(GLmat4(1.0f), scale) *
                worldToPhysicsTransform
            ));
            
            rigidBodies.emplace(cInfo);
            physicsWorld->getDynamicsWorld().addRigidBody(&rigidBodies.back());
        }
        
        // Empty the transforms vector since it is no longer needed.
        transforms.clear();
        transforms.shrink_to_fit();
        
        // Shrink the collision shapes vector since we won't add to it again.
        collisionShapes.shrink_to_fit();
    }
}
