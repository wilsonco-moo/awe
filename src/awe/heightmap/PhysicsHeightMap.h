/*
 * PhysicsHeightMap.h
 *
 *  Created on: 21 Jul 2020
 *      Author: wilson
 */

#ifndef AWE_HEIGHTMAP_PHYSICSHEIGHTMAP_H_
#define AWE_HEIGHTMAP_PHYSICSHEIGHTMAP_H_

#include <bullet3/LinearMath/btMotionState.h>

#include "HeightMap.h"

class btHeightfieldTerrainShape;
class btTransform;
class btRigidBody;

namespace awe {
    class PhysicsWorld;

    /**
     * This is a simple subclass of HeightMap - which provides a static
     * rigid body for the heightmap.
     * See HeightMap.h for more information about heightmaps.
     */
    class PhysicsHeightMap : public HeightMap, public btMotionState {
    private:
        PhysicsWorld * physicsWorld;
        
        // Both of these are initialise during onLoaded.
        btHeightfieldTerrainShape * terrainShape;
        btRigidBody * rigidBody;
        
        // Don't allow copying: We hold raw resources.
        PhysicsHeightMap(const PhysicsHeightMap & other);
        PhysicsHeightMap & operator = (const PhysicsHeightMap & other);
        
    public:
        /**
         * The main constructor for PhysicsHeightMap. Compared to HeightMap,
         * we additionally require a PhysicsWorld, into which our static rigid
         * body is placed.
         */
        PhysicsHeightMap(threading::ThreadQueue * threadQueue, PhysicsWorld * physicsWorld,
                         size_t mapSize, GLfloat gridSpacing, GLfloat mapHeight,
                         GLsizei baseTexSize, std::string heightTexFilename,
                         std::string terrainTexFilename,
                         const std::unordered_map<unsigned int, unsigned int> & textureIdMap,
                         const std::vector<std::string> & textureFilenames);
        virtual ~PhysicsHeightMap(void);
        
    protected:
        // Here we initialise the rigid body for the heightmap.
        virtual void onLoaded(GLuint shaderProgram) override;
    
    public:
        // ----------------------- Motion integration --------------------------
        
        virtual void getWorldTransform(btTransform & worldTrans) const override;
        virtual void setWorldTransform(const btTransform & worldTrans) override;
    };
}

#endif
