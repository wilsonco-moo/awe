/*
 * PhysicsHeightMap.cpp
 *
 *  Created on: 21 Jul 2020
 *      Author: wilson
 */

#include "PhysicsHeightMap.h"

#include <bullet3/BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#include <bullet3/BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <bullet3/LinearMath/btVector3.h>
#include <limits>

#include "../util/physics/PhysicsWorld.h"
#include "../util/Util.h"

namespace awe {
    
    PhysicsHeightMap::PhysicsHeightMap(threading::ThreadQueue * threadQueue, PhysicsWorld * physicsWorld, size_t mapSize, GLfloat gridSpacing, GLfloat mapHeight, GLsizei baseTexSize, std::string heightTexFilename, std::string terrainTexFilename, const std::unordered_map<unsigned int, unsigned int> & textureIdMap, const std::vector<std::string> & textureFilenames) :
        HeightMap(threadQueue, mapSize, gridSpacing, mapHeight, baseTexSize, heightTexFilename, terrainTexFilename, textureIdMap, textureFilenames),
        physicsWorld(physicsWorld),
        terrainShape(NULL),
        rigidBody(NULL) {
    }
    
    PhysicsHeightMap::~PhysicsHeightMap(void) {
        if (terrainShape != NULL) {
            physicsWorld->getDynamicsWorld().removeRigidBody(rigidBody);
            delete rigidBody;
            delete terrainShape;
        }
    }
    
    void PhysicsHeightMap::onLoaded(GLuint shaderProgram) {

        terrainShape = new btHeightfieldTerrainShape(
            getMapSize(), getMapSize(),                       // Grid width/height
            getHeightInfo(),                                  // Height data (short array)
            getMapHeight() / HEIGHTMAP_INTERNAL_HEIGHT_RANGE, // Height scale (make HEIGHTMAP_INTERNAL_HEIGHT_RANGE values fit into map height).
            getMapHeight() * -0.5f,                           // Minimum height (half the map height below the origin)
            getMapHeight() * 0.5f,                            // Maximum height (half the map height above the origin)
            2,                                                // Up axis (Z) is 2.
            PHY_SHORT,                                        // We are providing it with short data.
            true                                              // Whether to flip quad edges. We *do* want to do this, to stay consistent with how HeightMap does quads.
        );
        
        // This is the only way to scale the heightmap on the X and Y axis.
        // Set the heightmap's horizontal scale to the grid spacing.
        terrainShape->setLocalScaling(btVector3(getGridSpacing(), getGridSpacing(), 1.0f));
        
        // Build the accelerator for the terrain shape (this speeds up raycasting).
        terrainShape->buildAccelerator();
        
        // Create a rigid body with zero mass (static). Don't bother setting inertia - terrain doesn't move.
        rigidBody = new btRigidBody(0.0f, this, terrainShape);
        physicsWorld->getDynamicsWorld().addRigidBody(rigidBody);
    }
    
    void PhysicsHeightMap::getWorldTransform(btTransform & worldTrans) const {
        // Return a position in the centre of the heightmap, don't rotate.
        // Note that the centre of the heightmap is not actually
        // mapSize*spacing/2, it is (mapSize-1)*spacing/2, because the
        // heightmap doesn't cover the last row/column.
        GLfloat pos = (getMapSize() - 1) * getGridSpacing() * 0.5f;
        worldTrans.setOrigin(btVector3(pos, pos, getMapHeight() * 0.5f));
        worldTrans.setRotation(btQuaternion::getIdentity());
    }
    
    void PhysicsHeightMap::setWorldTransform(const btTransform & worldTrans) {
        // This should never be called: we create a static rigid body.
    }
}
