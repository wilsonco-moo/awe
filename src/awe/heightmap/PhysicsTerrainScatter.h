/*
 * PhysicsTerrainScatter.h
 *
 *  Created on: 2 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_HEIGHTMAP_PHYSICSTERRAINSCATTER_H_
#define AWE_HEIGHTMAP_PHYSICSTERRAINSCATTER_H_

#include <rascUI/util/EmplaceVector.h>
#include <GL/gl.h>
#include <vector>

#include "../util/types/Types.h"
#include "TerrainScatter.h"

class btCollisionShape;
class btRigidBody;

namespace awe {
    class PhysicsWorld;

    /**
     * PhysicsTerrainScatter world the same as TerrainScatter, except that each
     * scatter instance is added to a physics world, as STATIC objects.
     */
    class PhysicsTerrainScatter : public TerrainScatter {
    private:        
        // Stores transforms between onSetupScatterInstance and onCompleteScatterSetup.
        class InternalTransform {
        public:
            GLfloat scale;
            GLquat rotation;
            GLvec3 location;
        };
    
        PhysicsWorld * physicsWorld;
        std::vector<InternalTransform> transforms;
        std::vector<btCollisionShape *> collisionShapes;
        rascUI::EmplaceVector<btRigidBody> rigidBodies;
        
        // Don't allow copying: we hold raw resources.
        PhysicsTerrainScatter(const PhysicsTerrainScatter & other);
        PhysicsTerrainScatter & operator = (const PhysicsTerrainScatter & other);
        
    public:
        /**
         * Compared to TerrainScatter, we additionally require a PhysicsWorld.
         * 
         * See constructor in TerrainScatter.h for more documentation about
         * these parameters.
         */
        PhysicsTerrainScatter(threading::ThreadQueue * threadQueue, AssetManager * assetManager,
                              const std::string & assetFilename, HeightMap * heightmap,
                              const std::string & scatterTexFilename, GLfloat minScale, GLfloat maxScale,
                              GLfloat zOffset, bool randomiseRotations, size_t particleCount,
                              PhysicsWorld * physicsWorld,
                              unsigned long long seed = 0, unsigned int texChannel = 0);
        virtual ~PhysicsTerrainScatter(void);
        
    protected:
        virtual void onSetupScatterInstance(const GLmat4 & baseTransform, GLfloat scale, const GLquat & rotation, const GLvec3 & location) override;
        virtual void onCompleteScatterSetup(void) override;
    };
}

#endif
