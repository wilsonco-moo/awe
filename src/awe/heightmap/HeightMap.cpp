/*
 * HeightMap.cpp
 *
 *  Created on: 16 Jul 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "HeightMap.h"

#include <wool/texture/lodepng/lodepng.h>
#include <threading/ThreadQueue.h>
#include <glm/common.hpp>
#include <functional>
#include <iostream>
#include <cstdlib>
#include <utility>
#include <limits>
#include <chrono>
#include <cmath>

#include "../texture/TextureManager.h"
#include "../texture/MipMapTexture.h"
#include "../util/colour/Channels.h"
#include "../util/Util.h"

namespace awe {
    
    HeightMap::HeightMap(threading::ThreadQueue * threadQueue, size_t mapSize, GLfloat gridSpacing, GLfloat mapHeight, GLsizei baseTexSize, std::string heightTexFilename, std::string terrainTexFilename, const std::unordered_map<unsigned int, unsigned int> & textureIdMap, const std::vector<std::string> & textureFilenames) :
        vertexDataBuffer(0),
        vertexIndexBuffer(0),
        vao(0),
        textureArr(0),
    
        threadQueue(threadQueue),
        token(threadQueue->createToken()),
        
        mapSize(mapSize),
        gridSpacing(gridSpacing),
        mapHeight(mapHeight),
        baseTexSize(baseTexSize),
        vertexIndices((mapSize - 1) * (mapSize - 1) * 6), // Six vertices for each complete grid cell, (i.e: all grid cells except those at the right or bottom edge).
        loadRemaining(0),
        
        textureIdMap(textureIdMap),
        textureCount(textureFilenames.size()),
        terrainTextures(textureCount),
        
        tempVertexBuffer(NULL),
        tempIndexBuffer(NULL),
        textureInfo(NULL),
        heightInfo(NULL),
        
        loadStep(0),
        isLoaded(false),
        lastShaderProgram(0) {
        
        // Complain and give up if the map size is less than two (since that
        // would not make sense as a heightmap.
        if (mapSize < 2) {
            std::cerr << "ERROR: HeightMap: " << "Heightmap size cannot be less than two, as that does not makes sense for a heightmap.\n";
            exit(EXIT_FAILURE);
        }
        
        if ((textureCount & 1) != 0) {
            std::cerr << "WARNING: HeightMap: " << "The texture count must be an even number, since the second half of the textures are intended to be normal maps, corresponding to the first half of the textures.\n";
        }
        
        // Increment load remaining and add job to load vertices.
        loadRemaining.fetch_add(1, std::memory_order_release);
        threadQueue->add(token, [this, heightTexFilename = std::move(heightTexFilename), terrainTexFilename = std::move(terrainTexFilename)](void) {
            loadVertices(heightTexFilename, terrainTexFilename);
        });
        
        // Add terrain textures (and their respective load jobs).
        for (const std::string & textureFilename : textureFilenames) {
            terrainTextures.emplace(threadQueue, textureFilename, baseTexSize, &loadRemaining);
        }
        
        // Make sure the compiler hasn't done anything funny with the layout
        // of VertexInfo - we need this for exchanging data with the graphics card.
        static_assert(sizeof(GLbyte) == 1 &&
                      sizeof(GLubyte) == 1 &&
                      sizeof(GLushort) == 2 &&
                      sizeof(VertexBufferVInfo) == 8 &&
                      offsetof(VertexBufferVInfo, height) == 0 &&
                      offsetof(VertexBufferVInfo, mix) == 2 &&
                      offsetof(VertexBufferVInfo, textureId1) == 3 &&
                      offsetof(VertexBufferVInfo, textureId2) == 4 &&
                      offsetof(VertexBufferVInfo, normal) == 5 &&
                      sizeof(VertexBufferVInfo::normal) == 3);
                      
        // Make sure that the "short" is at least as big as GLushort.
        // For physics integration, it needs to be able to store out 16 bit
        // height values.
        static_assert(sizeof(short) >= sizeof(GLushort));
    }
    
    HeightMap::~HeightMap(void) {
        threadQueue->deleteToken(token);
        glDeleteBuffers(1, &vertexDataBuffer);  // Note that OpenGL delete calls silently ignore zeroes, so
        glDeleteBuffers(1, &vertexIndexBuffer); // these are fine to do even if we have not created resources yet.
        glDeleteVertexArrays(1, &vao);
        glDeleteTextures(1, &textureArr);
        
        delete[] tempVertexBuffer; // Delete temporary buffers, just in case this has not been done so already (i.e: If we were
        delete[] tempIndexBuffer;  // deleted while running the background thread).
        delete[] textureInfo;
        delete[] heightInfo;
    }
    
    unsigned int HeightMap::convertTextureId(unsigned int fromTextureImage) const {
        auto iter = textureIdMap.find(fromTextureImage);
        if (iter == textureIdMap.end()) {
            return 0;
        }
        return iter->second;
    }
    
    void HeightMap::loadVertices(const std::string & heightTexFilename, const std::string & terrainTexFilename) {
        
        // ------------------------ Load textures ------------------------------
        
        // Load in height tex data (24 bit). Complain and use no texture texture
        // if it is the wrong size or fails to load.
        unsigned char * heightTexData = NULL;
        {
            unsigned int width, height,
                         error = lodepng_decode24_file(&heightTexData, &width, &height, heightTexFilename.c_str());
            
            if (error != 0 || width != mapSize || height != mapSize) {
                std::cerr << "Warning: HeightMap: " << "Failed to load texture " << heightTexFilename << ", it is either wrong size or missing.\n";
                free(heightTexData);
                heightTexData = TextureManager::generateNoTextureTexture(mapSize);
            }
        }
        
        // Load in terrain tex data (24 bit). Complain and use no texture texture
        // if it is the wrong size or fails to load.
        unsigned char * terrainTexData = NULL;
        {
            unsigned int width, height,
                         error = lodepng_decode24_file(&terrainTexData, &width, &height, terrainTexFilename.c_str());
            
            if (error != 0 || width != mapSize || height != mapSize) {
                std::cerr << "Warning: HeightMap: " << "Failed to load texture " << terrainTexFilename << ", it is either wrong size or missing.\n";
                free(terrainTexData);
                terrainTexData = TextureManager::generateNoTextureTexture(mapSize);
            }
        }
        
        std::cout << "Loaded heightmap height and terrain textures.\n";
        
        // ------------------ Set up vertex buffer and height data -------------
        
        // Allocate buffers.
        size_t mapArea = mapSize * mapSize;
        VertexBufferVInfo * tempVertexBufferInternal = new VertexBufferVInfo[mapArea];
        VertexTextureInfo * textureInfoInternal = new VertexTextureInfo[mapArea];
        short * heightInfoInternal = new short[mapArea];
        
        // Setup "upto" variables.
        unsigned char * heightTexUpto = heightTexData,
                      * terrainTexUpto = terrainTexData;
        VertexBufferVInfo * vertexUpto = tempVertexBufferInternal,
                          * vertexEnd = tempVertexBufferInternal + mapArea;
        VertexTextureInfo * textureInfoUpto = textureInfoInternal;
        short * heightInfoUpto = heightInfoInternal;
        
        // Setup each vertex, for vertex buffer, texture info and height info.
        while(vertexUpto != vertexEnd) {
            
            int height = (heightTexUpto[Channels::red] << 8) | heightTexUpto[Channels::green],
                   mix = terrainTexUpto[Channels::red],
            textureId1 = convertTextureId(terrainTexUpto[Channels::green]),
            textureId2 = convertTextureId(terrainTexUpto[Channels::blue]);
            
            vertexUpto->height = height;
            vertexUpto->mix = mix;
            vertexUpto->textureId1 = textureId1;
            vertexUpto->textureId2 = textureId2;
            vertexUpto++;
            
            textureInfoUpto->mix = mix;
            textureInfoUpto->textureId1 = textureId1;
            textureInfoUpto->textureId2 = textureId2;
            textureInfoUpto++;
            
            *(heightInfoUpto++) = (short)(std::numeric_limits<short>::min() + height);
            
            heightTexUpto += 3;
            terrainTexUpto += 3;
        }
        
        // Free the texture data now we no longer need it.
        free(heightTexData);
        free(terrainTexData);
        
        // --------------------- Calculate terrain normals ---------------------
        
        for (GLuint iy = 0; iy < mapSize; iy++) {
            for (GLuint ix = 0; ix < mapSize; ix++) {
                GLvec3 normal = calculateTerrainNormal(mapSize, gridSpacing, mapHeight, heightInfoInternal, ix, iy);
                compressNormal(tempVertexBufferInternal[ix + iy * mapSize].normal, normal);
            }
        }
        
        // ----------------------- Set up index buffer data --------------------
        
        // Create a temporary RAM allocation for the index buffer.
        GLuint * tempIndexBufferInternal = new GLuint[vertexIndices];
        
        // Iterate through each complete grid cell (i.e: all those which are not at
        // the right or bottom edge).
        GLuint * indexDataUpto = (GLuint *)tempIndexBufferInternal;
        for (GLuint iy = 0; iy < mapSize - 1; iy++) {
            for (GLuint ix = 0; ix < mapSize - 1; ix++) {
                
                // Draw two triangles for each complete grid cell.
                *(indexDataUpto++) = (iy * mapSize + ix);
                *(indexDataUpto++) = ((iy + 1) * mapSize + ix + 1);
                *(indexDataUpto++) = ((iy + 1) * mapSize + ix);
                *(indexDataUpto++) = (iy * mapSize + ix);
                *(indexDataUpto++) = (iy * mapSize + ix + 1);
                *(indexDataUpto++) = ((iy + 1) * mapSize + ix + 1);
            }
        }
        
        // ---------------------------------------------------------------------
        
        // Store buffers into our fields.
        tempVertexBuffer = tempVertexBufferInternal;
        tempIndexBuffer = tempIndexBufferInternal;
        textureInfo = textureInfoInternal;
        heightInfo = heightInfoInternal;
        
        std::cout << "Completed setup of heightmap data.\n";
        
        // Decrement this since we have finished loading vertex data.
        loadRemaining.fetch_sub(1, std::memory_order_release);
    }
    
    void HeightMap::setupVertexBuffers(void) {
        
        // Work out size for vertex data and index data.
        GLsizei vertexDataSize = mapSize * mapSize * sizeof(VertexBufferVInfo),
                vertexIndexSize = vertexIndices * sizeof(GLuint);
        
        // Generate, bind, and send data for vertex data buffer.
        // Free our temporary RAM copy of the vertex buffer when we're done.
        glGenBuffers(1, &vertexDataBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, vertexDataBuffer);
        glBufferData(GL_ARRAY_BUFFER, vertexDataSize, tempVertexBuffer, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        delete[] tempVertexBuffer;
        tempVertexBuffer = NULL;
        
        // Generate, bind, and send data for index data buffer.
        // Free our temporary RAM copy of the index buffer when we're done.
        glGenBuffers(1, &vertexIndexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, vertexIndexSize, tempIndexBuffer, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        delete[] tempIndexBuffer;
        tempIndexBuffer = NULL;
        
        // Make sure this is used: Heightmaps polygons must be shaded from the
        // vertices above-left of them.
        glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
    }
    
    void HeightMap::setupVertexArrayObject(GLuint shaderProgram) {
        
        // Create and bind the vertex array object.
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        
        // Get the locations within the vertex shader of the 4 inputs.
        GLuint heightLoc = glGetAttribLocation(shaderProgram, "inHeight"),
               mixLoc    = glGetAttribLocation(shaderProgram, "inMix"),
               texId1Loc = glGetAttribLocation(shaderProgram, "inTextureId1"),
               texId2Loc = glGetAttribLocation(shaderProgram, "inTextureId2"),
               normalLoc = glGetAttribLocation(shaderProgram, "inNormal");
        
        // Bind the array buffer, so the glVertexAttribPointer calls know where to go.
        glBindBuffer(GL_ARRAY_BUFFER, vertexDataBuffer);
        
        // Create each of the 4 attributes. Height and mix must come
        // through as normalised floats, the other two just as integers.
        glVertexAttribPointer (heightLoc, 1, GL_UNSIGNED_SHORT, GL_TRUE, sizeof(VertexBufferVInfo), (void *)offsetof(VertexBufferVInfo, height));
        glVertexAttribPointer (mixLoc,    1, GL_UNSIGNED_BYTE,  GL_TRUE, sizeof(VertexBufferVInfo), (void *)offsetof(VertexBufferVInfo, mix));
        glVertexAttribIPointer(texId1Loc, 1, GL_UNSIGNED_BYTE,           sizeof(VertexBufferVInfo), (void *)offsetof(VertexBufferVInfo, textureId1));
        glVertexAttribIPointer(texId2Loc, 1, GL_UNSIGNED_BYTE,           sizeof(VertexBufferVInfo), (void *)offsetof(VertexBufferVInfo, textureId2));
        glVertexAttribPointer (normalLoc, 3, GL_BYTE,           GL_TRUE, sizeof(VertexBufferVInfo), (void *)offsetof(VertexBufferVInfo, normal));
        
        // This doesn't need to be bound any more.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // Enable each of the 4 attributes.
        glEnableVertexAttribArray(heightLoc);
        glEnableVertexAttribArray(mixLoc);
        glEnableVertexAttribArray(texId1Loc);
        glEnableVertexAttribArray(texId2Loc);
        glEnableVertexAttribArray(normalLoc);
        
        // Unbind VAO now as no longer needed.
        glBindVertexArray(0);
    }
    
    void HeightMap::createTextureArray(void) {
        
        // Allocate an ID for the texture and bind it.
        glGenTextures(1, &textureArr);
        glBindTexture(GL_TEXTURE_2D_ARRAY, textureArr);
        
        // Set texture parameters: Wrap at edges, linear mipmap linear interpolation.
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        
        // Allocate memory for the texture array. Repeat this for each mipmap level.
        unsigned int level = 0;
        for (unsigned int mipMapSize : TextureManager::getMipMapTextureSizes(baseTexSize)) {
            glTexImage3D(
                GL_TEXTURE_2D_ARRAY,      // Target
                level,                    // Mipmap level to specify.
                GL_RGBA8,                 // Internal format
                mipMapSize, mipMapSize,   // Size of this mipmap level (width, height)
                textureCount,             // Depth (number of textures in array): One for each texture we have.
                0,                        // Border (nobody knows what it does, but must be 0).
                GL_RGBA,                  // General format of pixel data (layout)
                GL_UNSIGNED_BYTE,         // Data type of pixel data
                NULL                      // Initial data (specify as null since we provide the data later with glTexSubImage3D.
            );
            level++;
        }
        
        // Unbind the texture now we have created it.
        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    }
    
    void HeightMap::setupUniforms(GLuint shaderProgram) const {
        GLuint normalMapOffsetUniform = Util::getUniformLocationChecked(shaderProgram, "HeightMap::setupUniforms", "normalMapOffset"),
                   gridSpacingUniform = Util::getUniformLocationChecked(shaderProgram, "HeightMap::setupUniforms", "gridSpacing"),
                     mapHeightUniform = Util::getUniformLocationChecked(shaderProgram, "HeightMap::setupUniforms", "mapHeight"),
                       mapSizeUniform = Util::getUniformLocationChecked(shaderProgram, "HeightMap::setupUniforms", "mapSize");

        glUniform1f(gridSpacingUniform, gridSpacing);
        glUniform1f(mapHeightUniform, mapHeight);
        glUniform1i(mapSizeUniform, mapSize);
        glUniform1ui(normalMapOffsetUniform, textureCount/2);
    }
    
    void HeightMap::manageLoad(unsigned int maxTime, GLuint shaderProgram) {
        
        // The number of main load steps in the case statement.
        #define MAIN_LOAD_STEPS 3
        
        // Work out mipmap level count, and total number of load steps.
        unsigned int levels        = TextureManager::getMipMapLevels(baseTexSize),
                     loadStepCount = MAIN_LOAD_STEPS + levels * textureCount;
        
        // Loop until we either run out of time, or run out of load steps.
        std::chrono::time_point<std::chrono::steady_clock> startTime = std::chrono::steady_clock::now();
        while(loadStep < loadStepCount && std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startTime).count() <= maxTime) {
            
            // Each iteration run the appropriate load operation, for the load step value.
            switch(loadStep) {
                case 0: setupVertexBuffers();                  break;
                case 1: setupVertexArrayObject(shaderProgram); break;
                case 2: createTextureArray();                  break;
                default: {
                    glBindTexture(GL_TEXTURE_2D_ARRAY, textureArr);
                    unsigned int layer = (loadStep - MAIN_LOAD_STEPS) / levels,
                                 level = (loadStep - MAIN_LOAD_STEPS) % levels;
                    terrainTextures[layer].loadMipMapLevelIntoGpu(layer, level);
                    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
                    break;
                }
            }
            
            loadStep++;
        }
        
        // When the load step reaches the total load step count, loading is complete, so
        // notify that and run onLoaded. After this is done, manageLoad will not be called again.
        if (loadStep == loadStepCount) {
            std::cout << "Heightmap initialised.\n";
            isLoaded = true;
            onLoaded(shaderProgram);
        }
    }
    
    void HeightMap::onLoaded(GLuint shaderProgram) {
    }
    
    // Note: triangles arranged like this:
    //    0,0 _____ 1,0
    //       |\    |        |\|\|\|
    //       |  \  |        |\|\|\|
    //       |____\|        |\|\|\|
    //    0,1       1,1
    //
    GLvec3 HeightMap::calculateTerrainNormal(size_t mapSize, GLfloat gridSpacing, GLfloat mapHeight, short * heightData, size_t x, size_t y) {
        
        // Accesses the terrain height (as a double), at posX,posY.
        #define CALC_TERR_HEIGHT(posX, posY) \
            (((double)heightData[(posX) + (posY) * mapSize] * mapHeight) / HEIGHTMAP_INTERNAL_HEIGHT_RANGE)
        
        // The centre point height.
        double mainHeight = CALC_TERR_HEIGHT(x, y);
        
        // Calculates the vector to the specified grid position offset, from posX,posY.
        #define CALC_TERR_VEC(posX, posY, offsetX, offsetY) \
            (GLvec3(gridSpacing * (offsetX), gridSpacing * (offsetY), CALC_TERR_HEIGHT((posX) + (offsetX), (posY) + (offsetY)) - mainHeight))
        
        // Accumulated normals
        GLvec3 normalAcc(0.0f, 0.0f, 0.0f);
        
        // Calculate these here to avoid having to work them out twice.
        GLvec3 left, right;
        if (x >= 1)          left  = CALC_TERR_VEC(x, y, -1, 0);
        if (x < mapSize - 1) right = CALC_TERR_VEC(x, y, 1, 0);
        
        if (y >= 1) {
            GLvec3 up = CALC_TERR_VEC(x, y, 0, -1);
            
            // Two triangles in above-left square.
            if (x >= 1) {
                GLvec3 diagonalL = CALC_TERR_VEC(x, y, -1, -1);
                normalAcc += glm::normalize(glm::cross(left, diagonalL));
                normalAcc += glm::normalize(glm::cross(diagonalL, up));
            }
            
            // Triangle in above-right square.
            if (x < mapSize - 1) {
                normalAcc += glm::normalize(glm::cross(up, right));
            }
        }
        
        if (y < mapSize - 1) {
            GLvec3 down = CALC_TERR_VEC(x, y, 0, 1);
            
            // Two triangles in bottom-right square.
            // Note that we already worked out right, so don't need to do it again.
            if (x < mapSize - 1) {
                GLvec3 diagonalR = CALC_TERR_VEC(x, y, 1, 1);
                normalAcc += glm::normalize(glm::cross(right, diagonalR));
                normalAcc += glm::normalize(glm::cross(diagonalR, down));
            }
            
            // Triangle in below-left square.
            // Note that we already worked out left so don't need to do it again.
            if (x >= 1) {
                normalAcc += glm::normalize(glm::cross(down, left));
            }
        }
        
        return glm::normalize(normalAcc);
    }
    
    void HeightMap::compressNormal(GLbyte * output, const GLvec3 & normal) {
        output[0] = (GLbyte)(normal.x * std::numeric_limits<GLbyte>::max());
        output[1] = (GLbyte)(normal.y * std::numeric_limits<GLbyte>::max());
        output[2] = (GLbyte)(normal.z * std::numeric_limits<GLbyte>::max());
    }
    
    unsigned int HeightMap::step(unsigned int maxTime, GLuint shaderProgram) {
        // If not loaded yet, run manageLoad if there are no remaining textures
        if (!isLoaded) {
            unsigned int remaining = loadRemaining.load(std::memory_order_acquire);
            if (remaining == 0) {
                manageLoad(maxTime, shaderProgram);
            }
            return remaining + 1;
        }
        
        // Otherwise, if loaded, do drawing.
        
        // Make sure uniforms are up to date (update them if shader program changed).
        if (shaderProgram != lastShaderProgram) {
            lastShaderProgram = shaderProgram;
            setupUniforms(shaderProgram);
        }
        
        // Bind texture array.
        glBindTexture(GL_TEXTURE_2D_ARRAY, textureArr);
        
        // Bind vertex array object and index buffer.
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
        
        // Draw vertexIndices vertices.
        glDrawElements(GL_TRIANGLES, vertexIndices, GL_UNSIGNED_INT, 0);
        
        // Unbind the buffer, vertex array and texture array.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
        
        return 0;
    }
    
    GLfloat HeightMap::getRealHeight(unsigned int x, unsigned int y) const {
        return ((heightInfo[x + y * mapSize] - (int)std::numeric_limits<short>::min()) * (GLfloat)mapHeight) / HEIGHTMAP_INTERNAL_HEIGHT_RANGE;
    }
    
    GLfloat HeightMap::getRealInterpHeight(GLfloat x, GLfloat y) const {
        // Work out mix factors and grid cell coordinates. Grid coordinates are at most
        // mapSize-2, since we will need to access the grid cells below and right for interpolation.
        GLfloat xMix = x / gridSpacing,
                yMix = y / gridSpacing;
        unsigned int gridX = std::min((unsigned int)(int)std::floor(xMix), (unsigned int)mapSize - 2),
                     gridY = std::min((unsigned int)(int)std::floor(yMix), (unsigned int)mapSize - 2);
        xMix = fmod(xMix, 1.0f);
        yMix = fmod(yMix, 1.0f);
        
        // Get heights at top-left and bottom-right (used for both).
        GLfloat heightTL = heightInfo[gridX + gridY * mapSize],
                heightBR = heightInfo[(gridX + 1) + (gridY + 1) * mapSize];
        
        GLfloat interpHeight;
        
        // 0,0 -------
        //     |\  TR|
        //     |  \  |
        //     |BL  \|
        //     ------- 1,1
        
        // For top-right (TR) triangle: Guess bottom left by duplicating top line, do bilinear interpolation.
        if (xMix > yMix) {
            GLfloat heightTR = heightInfo[(gridX + 1) + gridY * mapSize],
                    guessBL = heightBR + (heightTL - heightTR);
            
            interpHeight = glm::mix(glm::mix(heightTL, heightTR, xMix),
                                    glm::mix(guessBL,  heightBR, xMix), yMix);
            
        // For bottom-left (BL) triangle: Guess top right by duplicating top line, do bilinear interpolation.
        } else {
            GLfloat heightBL = heightInfo[gridX + (gridY + 1) * mapSize],
                    guessTR = heightTL + (heightBR - heightBL);
                    
            interpHeight = glm::mix(glm::mix(heightTL, guessTR,  xMix),
                                    glm::mix(heightBL, heightBR, xMix), yMix);
        }
        
        // Convert to real world height.
        return (interpHeight - (GLfloat)std::numeric_limits<short>::min()) * mapHeight / HEIGHTMAP_INTERNAL_HEIGHT_RANGE;
    }
}
