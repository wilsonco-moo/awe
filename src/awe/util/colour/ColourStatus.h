/*
 * ColourStatus.h
 *
 *  Created on: 27 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_COLOUR_COLOURSTATUS_H_
#define AWE_UTIL_COLOUR_COLOURSTATUS_H_

#include <cstdint>
#include <GL/gl.h>
#include <array>

#include "../colour/Channels.h"
#include "../types/Types.h"

namespace awe {

    /**
     * This class is intended to be inherited from. It adds the capability for
     * another class to store an RGBA "current colour". Amongst other things,
     * this is helpful for draw buffer classes.
     * 
     * Colour status internally stores colours as a 4 GLubyte array. It allows
     * assignment of colours using many different methods, to make use of this
     * class as convenient as possible. These methods roughly follow the
     * OpenGL function type suffixes.
     */
    class ColourStatus {
    private:
        std::array<GLubyte, 4> currentColour;

    public:
    
        /**
         * The default colour is fully opaque blue, in order to make default
         * constructed colours obvious.
         */
        ColourStatus(std::array<GLubyte, 4> defaultColour = {0, 0, 255, 255});
        ColourStatus(std::array<GLubyte, 3> defaultColour);
        virtual ~ColourStatus(void);
        
        
        // ------------------- Colour setting methods --------------------------
        
        /**
         * Sets the current draw colour using three float values (assumed to
         * be within the range 0-1). Default alpha of 1 is used.
         */
        inline void setColour3f(GLfloat red, GLfloat green, GLfloat blue) {
            setColour4f(red, green, blue, 1.0f);
        }
        
        /**
         * Sets the current draw colour using four float values (assumed to
         * be within the range 0-1).
         */
        void setColour4f(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
        
        /**
         * Sets the current draw colour using a floating point GLM three-vector
         * (assumed to be within the range 0-1). Default alpha of 1 is used.
         */
        inline void setColour3v(const GLvec3 & colour) {
            setColour4v(GLvec4(colour, 1.0f));
        }
        
        /**
         * Sets the current draw colour using a floating point GLM four-vector
         * (assumed to be within the range 0-1).
         */
        void setColour4v(const GLvec4 & colour);
        
        /**
         * Sets the current draw colour using three byte values. Default alpha
         * of 255 is used.
         * 
         * Note that setting colour using byte arrays or byte values (like this
         * method does) is the recommended method to set colours, especially
         * when using constant values. This is because it avoids the additional
         * operations of converting the colours from floating point values to
         * bytes.
         */
        inline void setColour3b(GLubyte red, GLubyte green, GLubyte blue) {
            setColour4a({red, green, blue, 255});
        }
        
        /**
         * Sets the current draw colour using three byte values. Default alpha
         * of 255 is used.
         * 
         * Note that setting colour using byte arrays or byte values (like this
         * method does) is the recommended method to set colours, especially
         * when using constant values. This is because it avoids the additional
         * operations of converting the colours from floating point values to
         * bytes.
         */
        inline void setColour4b(GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha) {
            setColour4a({red, green, blue, alpha});
        }
        
        /**
         * Sets the current draw colour using a three-byte RGBA array. Default
         * alpha of 255 is used.
         * 
         * Note that setting colour using byte arrays or byte values (like this
         * method does) is the recommended method to set colours, especially
         * when using constant values. This is because it avoids the additional
         * operations of converting the colours from floating point values to
         * bytes.
         */
        inline void setColour3a(std::array<GLubyte, 3> colour) {
            setColour4a({colour[Channels::red], colour[Channels::green], colour[Channels::blue], 255});
        }
        
        /**
         * Sets the current draw colour using a four-byte RGBA array.
         *
         * Note that setting colour using byte arrays or byte values (like this
         * method does) is the recommended method to set colours, especially
         * when using constant values. This is because it avoids the additional
         * operations of converting the colours from floating point values to
         * bytes.
         */
        void setColour4a(std::array<GLubyte, 4> colour);
        
        /**
         * Sets the current draw colour from a 32 bit RGBA int. Note that
         * red is the most significant byte, and alpha is the least significant
         * byte (so is compatible with wool::RGBA).
         */
        void setColourInt(uint32_t colour);
        
        
        // ---------------------- Accessor methods -----------------------------
        
        /**
         * Allows access to the current colour, as the internal format: a
         * 4 byte RGBA array.
         */
        inline std::array<GLubyte, 4> getCurrentColour(void) const {
            return currentColour;
        }
    };
}

#endif
