/*
 * ColourStatus.cpp
 *
 *  Created on: 27 Aug 2020
 *      Author: wilson
 */

#include "ColourStatus.h"

namespace awe {

    ColourStatus::ColourStatus(std::array<GLubyte, 4> defaultColour) :
        currentColour(defaultColour) {
            
        // Assert that the compiler has not done anything funny with padding
        // of the array (since this is used in buffer classes for talking to
        // the graphics card).
        static_assert(sizeof(GLubyte) == 1 &&
                      sizeof(std::array<GLubyte, 4>) == 4);
    }
    
    ColourStatus::ColourStatus(std::array<GLubyte, 3> defaultColour) :
        ColourStatus({defaultColour[Channels::red],
                      defaultColour[Channels::green],
                      defaultColour[Channels::blue], 255}) {
    }
    
    ColourStatus::~ColourStatus(void) {
    }
    
    void ColourStatus::setColour4f(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) {
        currentColour = {(GLubyte)(red   * 255.0f),
                         (GLubyte)(green * 255.0f),
                         (GLubyte)(blue  * 255.0f),
                         (GLubyte)(alpha * 255.0f)};
    }
    
    void ColourStatus::setColour4v(const GLvec4 & colour) {
        currentColour = {(GLubyte)(colour.r * 255.0f),
                         (GLubyte)(colour.g * 255.0f),
                         (GLubyte)(colour.b * 255.0f),
                         (GLubyte)(colour.a * 255.0f)};
    }
    
    void ColourStatus::setColour4a(std::array<GLubyte, 4> colour) {
        currentColour = colour;
    }
    
    void ColourStatus::setColourInt(uint32_t colour) {
        currentColour = {(GLubyte)((colour >> 24) & 255),
                         (GLubyte)((colour >> 16) & 255),
                         (GLubyte)((colour >> 8 ) & 255),
                         (GLubyte)( colour        & 255)};
    }
}
