/*
 * Channels.h
 *
 *  Created on: 27 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_COLOUR_CHANNELS_H_
#define AWE_UTIL_COLOUR_CHANNELS_H_

namespace awe {
    
    /**
     * This macro defines, as an X macro, the names of each colour channel.
     */
    #define CHANNELS_CHANNEL_NAMES_DEF \
        X(red)                         \
        X(green)                       \
        X(blue)                        \
        X(alpha)
    
    /**
     * This convenient enum/class defines colour channel names, from the above
     * macro. This should be used in place of indices (like col[0], col[1] ...)
     * for the sake of readability.
     * For example: Channels::red, Channels::green ...
     * 
     * The special value Channels::COUNT is set to the number of
     * colour channels (4).
     */
    class Channels {
    public:
        #define X(channelName) channelName,
        enum { CHANNELS_CHANNEL_NAMES_DEF COUNT };
        #undef X
    };
    
    /**
     * This defines, for each channel, no texture texture colours. This is used
     * by TextureManager.
     */
    #define CHANNELS_NO_TEXTURE_TEXTURE_COLOURS \
        X(red,   165, 255)                      \
        X(green, 160, 201)                      \
        X(blue,  50,  0  )                      \
        X(alpha, 255, 255)
}

#endif
