/*
 * ProgressControl.cpp
 *
 *  Created on: 10 Aug 2020
 *      Author: wilson
 */

#include "ProgressControl.h"

#include <algorithm>
#include <iostream>

namespace awe {

    ProgressControl::ProgressControl(float slop) : 
        slop(slop),
        isFirst(true),
        lastRemaining(0),
        lastProgressEstimate(0.0f),
        
        lastTickRemaining(0),
        lastPeakRemaining(0),
        completedJobs(0),
        lastTickChange(1),
        lastTickProgress(0.0f),
        baseProgress(0.0f),
        progressMultiplier(0.0f) {
    }
    
    float ProgressControl::progressTick(unsigned int remaining) {
        
        int change = (int)remaining - (int)lastTickRemaining;
        float outputProgress;
        
        // If we found a peak, set the progress multiplier so completing this
        // whole peak fills in slop times the remaining progress.
        // Reset completed jobs, set the last peak to current.
        if (change < 0 && lastTickChange > 0) {
            baseProgress = lastTickProgress;
            progressMultiplier = (1.0f - baseProgress) * slop;
            completedJobs = 0;
            lastPeakRemaining = lastTickRemaining;
        }
        
        // If something got completed.
        if (change < 0) {
            // Update completed jobs
            completedJobs += (unsigned int)(-change);
            
            // Always finish when zero things remain.
            if (remaining == 0) {
                outputProgress = 1.0f;
                
            // Otherwise work out from proportion towards last peak.
            // Use std::max so we NEVER go lower than previous progress,
            // since that would send progress backward.
            } else {
                outputProgress = std::max(lastTickProgress,
                                   baseProgress + progressMultiplier * ((float)completedJobs / lastPeakRemaining));
            }
            
        // If more stuff got added, always return the same as last time
        // (ignore further remaining changes until there is a peak).
        } else {
            outputProgress = lastTickProgress;
        }
        
        lastTickRemaining = remaining;
        lastTickChange = change;
        lastTickProgress = outputProgress;
        
        return outputProgress;
    }
    
    void ProgressControl::estimateProgress(float * progressEstimate, bool * hasChanged, unsigned int remaining) {
        // Discard initial zero remaining values.
        if (isFirst && remaining == 0) {
            *hasChanged = false;
            *progressEstimate = 0.0f;
            return;
        }
        
        // Only run progressTick if remaining has changed, or we are first.
        if (remaining == lastRemaining && !isFirst) {
            *hasChanged = false;
        } else {
            float newProgressEstimate = progressTick(remaining);
            // There is a change if we are first, or the progress estimate actually changed.
            *hasChanged = (isFirst || newProgressEstimate != lastProgressEstimate);
            isFirst = false;
            lastRemaining = remaining;
            lastProgressEstimate = newProgressEstimate;
        }
        *progressEstimate = lastProgressEstimate;
    }
    
    void ProgressControl::reset(float newSlop) {
        *this = ProgressControl(newSlop);
    }
    
    void ProgressControl::printProgress(float progress, unsigned int length) {
        // Make sure 0 prints no characters.
        if (progress <= 0.0f) progress = -1.0f;
        
        // Fill characters where their proportion is less than or equal to progress.
        char * str = new char[length + 2],
             * strUpto = str;
        for (unsigned int i = 0; i < length; i++) {
            *(strUpto++) = ((float)i / (length - 1) <= progress) ? '=' : '-';
        }
        strUpto[0] = '\n';
        strUpto[1] = '\0';
        std::cout << str;
        delete[] str;
    }
}
