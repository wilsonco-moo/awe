/*
 * Vertex.cpp
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#include "Vertex.h"

namespace awe {

    // Make sure the compiler hasn't done anything funny with the layout of Vertex.
    static_assert(
        sizeof(Vertex)             == sizeof(GLvec3) + sizeof(GLvec2) &&
        offsetof(Vertex, position) == 0                               &&
        offsetof(Vertex, uv)       == sizeof(GLvec3)
    );
    
    // Make sure the compiler hasn't done anything funny with the layout of SkelVertex.
    static_assert(
        sizeof(SkelVertex)                == 2*sizeof(GLvec3) + 2*sizeof(GLuint) + sizeof(GLfloat) + sizeof(GLvec2) &&
        offsetof(SkelVertex, position1)   == 0                                                                      &&
        offsetof(SkelVertex, position2)   ==   sizeof(GLvec3)                                                       &&
        offsetof(SkelVertex, skelNodeId1) == 2*sizeof(GLvec3)                                                       &&
        offsetof(SkelVertex, skelNodeId2) == 2*sizeof(GLvec3) +   sizeof(GLuint)                                    &&
        offsetof(SkelVertex, mix)         == 2*sizeof(GLvec3) + 2*sizeof(GLuint)                                    &&
        offsetof(SkelVertex, uv)          == 2*sizeof(GLvec3) + 2*sizeof(GLuint) + sizeof(GLfloat)
    );
}
