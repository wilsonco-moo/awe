/*
 * Vertex.h
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_GEOMETRY_VERTEX_H_
#define AWE_UTIL_GEOMETRY_VERTEX_H_

#include "../types/Types.h"

namespace awe {

    /**
     * Vertex is a simple class which contains all the information
     * necessary for an ordinary vertex.
     */
    class Vertex {
    public:
        GLvec3 position;
        GLvec2 uv;
    };
    
    /**
     * SkelVertex stores all the information necessary for a skeleton
     * based vertex.
     */
    class SkelVertex {
    public:
        GLvec3 position1, position2;
        GLuint skelNodeId1, skelNodeId2;
        GLfloat mix;
        GLvec2 uv;
    };
}

#endif
