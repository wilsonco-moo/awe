/*
 * SkelNode.cpp
 *
 *  Created on: 6 Jan 2020
 *      Author: wilson
 */

#include "SkelNode.h"

#include <glm/gtc/matrix_transform.hpp>
#include <tinyxml2/tinyxml2.h>
#include <iostream>
#include <limits>

#include "../Util.h"

namespace awe {
    
    // MUST HAVE THE SAME value as MAX_NODES in the the shader.
    const unsigned int SkelNode::MAX_NODES = 8;

    SkelNode::SkelNode(GLmat4 baseTransform, GLfloat radius) :
        baseTransform(baseTransform),
        radius(radius),
        calcPos(),
        calcTransform(),
        children() {
    }
    
    void SkelNode::updateCalc(const GLmat4 & transformWithin) {
        calcTransform = transformWithin * baseTransform;
        calcPos = GLvec3(calcTransform * GLvec4(0,0,0,1));
        for (long long childOffset : children) {
            (this + childOffset)->updateCalc(calcTransform);
        }
    }
    
    void SkelNode::addChild(SkelNode * child) {
        children.push_back(child - this);
    }
    
    bool SkelNode::buildSkelNodesFromXML(std::vector<SkelNode> & vector, tinyxml2::XMLElement * element) {
        // When any XML errors happen, print the error, make sure vector is cleared, and return false.
        #define PRINT_XML_ERROR(xmlErr)                                        \
            std::cerr << "WARNING: Failed to load skeleton:\n" xmlErr << '\n'; \
            vector.clear();                                                    \
            return false;
        
        // Get the count of how many skeleton nodes there are.
        GET_ATTRIB_UINT(nodesCount, element, "nodes")
        if (nodesCount < 2) {
            PRINT_XML_ERROR("Skeleton must have at least 2 nodes.")
        }
        // Complain if too many nodes are specified.
        if (nodesCount > MAX_NODES) {
            PRINT_XML_ERROR("Skeleton cannot have more than " << MAX_NODES << "nodes.")
        }
        
        // Reserve the correct number of nodes. This will avoid the vector
        // reallocating itself while we populate it.
        vector.clear();
        vector.reserve(nodesCount);
        
        // Go through each node, count as we go along.
        unsigned long int foundNodesCount = 0;
        ELEM_CHILDREN_FOR_EACH(skelNode, element, "skelNode") {
            
            // Complain if we *find* too many nodes. Stops us eating up too much memory if wayyy more nodes than specified are found.
            if (foundNodesCount >= MAX_NODES) {
                PRINT_XML_ERROR("Skeleton cannot have more than " << MAX_NODES << "nodes." << " Also, more nodes found than specified.")
            }
            
            // Check listed ID is correct.
            GET_ATTRIB_UINT(nodeId, skelNode, "id")
            if (nodeId != foundNodesCount) { PRINT_XML_ERROR("Node " << foundNodesCount << " has wrong ID: " << nodeId << '.') }
            foundNodesCount++;
            
            // Read properties of this node.
            GET_ELEM(propertiesElem, skelNode, "properties")
                GET_ATTRIB_GLFLOAT(radius, propertiesElem, "radius")
                GLmat4 baseTransform = Util::readTransformation(propertiesElem);
            
            // Create a SkelNode.
            vector.emplace_back(baseTransform, radius);
            
            // Populate it's children.
            GET_ELEM(children, skelNode, "children")
            ELEM_CHILDREN_FOR_EACH(child, children, "childNode") {
                GET_ATTRIB_UINT(childId, child, "id")
                // Use pointer arithmetic, to avoid doing anything undefined. The child may not have been initialised yet,
                // but we can still generate a pointer to it by offsetting from the first one, which ALWAYS exists.
                vector.back().addChild((&vector[0]) + childId);
            }
        }
        
        // Complain if there are wrong number of nodes.
        if (foundNodesCount != nodesCount) {
            PRINT_XML_ERROR("Wrong number of skelNodes: Found " << foundNodesCount << ", expected " << nodesCount << '.')
        }
        
        // Since the operations was successful, initialise the calculated positions and transformations of all nodes in the tree.
        // Start from the root node, which is always node 0.
        vector[0].updateCalc();
        
        return true;
    }
    
    void SkelNode::convertToSkelNodeVertices(std::vector<SkelVertex> & outputSkelVertices, const std::vector<Vertex> & inputVertices, const std::vector<SkelNode> & skelNodes) {
        
        // Clear output to start with.
        outputSkelVertices.clear();
        
        for (const Vertex & vertex : inputVertices) {
            
            GLfloat closestDist = std::numeric_limits<GLfloat>::max(),
                    secondClosestDist = std::numeric_limits<GLfloat>::max();
            
            GLuint closestId = 0,
                   secondClosestId = 0;
            
            // Find the closest skeleton node.
            for (GLuint i = 0; i < skelNodes.size(); i++) {
                GLfloat dist = glm::distance(vertex.position, skelNodes[i].getCalcPos());
                if (dist < closestDist) {
                    closestDist = dist;
                    closestId = i;
                }
            }
            
            // Find the second closest skeleton node.
            for (GLuint i = 0; i < skelNodes.size(); i++) {
                GLfloat dist = glm::distance(vertex.position, skelNodes[i].getCalcPos());
                if (i != closestId && dist < secondClosestDist) {
                    secondClosestDist = dist;
                    secondClosestId = i;
                }
            }
            
            const SkelNode & closestNode       = skelNodes[closestId],
                           & secondClosestNode = skelNodes[secondClosestId];
            
            GLfloat closestRadius       = closestNode.getRadius(),
                    secondClosestRadius = secondClosestNode.getRadius();
            
            // Build a SkelVertex and fill it with appropriate data.
            SkelVertex v;
            v.skelNodeId1 = closestId;
            v.skelNodeId2 = secondClosestId;
            v.position1 = vertex.position - closestNode.getCalcPos();
            v.position2 = vertex.position - secondClosestNode.getCalcPos();
            v.mix = (closestDist - closestRadius) / (closestDist + secondClosestDist - closestRadius - secondClosestRadius);
            v.uv = vertex.uv;
            
            outputSkelVertices.push_back(v);
        }
    }
}
