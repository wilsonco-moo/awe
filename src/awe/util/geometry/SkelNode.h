/*
 * SkelNode.h
 *
 *  Created on: 6 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_GEOMETRY_SKELNODE_H_
#define AWE_UTIL_GEOMETRY_SKELNODE_H_

#include <GL/gl.h>
#include <vector>

#include "../types/Types.h"
#include "Vertex.h"

namespace tinyxml2 {
    class XMLElement;
}

namespace awe {

    /**
     * SkelNode represents a single point, within the skeleton of an object,
     * used for deformation. The positions of verticesm when deformed, have
     * their position linearly interpolated between their relative position to
     * their closest two SkelNodes.
     * 
     * Each SkelNode has a radius, so is conceptually a sphere. These spheres
     * must not overlap when first created.
     * 
     * Typically, the skeleton of an object should be a vector of SkelNode
     * instances. Index zero is alrays the root node, and all other nodes should
     * be child nodes, forming a tree.
     * 
     * Each SkelNode has two transformations:
     *  > Base transformation (baseTransform).
     *    - The transformation of THIS node relative to THIS node's parent.
     *  > Calculated transformation (calcTransform), (and thus calculated position: calcPos).
     *    - The overall transformation of this node, with all parent nodes' transformations applied.
     *    - Vertices near us should be transformed by this.
     *
     * NOTE: A group of SkelNode instances MUST be stored contiguously. They use
     *       the offset from themselves to address their children.
     *       This makes it legal to copy and std::vector of SkelNodes,
     *       but makes it undefined behaviour to have a SkelNode in a DIFFERENT
     *       std::vector, as a child of another SkelNode.
     */
    class SkelNode {
    public:
        /**
         * The maximum number of skeleton nodes in an array. This MUST HAVE THE
         * SAME value as MAX_NODES in the the shader.
         */
        static const unsigned int MAX_NODES;
        
    private:
        GLmat4 baseTransform;
        GLfloat radius;
        
        GLvec3 calcPos;
        GLmat4 calcTransform;
        
        // This stores our children, as a POINTER OFFSET from ourself.
        std::vector<long long> children;
    
    public:
        /**
         * Provided should be our base transformation, and our radius.
         */
        SkelNode(GLmat4 baseTransform, GLfloat radius);
        
        /**
         * Updates the calculated positions and transformations of our child
         * nodes, using the specified transformation as a base transformation.
         * This should be applied to the root node, (with the default parameter:
         * the identity matrix), any time the base transformation of any node in
         * the tree is changed.
         */
        void updateCalc(const GLmat4 & transformWithin = GLmat4(1.0f));
        
        /**
         * Adds a child node to this node.
         */
        void addChild(SkelNode * node);
        
        /**
         * Returns our radius. Vertices closer to us than this radius will be
         * not be affected by any other node.
         */
        inline const GLfloat getRadius(void) const {
            return radius;
        }
        
        /**
         * Returns our calculated position, i.e: Our base transformation applied
         * to the calculated position of our parent SkelNode.
         */
        inline const GLvec3 & getCalcPos(void) const {
            return calcPos;
        }
        
        /**
         * Returns our calculatd transformation. Vertices attached to this node
         * should have their relative position to us transformed by this, to
         * work out their deformed position.
         */
        inline const GLmat4 & getCalcTransform(void) const {
            return calcTransform;
        }
        
        /**
         * Changes our base transformation, (our transformation relative to our
         * parent SkelNode).
         */
        inline void setBaseTransform(const GLmat4 & transform) {
            baseTransform = transform;
        }
        
        /**
         * Builds a std::vector of skeleton nodes, from the provided XML element.
         * This automatically runs updateCalc on the root node, using the identity
         * matrix, to initialise the calculated transformations and positions of all nodes.
         * 
         * If successful, returns true.
         * Otherwise prints complaints and returns false, leaving the provided vector empty
         * (since it is cleared at the start of this function).
         */
        static bool buildSkelNodesFromXML(std::vector<SkelNode> & vector, tinyxml2::XMLElement * element);
        
        /**
         * Clears and populates outputSkelVertices with:
         * skeleton vertices generated from the ordinary vertices in inputVertices,
         * using the skeleton nodes defined by skelNodes.
         * 
         * This takes an amount of time proportional to the number of input vertices,
         * multiplied by the number of skeleton nodes.
         */
        static void convertToSkelNodeVertices(std::vector<SkelVertex> & outputSkelVertices, const std::vector<Vertex> & inputVertices, const std::vector<SkelNode> & skelNodes);        
    };
}

#endif
