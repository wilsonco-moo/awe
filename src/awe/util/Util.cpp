/*
 * Util.cpp
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "Util.h"

#include <bullet3/BulletCollision/CollisionShapes/btCollisionShape.h>
#include <glm/gtc/matrix_transform.hpp>
#include <tinyxml2/tinyxml2.h>
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <GL/gl.h>
#include <sstream>
#include <cctype>
#include <cstdio>
#include <ios>

#include "physics/PhysicsWorld.h"

namespace awe {

    std::string Util::mat4ToString(const GLmat4 & matrix, bool hex, bool newlines) {
        std::stringstream str;
        if (hex) {
            str << std::hexfloat;
        }
        for (unsigned int iy = 0; iy < 4; iy++) {
            for (unsigned int ix = 0; ix < 4; ix++) {
                str << matrix[ix][iy];
                if (ix != 3) str << ' ';
            }
            if (iy != 3) {
                if (newlines) {
                    str << '\n';
                } else {
                    str << ' ';
                }
            }
        }
        return str.str();
    }
    
    GLmat4 Util::stringToMat4(const std::string & string) {
        GLmat4 matrix;
        const char * upTo = &string[0];
        for (unsigned int iy = 0; iy < 4; iy++) {
            for (unsigned int ix = 0; ix < 4; ix++) {
                char * endPtr;
                matrix[ix][iy] = std::strtof(upTo, &endPtr);
                if (endPtr == upTo) {
                    std::cerr << "WARNING: Util::stringToMat4: Invalid matrix format:\n" << string << "\n";
                    break;
                }
                upTo = endPtr;
            }
        }
        if (*upTo != '\0') {
            std::cerr << "WARNING: Util::mat4ToString: Invalid matrix format.\n";
        }
        return matrix;
    }
    
    uint32_t Util::roundUpToPower2(uint32_t value) {
        // Reference: https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
        //            http://web.archive.org/web/20200120124112/https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
        value--;
        value |= value >> 1;
        value |= value >> 2;
        value |= value >> 4;
        value |= value >> 8;
        value |= value >> 16;
        return value + 1;
    }
    
    inline GLfloat squared(GLfloat v) { return v * v; }
    bool Util::doesCubeIntersectSphere(const GLvec3 & C1, const GLvec3 & C2, const GLvec3 & S, GLfloat R) {
        // Reference: https://stackoverflow.com/a/4579069
        //            http://web.archive.org/web/20170322205504/https://stackoverflow.com/questions/4578967/cube-sphere-intersection-test
        GLfloat dist_squared = R * R;
        // Here we Assume that C1 and C2 are element-wise sorted.
        if (S.x < C1.x) dist_squared -= squared(S.x - C1.x);
        else if (S.x > C2.x) dist_squared -= squared(S.x - C2.x);
        if (S.y < C1.y) dist_squared -= squared(S.y - C1.y);
        else if (S.y > C2.y) dist_squared -= squared(S.y - C2.y);
        if (S.z < C1.z) dist_squared -= squared(S.z - C1.z);
        else if (S.z > C2.z) dist_squared -= squared(S.z - C2.z);
        return dist_squared > 0;
    }
    
    bool Util::doesCubeIntersectSphereUnsorted(const GLvec3 & cubeCorner1, const GLvec3 & cubeCorner2, const GLvec3 & sphereCentre, GLfloat sphereRadius) {
        return doesCubeIntersectSphere(glm::min(cubeCorner1, cubeCorner2), glm::max(cubeCorner1, cubeCorner2), sphereCentre, sphereRadius);
    }
    
    // If the specified attribute exists within the element, assigns the provided
    // variable to it's float converted value.
    void changeFloatAttribIfExists(GLfloat & variable, tinyxml2::XMLElement * element, const char * attribName) {
        const char * attribStr = element->Attribute(attribName);
        if (attribStr != NULL) {
            variable = std::strtof(attribStr, NULL);
        }
    }
    
    GLmat4 Util::readTransformation(tinyxml2::XMLElement * element) {
        // If there is just a base transform element, convert it to a matrix and use it.
        const char * baseTransform = element->Attribute("baseTransform");
        if (baseTransform != NULL) {
            return stringToMat4(baseTransform);
        }
        
        // Otherwise, work out user provided scale, rotation, translation.
        GLvec3 scale(1.0f, 1.0f, 1.0f),
               rotation(0.0f, 0.0f, 0.0f),
               translation(0.0f, 0.0f, 0.0f);
        
        // Read scale, rotation, translation.
        changeFloatAttribIfExists(scale.x,       element, "sx");
        changeFloatAttribIfExists(scale.y,       element, "sy");
        changeFloatAttribIfExists(scale.z,       element, "sz");
        changeFloatAttribIfExists(rotation.x,    element, "rx");
        changeFloatAttribIfExists(rotation.y,    element, "ry");
        changeFloatAttribIfExists(rotation.z,    element, "rz");
        changeFloatAttribIfExists(translation.x, element, "x");
        changeFloatAttribIfExists(translation.y, element, "y");
        changeFloatAttribIfExists(translation.z, element, "z");
        
        // Convert angles from degress to radians.
        rotation.x = glm::radians(rotation.x);
        rotation.y = glm::radians(rotation.y);
        rotation.z = glm::radians(rotation.z);
        
        // Outer transformation done first (inner * outer)   (scale then rotate then translate).
        return glm::scale(
                   glm::rotate(
                       glm::rotate(
                           glm::rotate(
                               glm::translate(
                                   glm::mat4(1.0f),
                                   translation
                               ),
                               rotation.z,
                               GLvec3(0,0,1)
                           ),
                           rotation.y,
                           GLvec3(0,1,0)
                       ),
                       rotation.x,
                       GLvec3(1,0,0)
                   ),
                   scale
               );
    }
    
    GLmat4 Util::perspectiveX(GLfloat fovX, GLfloat viewWidth, GLfloat viewHeight, GLfloat nearClip, GLfloat farClip) {
        // See: https://community.khronos.org/t/horizontal-vertical-fov-glm-projection-matrix/73854
        //      http://web.archive.org/web/20200721122744/https://community.khronos.org/t/horizontal-vertical-fov-glm-projection-matrix/73854
        // We can work out perspective based on horizontal fov, simply by subtituting the fovy for
        // a horizontal fov, and giving an inverted aspect ratio (height/width instead of width/height).
        // Then to turn it the right way around, just swap over [0][0] and [1][1], and hey presto
        // we have perspective based on horizontal field of view.
        GLmat4 perspec = glm::perspective(fovX, viewHeight / viewWidth, nearClip, farClip);
        std::swap(perspec[0][0], perspec[1][1]);
        return perspec;
    }
    
    btVector3 Util::getInertia(const btCollisionShape & shape, GLfloat mass) {
        btVector3 inertia;
        shape.calculateLocalInertia(mass, inertia);
        return inertia;
    }
    
    btRigidBody * Util::getAndAddToPhysics(PhysicsWorld * physicsWorld, btRigidBody * rigidBody) {
        physicsWorld->getDynamicsWorld().addRigidBody(rigidBody);
        return rigidBody;
    }
    
    GLint Util::getUniformLocationChecked(GLuint program, const char * identifier, const char * name) {
        GLint location = glGetUniformLocation(program, name);
        if (location == -1) {
            std::cerr << "WARNING: " << identifier << ": Cannot find uniform \"" << name << "\" in shader program.\n";
        }
        return location;
    }
    
    GLuint Util::getUniformBlockIndexChecked(GLuint program, const char * identifier, const char * uniformBlockName) {
        GLuint index = glGetUniformBlockIndex(program, uniformBlockName);
        if (index == GL_INVALID_INDEX) {
            std::cerr << "WARNING: " << identifier << ": Cannot find uniform block index \"" << uniformBlockName << "\" in shader program.\n";
        }
        return index;
    }
    
    static void printDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message, const void * userParam) {
        fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
                (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""), type, severity, message);
    }
    
    void Util::enableOpenGLDebugMessages(void) {
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(&printDebugMessage, 0);
    }
    
    void Util::compileAndCheckShader(const std::string & name, GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << "INFO: Compile log from shader: " << name << '\n' << str;
        }
        
        // If it didn't compile, complain and exit.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void Util::linkAndCheckProgram(const std::string & name, GLuint program) {
        // Try to link the shader.
        glLinkProgram(program);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetProgramInfoLog(program, len, NULL, &str[0]);
            std::cerr << "INFO: Link log from shader program: " << name << '\n' << str;
        }
        
        // If it didn't link, complain and exit.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            std::cerr << "ERROR: Failed to link " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    GLuint Util::compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource) {
        // Create and compile vertex shader.
        GLuint vertexShader = 0, geometryShader = 0, fragmentShader = 0;
        
        // Create and compile vertex shader.
        if (vertexShaderSource != NULL) {
            vertexShader = glCreateShader(GL_VERTEX_SHADER);
            glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
            compileAndCheckShader(name+" vertex shader", vertexShader);
        }
        
        // Create and compile geometry shader.
        if (geometryShaderSource != NULL) {
            geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
            glShaderSource(geometryShader, 1, &geometryShaderSource, NULL);
            compileAndCheckShader(name+" geometry shader", geometryShader);
        }
        
        // Create and compile fragment shader.
        if (fragmentShaderSource != NULL) {
            fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
            compileAndCheckShader(name+" fragment shader", fragmentShader);
        }
        
        // Create shader program, attach all shaders and link it.
        GLuint prog = glCreateProgram();
        if (vertexShaderSource != NULL)   glAttachShader(prog, vertexShader);
        if (geometryShaderSource != NULL) glAttachShader(prog, geometryShader);
        if (fragmentShaderSource != NULL) glAttachShader(prog, fragmentShader);
        linkAndCheckProgram(name+" main shader program", prog);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        if (vertexShaderSource != NULL)   glDeleteShader(vertexShader);
        if (geometryShaderSource != NULL) glDeleteShader(geometryShader);
        if (fragmentShaderSource != NULL) glDeleteShader(fragmentShader);
        
        return prog;
    }
    
    static std::unordered_map<std::string, bool> boolMap = {
        {"true",  true},  {"yes", true},  {"1", true},
        {"false", false}, {"no",  false}, {"0", false}
    };
    
    bool Util::stringToBool(const char * str) {
        std::string lower = str;
        for (char & chr : lower) { chr = std::tolower(chr); }
        auto iter = boolMap.find(lower);
        if (iter == boolMap.end()) {
            return false;
        } else {
            return iter->second;
        }
    }
}
