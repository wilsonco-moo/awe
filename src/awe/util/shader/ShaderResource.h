/*
 * ShaderResource.h
 *
 *  Created on: 5 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_SHADER_SHADERRESOURCE_H_
#define AWE_UTIL_SHADER_SHADERRESOURCE_H_

#include <unordered_set>
#include <GL/gl.h>

namespace awe {

    /**
     * This can be used to encapsulate a resource provided to a shader,
     * such as a uniform buffer.
     * 
     * When shaders are registered to it, assuming they're compatible, the
     * resource is made available to the shader. If the shader is not
     * compatible, warnings should be printed.
     */
    class ShaderResource {
    private:
        std::unordered_set<GLuint> shaders;

    public:
        /**
         * The default constructor takes no parameters: instead shaders must be
         * registered using the registerShader method.
         */
        ShaderResource(void);
        virtual ~ShaderResource(void);
        
    protected:
        /**
         * This is called whenever a shader is registered, (but never more than
         * once for each shader).
         * 
         * Note that the shader is added to our internal unordered set of
         * shaders immediately BEFORE this operation.
         */
        virtual void onRegisterShader(GLuint shaderProgram) = 0;
        
        /**
         * This is called whenever a shader is unregistered, (but never more
         * than once for each shader).
         * 
         * Note that the shader is removed from our internal unordered set of
         * shaders immediately AFTER this operation.
         */
        virtual void onUnregisterShader(GLuint shaderProgram) = 0;
    
    public:
        /**
         * Allows access to our shaders.
         */
        inline const std::unordered_set<GLuint> & getShaders(void) const {
            return shaders;
        }
        
        /**
         * Registers the shader program. This should not be called more than
         * once for each shader program, and must be called from an OpenGL
         * context.
         * During this operation, subclasses may bind the program, but should
         * always leave it unbound.
         */
        void registerShader(GLuint shaderProgram);
        
        /**
         * Unregisters the shader program. This should be called for each shader
         * program registered, before this instance is destroyed, from an
         * OpenGL context.
         */
        void unregisterShader(GLuint shaderProgram);
    };
}

#endif
