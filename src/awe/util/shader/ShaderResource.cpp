/*
 * ShaderResource.cpp
 *
 *  Created on: 5 Aug 2020
 *      Author: wilson
 */

#include "ShaderResource.h"

#include <iostream>
#include <cstddef>

namespace awe {

    ShaderResource::ShaderResource(void) :
        shaders() {
    }
    
    ShaderResource::~ShaderResource(void) {
        if (!shaders.empty()) {
            std::cerr << "WARNING: ShaderResource: " << shaders.size() << " shaders are still registered upon destruction (";
            size_t i = 0;
            for (GLuint shader : shaders) {
                if ((i++) != 0) std::cerr << ", ";
                std::cerr << shader;
            }
            std::cerr << ").\n";
        }
    }
    
    void ShaderResource::registerShader(GLuint shaderProgram) {
        auto iter = shaders.find(shaderProgram);
        if (iter == shaders.end()) {
            shaders.insert(shaderProgram);
            onRegisterShader(shaderProgram);
        } else {
            std::cerr << "WARNING: ShaderResource: " << "shader registered more than once (" << shaderProgram << ").\n";
        }
    }
    
    void ShaderResource::unregisterShader(GLuint shaderProgram) {
        auto iter = shaders.find(shaderProgram);
        if (iter == shaders.end()) {
            std::cerr << "WARNING: ShaderResource: " << "shader unregistered when it didn't already exist (" << shaderProgram << ").\n";
        } else {
            shaders.erase(iter);
            onUnregisterShader(shaderProgram);
        }
    }
}
