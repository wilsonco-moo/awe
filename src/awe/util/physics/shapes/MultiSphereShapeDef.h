/*
 * MultiSphereShapeDef.h
 *
 *  Created on: 3 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_SHAPES_MULTISPHERESHAPEDEF_H_
#define AWE_UTIL_PHYSICS_SHAPES_MULTISPHERESHAPEDEF_H_

#include <GL/gl.h>
#include <vector>

#include "../../types/Types.h"
#include "ShapeDef.h"

namespace awe {

    /**
     * This class defines a collision shape made from the convex hull formed
     * by a set of spheres.
     * 
     * Multi sphere collision shapes are defined in the XML file as follows:
     * 
     * <collisionMultiSphere mass="...">
     *     <sphere x="..." y="..." z="..." radius="..."/>
     *     <sphere x="..." y="..." z="..." radius="..."/>
     *     <sphere x="..." y="..." z="..." radius="..."/>
     *     ...
     *     <transform .../>
     * </collisionMultiSphere>
     */
    class MultiSphereShapeDef : public ShapeDef {
    private:
        class SphereDef {
        public:
            GLvec3 position;
            GLfloat radius;
        };
        
        std::vector<SphereDef> spheres;

    public:
        MultiSphereShapeDef(tinyxml2::XMLElement * element, bool & errorFlag);
        virtual ~MultiSphereShapeDef(void);
        
        virtual btCollisionShape * createBulletCollisionShape(const GLvec3 & scale) const override;
    };
}

#endif
