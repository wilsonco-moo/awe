/*
 * BoxShapeDef.h
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_SHAPES_BOXSHAPEDEF_H_
#define AWE_UTIL_PHYSICS_SHAPES_BOXSHAPEDEF_H_

#include "ShapeDef.h"

namespace awe {

    /**
     * This class defines a box collision shape.
     * 
     * Box collision shapes are defined in the XML file as follows:
     * 
     * <collisionBox mass="...">
     *      <halfExtent xSize="..." ySize="..." zSize="..."/>
     *      <transform .../>
     * </collisionBox>
     */
    class BoxShapeDef : public ShapeDef {
    private:
        GLvec3 collisionHalfExtents;

    public:
        BoxShapeDef(tinyxml2::XMLElement * element, bool & errorFlag);
        virtual ~BoxShapeDef(void);
        
        virtual btCollisionShape * createBulletCollisionShape(const GLvec3 & scale) const override;
    };
}

#endif
