/*
 * MultiSphereShapeDef.cpp
 *
 *  Created on: 3 Sep 2020
 *      Author: wilson
 */

#include "MultiSphereShapeDef.h"

#include <bullet3/BulletCollision/CollisionShapes/btMultiSphereShape.h>
#include <bullet3/LinearMath/btVector3.h>
#include <bullet3/LinearMath/btScalar.h>
#include <tinyxml2/tinyxml2.h>
#include <iostream>

#include "../../types/ConversionUtil.h"
#include "../../Util.h"

namespace awe {

    // When any XML errors happen, print a complaint, set error flag and return.
    #define PRINT_XML_ERROR(xmlErr)                                                                  \
        std::cerr << "WARNING: MultiSphereShapeDef: " << "Error in XML file: " << xmlErr << '\n';    \
        errorFlag = true;                                                                            \
        return;

    MultiSphereShapeDef::MultiSphereShapeDef(tinyxml2::XMLElement * element, bool & errorFlag) :
        ShapeDef(element, errorFlag),
        spheres() {
        
        ELEM_CHILDREN_FOR_EACH(sphereElem, element, "sphere") {
            GET_ATTRIB_GLFLOAT(x,      sphereElem, "x")
            GET_ATTRIB_GLFLOAT(y,      sphereElem, "y")
            GET_ATTRIB_GLFLOAT(z,      sphereElem, "z")
            GET_ATTRIB_GLFLOAT(radius, sphereElem, "radius")
            spheres.push_back({GLvec3(x, y, z), radius});
        }
        
        if (spheres.size() == 0) {
            PRINT_XML_ERROR("Multi sphere shape must have at least one sphere.")
        }
    }
    
    MultiSphereShapeDef::~MultiSphereShapeDef(void) {
    }
    
    btCollisionShape * MultiSphereShapeDef::createBulletCollisionShape(const GLvec3 & scale) const {
        GLfloat avgScale = (scale.x + scale.y + scale.z) / 3.0f;
        std::vector<btVector3> positions;
        std::vector<btScalar> radi;
        positions.reserve(spheres.size());
        radi.reserve(spheres.size());
        for (const SphereDef & sphere : spheres) {
            // Multiply positions by the scale, multiply radiuses by average scale.
            positions.push_back(ConversionUtil::glmToBulletVec3(sphere.position * scale));
            radi.push_back(sphere.radius * avgScale);
        }
        // It is safe to provide btMultiSphereShape with temporary allocations, since btMultiSphereShape
        // internally copies the data.
        return new btMultiSphereShape(&positions[0], &radi[0], spheres.size());
    }
}
