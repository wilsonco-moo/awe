/*
 * CapsuleShapeDef.h
 *
 *  Created on: 1 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_SHAPES_CAPSULESHAPEDEF_H_
#define AWE_UTIL_PHYSICS_SHAPES_CAPSULESHAPEDEF_H_

#include "ShapeDef.h"

namespace awe {

    /**
     * This class defines a capsule collision shape, aligned around the axis
     * specified with a ShapeDirection.
     * 
     * Capsule collision shapes are defined in the XML file as follows:
     * (Note that there are three variants: collisionCapsuleX, collisionCapsuleY, collisionCapsuleZ).
     * 
     * <collisionCapsuleX mass="...">
     *      <properties radius="..." length="..."/>
     *      <transform .../>
     * </collisionCapsuleX>
     */
    class CapsuleShapeDef : public ShapeDef {
    private:
        GLfloat radius, length;
        ShapeDirection direction;

    public:
        CapsuleShapeDef(tinyxml2::XMLElement * element, bool & errorFlag, ShapeDirection direction);
        virtual ~CapsuleShapeDef(void);
        
        virtual btCollisionShape * createBulletCollisionShape(const GLvec3 & scale) const override;
    };
}

#endif
