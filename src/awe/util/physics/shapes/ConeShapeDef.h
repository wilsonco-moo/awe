/*
 * ConeShapeDef.h
 *
 *  Created on: 4 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_SHAPES_CONESHAPEDEF_H_
#define AWE_UTIL_PHYSICS_SHAPES_CONESHAPEDEF_H_

#include "ShapeDef.h"

namespace awe {

    /**
     * This class defines a cone collision shape, aligned around the axis
     * specified with a ShapeDirection.
     * 
     * Cone collision shapes are defined in the XML file as follows:
     * (Note that there are three variants: collisionConeX, collisionConeY, collisionConeZ).
     * 
     * <collisionConeX mass="...">
     *      <properties radius="..." length="..."/>
     *      <transform .../>
     * </collisionConeX>
     */
    class ConeShapeDef : public ShapeDef {
    private:
        GLfloat radius, length;
        ShapeDirection direction;

    public:
        ConeShapeDef(tinyxml2::XMLElement * element, bool & errorFlag, ShapeDirection direction);
        virtual ~ConeShapeDef(void);
        
        virtual btCollisionShape * createBulletCollisionShape(const GLvec3 & scale) const override;
    };
}

#endif
