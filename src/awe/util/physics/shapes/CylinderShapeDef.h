/*
 * CylinderShapeDef.h
 *
 *  Created on: 4 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_SHAPES_CYLINDERSHAPEDEF_H_
#define AWE_UTIL_PHYSICS_SHAPES_CYLINDERSHAPEDEF_H_

#include "ShapeDef.h"

namespace awe {

    /**
     * This class defines a cylinder collision shape, aligned around the axis
     * specified with a ShapeDirection.
     * 
     * Cylinder collision shapes are defined in the XML file as follows:
     * (Note that there are three variants: collisionCylinderX, collisionCylinderY, collisionCylinderZ).
     * 
     * <collisionCylinderX mass="...">
     *      <properties radius="..." length="..."/>
     *      <transform .../>
     * </collisionCylinderX>
     */
    class CylinderShapeDef : public ShapeDef {
    private:
        GLfloat radius, halfLength;
        ShapeDirection direction;

    public:
        CylinderShapeDef(tinyxml2::XMLElement * element, bool & errorFlag, ShapeDirection direction);
        virtual ~CylinderShapeDef(void);
        
        virtual btCollisionShape * createBulletCollisionShape(const GLvec3 & scale) const override;
    };
}

#endif
