/*
 * ShapeDef.h
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_SHAPES_SHAPEDEF_H_
#define AWE_UTIL_PHYSICS_SHAPES_SHAPEDEF_H_

#include <GL/gl.h>

#include "../../types/Types.h"

class btCollisionShape;
class btTransform;

namespace tinyxml2 {
    class XMLElement;
}

namespace awe {
    
    /**
     * This enum defines axis aligned direction, used in some collision shapes.
     */
    enum class ShapeDirection { x, y, z };
    
    /**
     * This defines the base type of collision shapes. All collision shapes,
     * regardless of type, have an associated position, orientation and mass.
     * 
     * The provided error flag is set true if information is missing from the
     * XML file.
     */
    class ShapeDef {
    private:
        GLvec3 position;
        GLquat orientation;
        GLfloat mass;

    public:
        ShapeDef(tinyxml2::XMLElement * element, bool & errorFlag);
        virtual ~ShapeDef(void);
    
    public:
        /**
         * Creates a new shape from the XML element, or NULL if this element
         * does not correspond to a collision shape. If the element is invalid,
         * the error flag is set and NULL is returned.
         * 
         * Note: If the error flag is set to true, this method ONLY EVER returns NULL.
         */
        static ShapeDef * createShape(tinyxml2::XMLElement * element, bool & errorFlag);
        
        /**
         * Implementing shapes should use this to create an (appropriately scaled)
         * bullet physics collision shape.
         */
        virtual btCollisionShape * createBulletCollisionShape(const GLvec3 & scale) const = 0;
        
        /**
         * Allows access to our mass.
         */
        inline GLfloat getMass(void) const {
            return mass;
        }
        
        /**
         * Converts our position and orientation directly to a GLM transformation
         * matrix, and returns it. This is helpful for manually working with our
         * transformation.
         */
        GLmat4 getCombinedTransform(void) const;
        
        /**
         * Resets our position to zero, and resets our orientation to identity.
         * This is used by AssetCollisionShapeDef when baking our position and
         * orientation into the physics transform.
         */
        void resetToIdentityTransform(void);
        
        /**
         * Gets the transform which should be applied to this shape, given the
         * provided scale.
         */
        void getTransform(btTransform & transform, const GLvec3 & scale) const;
    };
}

#endif
