/*
 * SphereShapeDef.cpp
 *
 *  Created on: 1 Sep 2020
 *      Author: wilson
 */

#include "SphereShapeDef.h"

#include <bullet3/BulletCollision/CollisionShapes/btSphereShape.h>
#include <tinyxml2/tinyxml2.h>
#include <iostream>

#include "../../Util.h"

namespace awe {

    // When any XML errors happen, print a complaint, set error flag and return.
    #define PRINT_XML_ERROR(xmlErr)                                                       \
        std::cerr << "WARNING: SphereShapeDef: Error in XML file: " << xmlErr << '\n';    \
        errorFlag = true;                                                                 \
        return;

    SphereShapeDef::SphereShapeDef(tinyxml2::XMLElement * element, bool & errorFlag) :
        ShapeDef(element, errorFlag) {
        
        // Find extents.
        GET_ELEM(propertiesElem, element, "properties")
        GET_ATTRIB_GLFLOAT(radiusValue, propertiesElem, "radius")
        radius = radiusValue;
    }
    
    SphereShapeDef::~SphereShapeDef(void) {
    }
    
    btCollisionShape * SphereShapeDef::createBulletCollisionShape(const GLvec3 & scale) const {
        // Since spheres only support uniform scale, work out closest scale as
        // average of x,y,z scales.
        return new btSphereShape(radius * (scale.x + scale.y + scale.z) / 3.0f);
    }
}
