/*
 * BoxShapeDef.cpp
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#include "BoxShapeDef.h"

#include <bullet3/BulletCollision/CollisionShapes/btBoxShape.h>
#include <tinyxml2/tinyxml2.h>
#include <iostream>

#include "../../types/ConversionUtil.h"
#include "../../Util.h"

namespace awe {

    // When any XML errors happen, print a complaint, set error flag and return.
    #define PRINT_XML_ERROR(xmlErr)                                                    \
        std::cerr << "WARNING: BoxShapeDef: Error in XML file: " << xmlErr << '\n';    \
        errorFlag = true;                                                              \
        return;

    BoxShapeDef::BoxShapeDef(tinyxml2::XMLElement * element, bool & errorFlag) :
        ShapeDef(element, errorFlag) {
        
        // Find extents.
        GET_ELEM(halfExtentElem, element, "halfExtent")
        GET_ATTRIB_GLFLOAT(xSize, halfExtentElem, "xSize")
        GET_ATTRIB_GLFLOAT(ySize, halfExtentElem, "ySize")
        GET_ATTRIB_GLFLOAT(zSize, halfExtentElem, "zSize")
        collisionHalfExtents = GLvec3(xSize, ySize, zSize);
    }
    
    BoxShapeDef::~BoxShapeDef(void) {
    }
    
    btCollisionShape * BoxShapeDef::createBulletCollisionShape(const GLvec3 & scale) const {
        return new btBoxShape(ConversionUtil::glmToBulletVec3(collisionHalfExtents * scale));
    }
}
