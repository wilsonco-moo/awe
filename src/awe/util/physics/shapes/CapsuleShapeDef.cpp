/*
 * CapsuleShapeDef.cpp
 *
 *  Created on: 1 Sep 2020
 *      Author: wilson
 */

#include "CapsuleShapeDef.h"

#include <bullet3/BulletCollision/CollisionShapes/btCapsuleShape.h>
#include <tinyxml2/tinyxml2.h>
#include <iostream>

#include "../../Util.h"

namespace awe {

    // When any XML errors happen, print a complaint, set error flag and return.
    #define PRINT_XML_ERROR(xmlErr)                                                        \
        std::cerr << "WARNING: CapsuleShapeDef: Error in XML file: " << xmlErr << '\n';    \
        errorFlag = true;                                                                  \
        return;
    
    
    // =========================================================================
    
    CapsuleShapeDef::CapsuleShapeDef(tinyxml2::XMLElement * element, bool & errorFlag, ShapeDirection direction) :
        ShapeDef(element, errorFlag),
        direction(direction) {
        
        // Find extents.
        GET_ELEM(propertiesElem, element, "properties")
        GET_ATTRIB_GLFLOAT(radiusValue, propertiesElem, "radius")
        GET_ATTRIB_GLFLOAT(lengthValue, propertiesElem, "length")
        radius = radiusValue;
        length = lengthValue;
    }
    
    CapsuleShapeDef::~CapsuleShapeDef(void) {
    }
    
    btCollisionShape * CapsuleShapeDef::createBulletCollisionShape(const GLvec3 & scale) const {
        switch(direction) {
        case ShapeDirection::x:
            // X scale for capsule length, average of other two for radius.
            return new btCapsuleShapeX(radius * (scale.y + scale.z) / 2.0f, length * scale.x);
        
        case ShapeDirection::y:
            // Y scale for capsule length, average of other two for radius.
            // Note that default axis for btCapsuleShape is Y axis. There is no "btCapsuleShapeY".
            return new btCapsuleShape(radius * (scale.x + scale.z) / 2.0f, length * scale.y);
        
        default: // z
            // Z scale for capsule length, average of other two for radius.
            return new btCapsuleShapeZ(radius * (scale.x + scale.y) / 2.0f, length * scale.z);
        }
    }
}
