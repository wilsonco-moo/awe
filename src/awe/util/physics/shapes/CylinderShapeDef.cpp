/*
 * CylinderShapeDef.cpp
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#include "CylinderShapeDef.h"

#include <bullet3/BulletCollision/CollisionShapes/btCylinderShape.h>
#include <bullet3/LinearMath/btVector3.h>
#include <tinyxml2/tinyxml2.h>
#include <iostream>

#include "../../types/ConversionUtil.h"
#include "../../Util.h"

namespace awe {

    // When any XML errors happen, print a complaint, set error flag and return.
    #define PRINT_XML_ERROR(xmlErr)                                                         \
        std::cerr << "WARNING: CylinderShapeDef: Error in XML file: " << xmlErr << '\n';    \
        errorFlag = true;                                                                   \
        return;

    CylinderShapeDef::CylinderShapeDef(tinyxml2::XMLElement * element, bool & errorFlag, ShapeDirection direction) :
        ShapeDef(element, errorFlag),
        direction(direction) {
        
        // Find extents.
        GET_ELEM(propertiesElem, element, "properties")
        GET_ATTRIB_GLFLOAT(radiusValue, propertiesElem, "radius")
        GET_ATTRIB_GLFLOAT(lengthValue, propertiesElem, "length")
        radius = radiusValue;
        halfLength = lengthValue * 0.5f;
    }
    
    CylinderShapeDef::~CylinderShapeDef(void) {
    }
    
    btCollisionShape * CylinderShapeDef::createBulletCollisionShape(const GLvec3 & scale) const {
        // Note: In the XML file we use radius/length to be consistent with other collision
        //       shapes (also it makes more sense). Bullet requires cylinders to be specified
        //       as half extent bounding boxes, even though bullet doesn't support non-uniform
        //       scale anyway. So convert radius/length to bounding boxes, scaling on appropriate axes.
        
        switch(direction) {
            case ShapeDirection::x: {
                // X scale for cylinder (half) length, average of other two for radius.
                GLfloat scaledRadius  = radius * (scale.y + scale.z) / 2.0f,
                        scaledHalfLen = halfLength * scale.x;
                return new btCylinderShapeX(btVector3(scaledHalfLen, scaledRadius, scaledRadius));
            }
            
            case ShapeDirection::y: {
                // Y scale for cylinder (half) length, average of other two for radius.
                // Note that default axis for btConeShape is Y axis. There is no "btConeShapeY".
                GLfloat scaledRadius  = radius * (scale.x + scale.z) / 2.0f,
                        scaledHalfLen = halfLength * scale.y;
                return new btCylinderShape(btVector3(scaledRadius, scaledHalfLen, scaledRadius));
            }
            
            default: { // z
                // Z scale for cylinder (half) length, average of other two for radius.
                GLfloat scaledRadius  = radius * (scale.x + scale.y) / 2.0f,
                        scaledHalfLen = halfLength * scale.z;
                return new btCylinderShapeZ(btVector3(scaledRadius, scaledRadius, scaledHalfLen));
            }
        }
    }
}
