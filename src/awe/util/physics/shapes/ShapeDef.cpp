/*
 * ShapeDef.cpp
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#include "ShapeDef.h"

#include <bullet3/LinearMath/btTransform.h>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <tinyxml2/tinyxml2.h>
#include <unordered_map>
#include <functional>
#include <iostream>
#include <cstddef>
#include <string>

#include "../../types/ConversionUtil.h"
#include "MultiSphereShapeDef.h"
#include "CylinderShapeDef.h"
#include "CapsuleShapeDef.h"
#include "SphereShapeDef.h"
#include "ConeShapeDef.h"
#include "BoxShapeDef.h"
#include "../../Util.h"

namespace awe {

    // When any XML errors happen, print a complaint, set error flag and return.
    #define PRINT_XML_ERROR(xmlErr)                                                       \
        std::cerr << "WARNING: ShapeDef: " << "Error in XML file: " << xmlErr << '\n';    \
        errorFlag = true;                                                                 \
        return;

    ShapeDef::ShapeDef(tinyxml2::XMLElement * element, bool & errorFlag) {
        
        // Read and set mass.
        GET_ATTRIB_GLFLOAT(massAttr, element, "mass")
        mass = massAttr;
        
        // Get our transformation (scale, orientation, position).
        // Use decompose to get our position and orientation. Ignore scale, skew, perspective.
        GET_ELEM(transformElem, element, "transform")
        if (transformElem != NULL) {
            GLmat4 transformation = Util::readTransformation(transformElem);
            GLvec3 scale, skew; GLvec4 perspective;
            glm::decompose(transformation, scale, orientation, position, skew, perspective);
        }
    }
    
    ShapeDef::~ShapeDef(void) {
    }
    
    // For creating each subclass.
    static std::unordered_map<std::string, std::function<ShapeDef *(tinyxml2::XMLElement *, bool &)>> shapeCreateMap = {
        {"collisionBox", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new BoxShapeDef(element, errorFlag);
        }},
        {"collisionSphere", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new SphereShapeDef(element, errorFlag);
        }},
        {"collisionCapsuleX", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new CapsuleShapeDef(element, errorFlag, ShapeDirection::x);
        }},
        {"collisionCapsuleY", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new CapsuleShapeDef(element, errorFlag, ShapeDirection::y);
        }},
        {"collisionCapsuleZ", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new CapsuleShapeDef(element, errorFlag, ShapeDirection::z);
        }},
        {"collisionMultiSphere", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new MultiSphereShapeDef(element, errorFlag);
        }},
        {"collisionCylinderX", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new CylinderShapeDef(element, errorFlag, ShapeDirection::x);
        }},
        {"collisionCylinderY", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new CylinderShapeDef(element, errorFlag, ShapeDirection::y);
        }},
        {"collisionCylinderZ", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new CylinderShapeDef(element, errorFlag, ShapeDirection::z);
        }},
        {"collisionConeX", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new ConeShapeDef(element, errorFlag, ShapeDirection::x);
        }},
        {"collisionConeY", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new ConeShapeDef(element, errorFlag, ShapeDirection::y);
        }},
        {"collisionConeZ", [](tinyxml2::XMLElement * element, bool & errorFlag) {
            return new ConeShapeDef(element, errorFlag, ShapeDirection::z);
        }}
    };
    
    ShapeDef * ShapeDef::createShape(tinyxml2::XMLElement * element, bool & errorFlag) {
        // Look up shape type.
        std::string elementName = element->Name();
        auto iter = shapeCreateMap.find(elementName);
        
        // If not found, set error flag if this is not a transform node, return nothing.
        if (iter == shapeCreateMap.end()) {
            if (elementName != "physicsTransform") {
                std::cerr << "WARNING: ShapeDef: " << "Unknown collision shape type: \"" << elementName << "\".\n";
                errorFlag = true;
            }
            return NULL;
        
        // Otherwise create using the function from the map. If the new shape reports
        // an error, delete it and return NULL. Otherwise just return the shape.
        } else {
            ShapeDef * shape = iter->second(element, errorFlag);
            if (errorFlag) {
                delete shape;
                return NULL;
            }
            return shape;
        }
    }
    
    GLmat4 ShapeDef::getCombinedTransform(void) const {
        // Rotate then translate.
        return glm::translate(GLmat4(1.0f), position) * glm::mat4_cast(orientation);
    }
    
    void ShapeDef::resetToIdentityTransform(void) {
        position = GLvec3(0.0f, 0.0f, 0.0f);
        orientation = glm::identity<GLquat>();
    }
    
    void ShapeDef::getTransform(btTransform & transform, const GLvec3 & scale) const {
        transform = btTransform(
            ConversionUtil::glmToBulletQuat(orientation),
            ConversionUtil::glmToBulletVec3(position * scale)
        );
    }
}
