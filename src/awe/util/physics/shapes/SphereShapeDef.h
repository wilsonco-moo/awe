/*
 * SphereShapeDef.h
 *
 *  Created on: 1 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_SHAPES_SPHERESHAPEDEF_H_
#define AWE_UTIL_PHYSICS_SHAPES_SPHERESHAPEDEF_H_

#include "ShapeDef.h"

namespace awe {

    /**
     * This class defines a sphere collision shape.
     * 
     * Sphere collision shapes are defined in the XML file as follows:
     * 
     * <collisionSphere mass="...">
     *      <properties radius="..."/>
     *      <transform .../>
     * </collisionSphere>
     */
    class SphereShapeDef : public ShapeDef {
    private:
        GLfloat radius;

    public:
        SphereShapeDef(tinyxml2::XMLElement * element, bool & errorFlag);
        virtual ~SphereShapeDef(void);
        
        virtual btCollisionShape * createBulletCollisionShape(const GLvec3 & scale) const override;
    };
}

#endif
