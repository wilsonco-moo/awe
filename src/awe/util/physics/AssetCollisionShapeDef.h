/*
 * AssetCollisionShapeDef.h
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_ASSETCOLLISIONSHAPEDEF_H_
#define AWE_UTIL_PHYSICS_ASSETCOLLISIONSHAPEDEF_H_

#include <vector>

#include "../types/Types.h"

namespace tinyxml2 {
    class XMLElement;
}

namespace awe {
    class ShapeDef;

    /**
     * Represents the collision information about an asset.
     * An instance of this, along with a scale, can be used to create an
     * InstanceCollisionShapeDef.
     * 
     * AssetCollisionShapeDef contains whether the collision is static, and
     * whether a compound collision shape is required. It also stores a vector
     * of collision shapes, as ShapeDef instances. Each ShapeDef has its own
     * in-built position and orientation. We also hold "physicsTransform", which
     * is the transformation of the centre of mass (origin) of the collision
     * shapes, relative to an instance using it.
     * 
     * NOTE: If we only have a single collision shape (ShapeDef), and the
     *       use of a compound shape is not forced with requiresCompoundShape,
     *       then its position and orientation will ALWAYS be set to zero
     *       (identity). Instead, its transformation is baked into
     *       "physicsTransform". This means that users of this class, when there
     *       is only one collision shape, do not have to worry about its
     *       transformation: "physicsTransform" can be used instead.
     * 
     * The provided error flag is set true if information is missing from the
     * XML file.
     * 
     * Note: Do not make subclasses.
     */
    class AssetCollisionShapeDef {
    private:
        // Whether the collision is static, and whether a compound shape is required.
        bool isStatic, requiresCompoundShape;
        
        // The transformation, relative to the instance, of it's collision shape.
        // This is optionally specified in the XML file. Where we only have a single
        // collision shape, this is combined with the transformation for the single
        // collision shape.
        GLmat4 physicsTransform;
        
        // Our vector of shapes, loaded from the XML file.
        std::vector<ShapeDef *> shapes;
        
        // Don't allow copying: we hold raw resources.
        AssetCollisionShapeDef(const AssetCollisionShapeDef & other);
        AssetCollisionShapeDef & operator = (const AssetCollisionShapeDef & other);
        
    public:
        AssetCollisionShapeDef(tinyxml2::XMLElement * element, bool & errorFlag);
        ~AssetCollisionShapeDef(void);
        
        /**
         * Allows (const) access to our vector of ShapeDef shapes. If we were
         * created without error, there will always be at least one shape here.
         */
        inline const std::vector<ShapeDef *> & getShapes(void) const {
            return shapes;
        }
        
        /**
         * Returns true if we are required to use static collision.
         */
        inline bool isCollisionStatic(void) const {
            return isStatic;
        }
        
        /**
         * Returns true if a compound shape is required for this asset.
         * This is always true when we have more than one collision shape.
         * Optionally, users can force this to be true (in the XML file), when
         * we only have a single shape.
         */
        inline bool doesRequireCompoundShape(void) const {
            return requiresCompoundShape;
        }
        
        /**
         * Allows access to the physics transform. This is the transformation
         * (relative to the instance) of the origin of the collision shape.
         */
        inline const GLmat4 & getPhysicsTransform(void) const {
            return physicsTransform;
        }
    };
}

#endif
