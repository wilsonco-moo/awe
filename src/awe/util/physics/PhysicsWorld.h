/*
 * PhysicsWorld.h
 *
 *  Created on: 28 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_PHYSICSWORLD_H_
#define AWE_UTIL_PHYSICS_PHYSICSWORLD_H_

#pragma GCC diagnostic push // Ignore some warnings from bullet physics library headers.
#pragma GCC diagnostic ignored "-Wreorder"
#include <bullet3/BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h>
#include <bullet3/BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.h>
#include <bullet3/BulletCollision/CollisionDispatch/btCollisionDispatcher.h>
#include <bullet3/BulletCollision/BroadphaseCollision/btDbvtBroadphase.h>
#include <bullet3/BulletCollision/CollisionDispatch/btCollisionWorld.h>
#include <bullet3/BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include <bullet3/LinearMath/btVector3.h>
#pragma GCC diagnostic pop

#include <unordered_map>

#include "../types/Types.h"

namespace awe {
    class Instance;

    /**
     *
     */
    class PhysicsWorld {
    public:
        // ===================== Custom instance raycast class =================
    
        /**
         * This class allows doing raycasting through a collision world, but
         * ignoring (and passing through) all collision objects which do not
         * have associated instances. This is used within raycastSingleInstance,
         * although can be used outside of that.
         * 
         * Note that getHitPointWorld and getFoundInstance are ONLY defined if
         * our hasHit method returns true.
         */
        class InstanceRayCallback : public btCollisionWorld::RayResultCallback {
        private:
            const PhysicsWorld * physicsWorld;
            btVector3 rayFromWorld,  // Used to calculate hitPointWorld from hitFraction.
                      rayToWorld,
                      hitPointWorld;
            Instance * foundInstance;

        public:
            InstanceRayCallback(const btVector3 & rayFromWorld, const btVector3 & rayToWorld, const PhysicsWorld * physicsWorld);
            virtual ~InstanceRayCallback(void);
            virtual btScalar addSingleResult(btCollisionWorld::LocalRayResult & rayResult, bool normalInWorldSpace) override;
            inline const btVector3 & getHitPointWorld(void) const { return hitPointWorld; }
            inline Instance * getFoundInstance(void) const { return foundInstance; }
        };

        // =====================================================================

    private:
        std::unordered_map<const btRigidBody *, Instance *> rigidBodyToInstance;
        btDefaultCollisionConfiguration collisionConfig;
        btCollisionDispatcher collisionDispatcher;
        btDbvtBroadphase pairCache;
        btSequentialImpulseConstraintSolver solver;
        btDiscreteDynamicsWorld dynamicsWorld;
        GLfloat fixedTimeStep;
        int maxSubSteps;

        // Don't allow copying - that would break pointers to us.
        PhysicsWorld(const PhysicsWorld & other);
        PhysicsWorld & operator = (const PhysicsWorld & other);

    public:
        PhysicsWorld(const GLvec3 & gravity = GLvec3(0.0f, 0.0f, -9.81f), GLfloat fixedTimeStep = (1.0f / 60.0f), int maxSubSteps = 3);
        virtual ~PhysicsWorld(void);
        
        /**
         * This moves the physics simulation forward by the specified amount
         * of elapsed time. Internally, bullet physics will move the simulation
         * forward either nothing, or a maximum of maxSubSteps increments
         * of fixedTimeStep. Bullet physics works out all the interpolation
         * for you.
         */
        void step(GLfloat elapsed);
        
        /**
         * Adds the rigid body to the dynamics world, and registers an instance
         * to the rigid body. This allows instance driven ray casting, amongst
         * other things.
         */
        void registerRigidBody(btRigidBody * rigidBody, Instance * instance);
        
        /**
         * Removes the rigid body from the dynamics world, and unregisters the
         * instance associated with the specified rigid body.
         */
        void unregisterRigidBody(btRigidBody * rigidBody);
        
        // =========================== Raycasting ==============================
        
        /**
         * An easy way to perform raycasting.
         * Returns ONLY the first collision body that has been hit by the ray,
         * fired from startPoint.
         * NULL is returned if none is found.
         * If the ray does hit, hitPoint is set to the point at which the ray
         * hits the surface.
         * 
         * NOTE: The statement below was true with react physics, not sure about
         *       bullet physics.
         *       This is safe to use even when using collision-based movement.
         *       If a ray starts INSIDE a collision body, then that collision body seems to be
         *       ignored by reactphysics3d. That means the player's collision body won't be returned,
         *       as long as the ray is cast from inside it.
         */
        const btCollisionObject * raycastSingle(GLvec3 & hitPoint, const GLvec3 & startPoint, GLfloat hDir, GLfloat vDir, GLfloat length) const;
        const btCollisionObject * raycastSingle(GLvec3 & hitPoint, const GLvec3 & startPoint, const GLvec3 & endPoint) const;
        
        /**
         * These are the same as rayCastSingle, but convert the result to an instance,
         * for convenience. Any RigidBody which does not have an associated instance
         * is ignored, (so the ray passes straight through it).
         */
        Instance * raycastSingleInstance(GLvec3 & hitPoint, const GLvec3 & startPoint, GLfloat hDir, GLfloat vDir, GLfloat length) const;
        Instance * raycastSingleInstance(GLvec3 & hitPoint, const GLvec3 & startPoint, const GLvec3 & endPoint) const;
        
        // =========================== Accessor methods ========================
        
        /**
         * Accesses the instance associated with the specified rigid body.
         * This works for static AND dynamic rigid bodies.
         * If no instance can be found, this returns NULL.
         */
        Instance * getInstanceFromRigidBody(const btRigidBody * rigidBody) const;
        
        /**
         * Allows access to our internal dynamics world.
         */
        inline btDiscreteDynamicsWorld & getDynamicsWorld(void) {
            return dynamicsWorld;
        }
    };
}

#endif
