/*
 * PhysicsWorld.cpp
 *
 *  Created on: 28 Jan 2020
 *      Author: wilson
 */

#include "PhysicsWorld.h"

#include <iostream>
#include <cstddef>
#include <cmath>

#include "../types/ConversionUtil.h"

namespace awe {

    // =================== Internal class for raycasting =======================
    
    PhysicsWorld::InstanceRayCallback::InstanceRayCallback(const btVector3 & rayFromWorld, const btVector3 & rayToWorld, const PhysicsWorld * physicsWorld) :
        btCollisionWorld::RayResultCallback(),
        physicsWorld(physicsWorld),
        rayFromWorld(rayFromWorld),
        rayToWorld(rayToWorld) {
    }
    
    PhysicsWorld::InstanceRayCallback::~InstanceRayCallback(void) {
    }
    
    btScalar PhysicsWorld::InstanceRayCallback::addSingleResult(btCollisionWorld::LocalRayResult & rayResult, bool normalInWorldSpace) {
        
        // Find the instance related to the ray result's collision object. If one cannot be found,
        // return 1 (presumably, like ReactPhysics3d, this return value is a maximum fraction,
        // although bullet physics doesn't say this anywhere. Returning one means to continue doing raycasting).
        auto iter = physicsWorld->rigidBodyToInstance.find((const btRigidBody *)rayResult.m_collisionObject);
        if (iter == physicsWorld->rigidBodyToInstance.end()) {
            return 1.0f;
        }
        
        // Record closest hit fraction, collision object and instance.
        m_closestHitFraction = rayResult.m_hitFraction;
        m_collisionObject = rayResult.m_collisionObject;
        foundInstance = iter->second;
        
        // Hitpoint is interpolation of hit fraction, between start and end points.
        hitPointWorld.setInterpolate3(rayFromWorld, rayToWorld, rayResult.m_hitFraction);
        
        // Return hit fraction (this means stop doing the raycasting, since setting the max fraction to
        // the current fraction means go no further with the raycasting).
        return rayResult.m_hitFraction;
    }
    
    // =========================================================================


    PhysicsWorld::PhysicsWorld(const GLvec3 & gravity, GLfloat fixedTimeStep, int maxSubSteps) :
        rigidBodyToInstance(),
        collisionConfig(),
        collisionDispatcher(&collisionConfig),
        pairCache(),
        solver(),
        dynamicsWorld(&collisionDispatcher, &pairCache, &solver, &collisionConfig),
        fixedTimeStep(fixedTimeStep),
        maxSubSteps(maxSubSteps) {
        
        dynamicsWorld.setGravity(ConversionUtil::glmToBulletVec3(gravity));
    }

    PhysicsWorld::~PhysicsWorld(void) {
    }
    
    void PhysicsWorld::step(GLfloat elapsed) {
        dynamicsWorld.stepSimulation(elapsed, maxSubSteps, fixedTimeStep);
    }
    
    void PhysicsWorld::registerRigidBody(btRigidBody * rigidBody, Instance * instance) {
        auto iter = rigidBodyToInstance.find(rigidBody);
        if (iter == rigidBodyToInstance.end()) {
            dynamicsWorld.addRigidBody(rigidBody);
            rigidBodyToInstance.emplace(rigidBody, instance);
        } else {
            std::cerr << "WARNING: PhysicsWorld: " << "Tried to register an instance to the same rigid body twice.\n";
        }
    }
    
    void PhysicsWorld::unregisterRigidBody(btRigidBody * rigidBody) {
        auto iter = rigidBodyToInstance.find(rigidBody);
        if (iter == rigidBodyToInstance.end()) {
            std::cerr << "WARNING: PhysicsWorld: " << "Tried to unregister a rigid body which was not registered.\n";
        } else {
            dynamicsWorld.removeRigidBody(rigidBody);
            rigidBodyToInstance.erase(iter);
        }
    }
    
    Instance * PhysicsWorld::getInstanceFromRigidBody(const btRigidBody * rigidBody) const {
        auto iter = rigidBodyToInstance.find(rigidBody);
        if (iter == rigidBodyToInstance.end()) {
            return NULL;
        }
        return iter->second;
    }
    
    // ========================= Raycasting ====================================
    
    const btCollisionObject * PhysicsWorld::raycastSingle(GLvec3 & hitPoint, const GLvec3 & startPoint, GLfloat hDir, GLfloat vDir, GLfloat length) const {
        return raycastSingle(hitPoint, startPoint, GLvec3(startPoint.x + cos(hDir) * cos(vDir) * length,
                                                          startPoint.y - sin(hDir) * cos(vDir) * length,
                                                          startPoint.z - sin(vDir) * length));
    }
    
    const btCollisionObject * PhysicsWorld::raycastSingle(GLvec3 & hitPoint, const GLvec3 & startPoint, const GLvec3 & endPoint) const {
        // Convert start/end points to bullet vectors.
        btVector3 start = ConversionUtil::glmToBulletVec3(startPoint),
                    end = ConversionUtil::glmToBulletVec3(endPoint);
        
        // Do the raytest using the default closest callback.
        btCollisionWorld::ClosestRayResultCallback result(start, end);
        dynamicsWorld.rayTest(start, end, result);
        
        // Set hitpoint and return collision object, on hit.
        if (result.hasHit()) {
            hitPoint = ConversionUtil::bulletToGlmVec3(result.m_hitPointWorld);
            return result.m_collisionObject;
        }
        return NULL;
    }

    Instance * PhysicsWorld::raycastSingleInstance(GLvec3 & hitPoint, const GLvec3 & startPoint, GLfloat hDir, GLfloat vDir, GLfloat length) const {
        return raycastSingleInstance(hitPoint, startPoint, GLvec3(startPoint.x + cos(hDir) * cos(vDir) * length,
                                                                  startPoint.y - sin(hDir) * cos(vDir) * length,
                                                                  startPoint.z - sin(vDir) * length));
    }
    
    Instance * PhysicsWorld::raycastSingleInstance(GLvec3 & hitPoint, const GLvec3 & startPoint, const GLvec3 & endPoint) const {
        // Convert start/end points to bullet vectors.
        btVector3 start = ConversionUtil::glmToBulletVec3(startPoint),
                    end = ConversionUtil::glmToBulletVec3(endPoint);
        
        // Do the raytest using our custom instance callback.
        InstanceRayCallback result(start, end, this);
        dynamicsWorld.rayTest(start, end, result);
        
        // Set hitpoint and return instance, on hit.
        if (result.hasHit()) {
            hitPoint = ConversionUtil::bulletToGlmVec3(result.getHitPointWorld());
            return result.getFoundInstance();
        }
        return NULL;
    }
}
