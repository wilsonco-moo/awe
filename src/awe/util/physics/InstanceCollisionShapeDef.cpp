/*
 * InstanceCollisionShapeDef.cpp
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#include "InstanceCollisionShapeDef.h"

#include <bullet3/BulletCollision/CollisionShapes/btCollisionShape.h>
#include <bullet3/BulletCollision/CollisionShapes/btCompoundShape.h>
#include <bullet3/LinearMath/btTransform.h>
#include <glm/gtc/matrix_transform.hpp>

#include "AssetCollisionShapeDef.h"
#include "shapes/ShapeDef.h"

namespace awe {

    InstanceCollisionShapeDef::InstanceCollisionShapeDef(const AssetCollisionShapeDef * assetCollision, const GLvec3 & scale) :
        shapes(),
        totalMass(0.0f) {
        populateCollisionShapes(shapes, totalMass, physicsToWorldTransform, worldToPhysicsTransform, assetCollision, scale);
        shapes.shrink_to_fit();
    }
    
    InstanceCollisionShapeDef::~InstanceCollisionShapeDef(void) {
        for (btCollisionShape * shape : shapes) {
            delete shape;
        }
    }
    
    btCollisionShape * InstanceCollisionShapeDef::populateCollisionShapes(std::vector<btCollisionShape *> & shapes, GLfloat & totalMass,
                                                                          GLmat4 & physicsToWorldTransform, GLmat4 & worldToPhysicsTransform,
                                                                          const AssetCollisionShapeDef * assetCollision, const GLvec3 & scale) {
        btCollisionShape * primaryShape;

        if (assetCollision->doesRequireCompoundShape()) {
            // Create a compound shape, then populate it with shapes. Each shape needs a transformation
            // relative to the compound shape's origin.
            btCompoundShape * compoundShape = new btCompoundShape(true, assetCollision->getShapes().size());
            primaryShape = compoundShape;
            shapes.push_back(compoundShape);
            for (ShapeDef * shapeDef : assetCollision->getShapes()) {
                btCollisionShape * collisionShape = shapeDef->createBulletCollisionShape(scale);
                btTransform transform;
                shapeDef->getTransform(transform, scale);
                compoundShape->addChildShape(transform, collisionShape);
                shapes.push_back(collisionShape);
                totalMass += shapeDef->getMass();
            }
        } else {
            // If no compound shape is needed, we don't need to worry about the
            // position and orientation of the single collision shape: it is
            // baked into assetCollision's physicsTransform. 
            ShapeDef * shapeDef = assetCollision->getShapes().front();
            primaryShape = shapeDef->createBulletCollisionShape(scale);
            shapes.push_back(primaryShape);
            totalMass += shapeDef->getMass();
        }
        
        GLmat4 scaleTransform   = glm::scale(GLmat4(1.0f), scale),
               physicsTransform = assetCollision->getPhysicsTransform();
        
        // To transform to world coordinates, inverse the physics transform. Reset to the asset's
        // scale afterwards, to ensure that any translations done in physicsTransform are scaled.
        // NOTE: This does not treat non uniform scales correctly when there is a rotation in
        //       the physics transform, since the scale is not done in the rotation space of the
        //       instance's origin, like it should be. Although, this *does* mean that dynamic
        //       instances are displayed more consistently with how the physics system sees their
        //       collision shapes, since collision shape scale cannot be done in the rotation
        //       space of the instance's origin, without causing skew.
        physicsToWorldTransform = scaleTransform * glm::inverse(physicsTransform);
        
        // Physics must be given transformation without scale, but translations done in
        // physicsTransform must still be scaled. So first transform by an inverse scale,
        // then physics transform. Adjust the inverse scale to counter the rotations in
        // physics transform, so the physics is not sent a non-uniform scale, even when
        // using a non uniform scale and rotation in physics transform.
        // NOTE: Collision shape will not be correct in this case however, since collision
        //       shape scales are not done in the rotation space of the instance's origin,
        //       as that would cause skew when using non uniform scales.
        GLmat4 inverseScale = GLmat4(glm::inverse(GLmat3(scaleTransform) * GLmat3(physicsTransform)) * GLmat3(physicsTransform));
        worldToPhysicsTransform = physicsTransform * inverseScale;
        
        return primaryShape;
    }
}

