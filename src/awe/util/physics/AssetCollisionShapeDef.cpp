/*
 * AssetCollisionShapeDef.cpp
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#include "AssetCollisionShapeDef.h"

#include <tinyxml2/tinyxml2.h>
#include <iostream>
#include <cstddef>
#include <string>

#include "shapes/ShapeDef.h"
#include "../Util.h"

namespace awe {

    // When any XML errors happen, print a complaint, set error flag and return.
    #define PRINT_XML_ERROR(xmlErr)                                                                     \
        std::cerr << "WARNING: AssetCollisionShapeDef: " << "Error in XML file: " << xmlErr << '\n';    \
        errorFlag = true;                                                                               \
        return;

    AssetCollisionShapeDef::AssetCollisionShapeDef(tinyxml2::XMLElement * element, bool & errorFlag) :
        shapes() {
        
        // Work out collision mode.
        GET_ATTRIB_STDSTRING(collisionModeAttr, element, "mode")
        if (collisionModeAttr == "static") {
            isStatic = true;
        } else if (collisionModeAttr == "dynamic") {
            isStatic = false;
        } else {
            PRINT_XML_ERROR("Unknown collision type " << collisionModeAttr << ". Only \"static\" and \"dynamic\" are allowed.\n")
        }
        
        // Iterate all collision shape elements. For each one, use the method
        // from ShapeDef to create a collision shape. Give up if it reports an error.
        ELEM_CHILDREN_FOR_EACH_ALL(collisionShapeElem, element) {
            ShapeDef * shape = ShapeDef::createShape(collisionShapeElem, errorFlag);
            if (errorFlag) return;
            if (shape != NULL) shapes.push_back(shape);
        }
        
        // Complain and give up if we have no collision shapes.
        if (shapes.size() == 0) {
            PRINT_XML_ERROR("Collision for asset has no collision shapes.")
        }
        
        // We require a compound shape if we have more than one collision shape, (this is default unless user specifies otherwise).
        requiresCompoundShape = (shapes.size() > 1);
        const char * requiresCompoundShapeAttr = element->Attribute("requiresCompoundShape");
        if (requiresCompoundShapeAttr != NULL) {
            if (shapes.size() > 1) {
                PRINT_XML_ERROR("requiresCompoundShape attribute is only valid for a single collision shape, to force the use of a compound shape anyway.")
            }
            requiresCompoundShape = Util::stringToBool(requiresCompoundShapeAttr);
        }
        
        // Read the physics transform. Use default (identity) matrix if it is not specified.
        tinyxml2::XMLElement * physicsTransformElem = element->FirstChildElement("physicsTransform");
        if (physicsTransformElem == NULL) {
            physicsTransform = GLmat4(1.0f);
        } else {
            physicsTransform = Util::readTransformation(physicsTransformElem);
        }
        
        // If we are not using a compound shape, for the single shape we have,
        // reset its position to identity and bake its transformation into
        // physicsTransform. This has to be done since bullet physics does not
        // support changing the origin of a single shape.
        if (!requiresCompoundShape) {
            physicsTransform = physicsTransform * shapes.front()->getCombinedTransform();
            shapes.front()->resetToIdentityTransform();
        }
    }
    
    AssetCollisionShapeDef::~AssetCollisionShapeDef(void) {
        for (ShapeDef * shape : shapes) {
            delete shape;
        }
    }
}
