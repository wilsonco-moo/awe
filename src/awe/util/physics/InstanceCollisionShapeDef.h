/*
 * InstanceCollisionShapeDef.h
 *
 *  Created on: 31 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PHYSICS_INSTANCECOLLISIONSHAPEDEF_H_
#define AWE_UTIL_PHYSICS_INSTANCECOLLISIONSHAPEDEF_H_

#include <vector>

#include "../types/Types.h"

class btCollisionShape;

namespace awe {
    class AssetCollisionShapeDef;

    /**
     * An InstanceCollisionShape represents the collision information for
     * a single instance. It provides (and manages) a bullet physics collision
     * shape, which can be directly used.
     * 
     * Note: Do not make subclasses.
     */
    class InstanceCollisionShapeDef {
    private:
        std::vector<btCollisionShape *> shapes;
        
        GLmat4 worldToPhysicsTransform,
               physicsToWorldTransform;
        
        GLfloat totalMass;
        
        // Don't allow copying: we hold raw resources.
        InstanceCollisionShapeDef(const InstanceCollisionShapeDef & other);
        InstanceCollisionShapeDef & operator = (const InstanceCollisionShapeDef & other);
        
    public:
        InstanceCollisionShapeDef(const AssetCollisionShapeDef * assetCollision, const GLvec3 & scale);
        ~InstanceCollisionShapeDef(void);
        
        /**
         * Gets the collision shape which should be used.
         * This is either a single collision shape, or a compound shape linked
         * to some others.
         */
        inline btCollisionShape * getPrimaryCollisionShape(void) const {
            return shapes.front();
        }
        
        /**
         * The transformation from the instance, to the origin of it's physics
         * shape.
         * Note that this first scales by one divided by the scale of the
         * instance.
         * This is the inverse of getPhysicsToWorldTransform().
         */
        inline const GLmat4 & getWorldToPhysicsTransform(void) const {
            return worldToPhysicsTransform;
        }
        
        /**
         * The transformation from the instance's collision shape origin, to
         * the transformation of the instance.
         * Note that this scales to the scale of the instance.
         * This is the inverse of getWorldToPhysicsTransform().
         */
        inline const GLmat4 & getPhysicsToWorldTransform(void) const {
            return physicsToWorldTransform;
        }
        
        /**
         * Returns the total mass of all of our collision shapes.
         */
        inline GLfloat getTotalMass(void) const {
            return totalMass;
        }
        
        /**
         * Appends to the back of the provided vector of collision shapes,
         * appropriate collision shapes for the provided AssetCollisionShapeDef
         * and scale. The primary collision shape is always the first one added
         * to the vector, and for convenience it is returned.
         * The provided matrices are set to the appropriate physics to world,
         * and world to physics transformations. The mass of all of the shapes
         * is also ADDED to the provided total mass.
         * This static method provides an alternative to directly using an
         * InstanceCollisionShapeDef instance.
         */
        static btCollisionShape * populateCollisionShapes(std::vector<btCollisionShape *> & shapes, GLfloat & totalMass,
                                                          GLmat4 & physicsToWorldTransform, GLmat4 & worldToPhysicsTransform,
                                                          const AssetCollisionShapeDef * assetCollision, const GLvec3 & scale);
    };
}

#endif
