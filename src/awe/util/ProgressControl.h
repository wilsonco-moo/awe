/*
 * ProgressControl.h
 *
 *  Created on: 10 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_PROGRESSCONTROL_H_
#define AWE_UTIL_PROGRESSCONTROL_H_

namespace awe {

    /**
     * This class allows working out a semi-reasonable progress estimate, from
     * the sum of "remaining" values which awe resource loader classes return.
     * The "remaining" values are very sloppy estimates, and cannot be used
     * directly as progress, as new things to load are noticed half way through
     * loading such as loading textures for assets, (which is only found out
     * about when the asset's XML file is loaded, which is put to the back of
     * the thread queue).
     * 
     * To generate a semi-sensible progress value, an amount of "sloppiness"
     * is put into the estimate. This works by only allowing each consecutive
     * peak in remaining values to occupy a "slop" proportion of the total
     * progress. This is reset and recalculated each time the remaining values
     * peak. This way, decreases in remaining always provide fairly sensible
     * advances in progress, and backward progress never happens, all while
     * allowing for an arbitrary number of peaks in the remaining estimate.
     * 
     * The "slop" proportion must be larger than zero, and have a maximum of
     * one. It represents the proportion of the total progress represented
     * by the first peak in "remaining" values.
     * The "slop" proportion must be tailored for the specific application,
     * in order to achieve a useful progress estimate.
     *
     *  > Situations with fairly sensible remaining values, which rarely
     *    increase by much (if at all), should use a slop value close to 1.
     *  > Awe resource loaders typically find a bunch of new resources to load
     *    about half way through the loading procedure. A slop value of 0.85
     *    is typically most appropriate, (and is the default).
     *  > Situations where the remaining values are very erratic, with new
     *    things to load regularly found during the loading procedure, should
     *    use a small slop value (like 0.5).
     */
    class ProgressControl {
    private:
        // Current slop value.
        float slop;
        
        // Used by estimateProgress to control calls to progressTick.
        bool isFirst;
        unsigned int lastRemaining;
        float lastProgressEstimate;
        
        // Used by progressTick
        unsigned int lastTickRemaining, // Previous remaining
                     lastPeakRemaining, // Previous peak remaining
                     completedJobs;     // Number of jobs completed since previous peak
        int lastTickChange;       // Change in remaining on previous tick
        float lastTickProgress,   // Progress reported by previous tick (never return lower than this).
              baseProgress,       // Progress to use as base (progress at most recent peak).
              progressMultiplier; // Multiplier for values after most recent peak (avoid going past slop).
        
    public:
        /**
         * The main constructor for ProgressControl. The default slop value is
         * 0.85 - this default provides a reasonable balance for how awe usually
         * acts.
         */
        ProgressControl(float slop = 0.85f);
        
    private:
        // A single progress tick. Called by estimateProgress when the remaining
        // value is different to last time.
        float progressTick(unsigned int remaining);
        
    public:
        /**
         * Given the current remaining value, estimates the progress, and
         * reports whether it has changed since last time.
         * This returns an estimate of 0 for the first (non-zero) remaining
         * value, and an estimate of 1 for the last (zero) remaining value.
         * 
         * This will NEVER return an estimate which is less than a previous one,
         * as long as at least one previous estimate was greater than zero,
         * unless reset is called.
         */
        void estimateProgress(float * progressEstimate, bool * hasChanged, unsigned int remaining);
        
        /**
         * Resets the progress control ready for re-use.
         */
        void reset(float newSlop = 0.85f);
        
        /**
         * Prints progress, as a progress bar looking thing of length "length".
         */
        static void printProgress(float progress, unsigned int length);
    };
}

#endif
