/*
 * ConversionUtil.h
 *
 *  Created on: 27 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_TYPES_CONVERSIONUTIL_H_
#define AWE_UTIL_TYPES_CONVERSIONUTIL_H_

#include <bullet3/LinearMath/btQuaternion.h>
#include <bullet3/LinearMath/btVector3.h>
#include <cmath>

#include "Types.h"

namespace awe {

    /**
     * This class contains methods to convert between types, and constants
     * to convert between units.
     */
    class ConversionUtil {
    public:
        
        // ------------------------- Type conversions --------------------------
        
        /**
         * Converts back and fourth between glm and bullet physics 3-vectors.
         */
        static btVector3 glmToBulletVec3(const GLvec3 & vec);
        static GLvec3 bulletToGlmVec3(const btVector3 & vec);
        
        /**
         * Converts back and fourth between glm and bullet physics quaternions.
         */
        static btQuaternion glmToBulletQuat(const GLquat & quat);
        static GLquat bulletToGlmQuat(const btQuaternion & quat);
        
        
        // ------------------------ Unit conversions ---------------------------
        
        /**
         * Multiplying by each of these values converts between metres per
         * second and miles per hour.
         * MS_TO_MPH is derived as:
         *   1 metre per second is 3600 metres per hour
         *   Divide by size of a yard, in metres, to convert to yards per hour
         *   Divide by number of yards in a mile, to convert to miles per hour
         */
        static constexpr double MS_TO_MPH = (3600.0 / (2.54 * 12.0 * 3.0 * 0.01)) / 1760.0,
                                MPH_TO_MS = 1760.0 / (3600.0 / (2.54 * 12.0 * 3.0 * 0.01));
        /**
         * Multiplying by each of these values converts between radians per
         * second and revolutions per minute (RPM).
         * RADS_TO_RPM is derived as:
         *   1 radian per second is 60 radians per minute
         *   Multiply by (180/pi) to convert to degrees per minute
         *   Divide by 360 to convert to revolutions per minute
         */
        static constexpr double RADS_TO_RPM = (60.0 * (180.0 / M_PI)) / 360.0,
                                RPM_TO_RADS = 360.0 / (60.0 * (180.0 / M_PI));
    };
}

#endif
