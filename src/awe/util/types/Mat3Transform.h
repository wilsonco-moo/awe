/*
 * Mat3Transform.h
 *
 *  Created on: 27 Sep 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_TYPES_MAT3TRANSFORM_H_
#define AWE_UTIL_TYPES_MAT3TRANSFORM_H_

#include "Types.h"

namespace awe {

    /**
     * This class provides static utility methods for 2D transformations using
     * 3x3 matrices.
     */
    class Mat3Transform {
    public:
        
        /**
         * Creates a transformation matrix which translates by the provided
         * offset.
         */
        static GLmat3 translate(GLfloat x, GLfloat y);
        static inline GLmat3 translate(const GLvec2 & offset) {
            return translate(offset.x, offset.y);
        }
        
        /**
         * Creates a transformation matrix which scales by the provided amount.
         */
        static GLmat3 scale(GLfloat x, GLfloat y);
        static inline GLmat3 scale(const GLvec2 & amount) {
            return scale(amount.x, amount.y);
        }
        
        /**
         * Creates a transformation matrix which rotates by the provided angle
         * (in radians).
         */
        static GLmat3 rotate(GLfloat angle);
    };
}

#endif
