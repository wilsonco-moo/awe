/*
 * Types.cpp
 *
 *  Created on: 16 Jan 2020
 *      Author: wilson
 */

#include "Types.h"

namespace awe {
    
    // Assert that the compiler is properly complying with the sizes of (the most used) OpenGL typedefs.
    static_assert(sizeof(GLdouble) == 8);
    static_assert(sizeof(GLfloat) == 4);
    static_assert(sizeof(GLint) == 4);
    static_assert(sizeof(GLuint) == 4);
    
    // Make sure the compiler hasn't done anything funny with the sizes of any
    // of the matrix or vector types, since we use them for communication with
    // the graphics card.
    static_assert(sizeof(GLmat4) == (4 * 4) * 4);
    static_assert(sizeof(GLmat3) == (3 * 3) * 4);
    static_assert(sizeof(GLvec4) == 4 * 4);
    static_assert(sizeof(GLvec3) == 3 * 4);
    static_assert(sizeof(GLvec2) == 2 * 4);
    
    static_assert(sizeof(GLimat4) == (4 * 4) * 4);
    static_assert(sizeof(GLimat3) == (3 * 3) * 4);
    static_assert(sizeof(GLivec4) == 4 * 4);
    static_assert(sizeof(GLivec3) == 3 * 4);
    static_assert(sizeof(GLivec2) == 2 * 4);
    
    static_assert(sizeof(GLumat4) == (4 * 4) * 4);
    static_assert(sizeof(GLumat3) == (3 * 3) * 4);
    static_assert(sizeof(GLuvec4) == 4 * 4);
    static_assert(sizeof(GLuvec3) == 3 * 4);
    static_assert(sizeof(GLuvec2) == 2 * 4);
}
