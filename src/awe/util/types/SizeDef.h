/*
 * SizeDef.h
 * C/GLSL header file
 * 
 *  Created on: 9 Aug 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_TYPES_SIZEDEF_H_
#define AWE_UTIL_TYPES_SIZEDEF_H_

/**
 * This header file contains some generic size macros, for use within GLSL. This
 * includes uniform sizes, and the sizes of the most common types (in bytes).
 */



/**
 * This is the maximum uniform size that we should have bound at any one time,
 * (in bytes). This must be smaller or equal to GL_UNIFORM_BLOCK_SIZE_DATA.
 * OpenGL guarantees that GL_UNIFORM_BLOCK_SIZE_DATA is at least 16KiB.
 * By default, we have this value set to 16384 to support all hardware.
 */
#define SIZEOF_MAX_UNIFORM 16384u

/**
 * Sizes of scalar types (in bytes).
 */
#define SIZEOF_INT    4u
#define SIZEOF_UINT   4u
#define SIZEOF_FLOAT  4u
#define SIZEOF_DOUBLE 8u

/**
 * Sizes of two-vector types (in bytes).
 */
#define SIZEOF_IVEC2 (SIZEOF_INT    * 2u)
#define SIZEOF_UVEC2 (SIZEOF_UINT   * 2u)
#define SIZEOF_VEC2  (SIZEOF_FLOAT  * 2u)
#define SIZEOF_DVEC2 (SIZEOF_DOUBLE * 2u)

/**
 * Sizes of three-vector types (in bytes).
 */
#define SIZEOF_IVEC3 (SIZEOF_INT    * 3u)
#define SIZEOF_UVEC3 (SIZEOF_UINT   * 3u)
#define SIZEOF_VEC3  (SIZEOF_FLOAT  * 3u)
#define SIZEOF_DVEC3 (SIZEOF_DOUBLE * 3u)

/**
 * Sizes of four-vector types (in bytes).
 */
#define SIZEOF_IVEC4 (SIZEOF_INT    * 4u)
#define SIZEOF_UVEC4 (SIZEOF_UINT   * 4u)
#define SIZEOF_VEC4  (SIZEOF_FLOAT  * 4u)
#define SIZEOF_DVEC4 (SIZEOF_DOUBLE * 4u)

/**
 * Sizes of two-matrix types (in bytes).
 */
#define SIZEOF_IMAT2 (SIZEOF_INT    * 4u)
#define SIZEOF_UMAT2 (SIZEOF_UINT   * 4u)
#define SIZEOF_MAT2  (SIZEOF_FLOAT  * 4u)
#define SIZEOF_DMAT2 (SIZEOF_DOUBLE * 4u)

/**
 * Sizes of three-matrix types (in bytes).
 */
#define SIZEOF_IMAT3 (SIZEOF_INT    * 9u)
#define SIZEOF_UMAT3 (SIZEOF_UINT   * 9u)
#define SIZEOF_MAT3  (SIZEOF_FLOAT  * 9u)
#define SIZEOF_DMAT3 (SIZEOF_DOUBLE * 9u)

/**
 * Sizes of four-matrix types (in bytes).
 */
#define SIZEOF_IMAT4 (SIZEOF_INT    * 16u)
#define SIZEOF_UMAT4 (SIZEOF_UINT   * 16u)
#define SIZEOF_MAT4  (SIZEOF_FLOAT  * 16u)
#define SIZEOF_DMAT4 (SIZEOF_DOUBLE * 16u)

#endif
