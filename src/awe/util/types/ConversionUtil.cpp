/*
 * ConversionUtil.cpp
 *
 *  Created on: 27 Jan 2020
 *      Author: wilson
 */

#include "ConversionUtil.h"

namespace awe {
    
    btVector3 ConversionUtil::glmToBulletVec3(const GLvec3 & vec) {
        return btVector3(vec.x, vec.y, vec.z);
    }
    
    GLvec3 ConversionUtil::bulletToGlmVec3(const btVector3 & vec) {
        return GLvec3(vec.getX(), vec.getY(), vec.getZ());
    }
    
    btQuaternion ConversionUtil::glmToBulletQuat(const GLquat & quat) {
        // NOTE: glm Quaternions take parameters in order w,x,y,z in constructor
        //       bullet physics Quaternions take parameters in order x,y,z,w in constructor.
        return btQuaternion(quat.x, quat.y, quat.z, quat.w);
    }
    
    GLquat ConversionUtil::bulletToGlmQuat(const btQuaternion & quat) {
        // NOTE: glm Quaternions take parameters in order w,x,y,z in constructor
        //       bullet physics Quaternions take parameters in order x,y,z,w in constructor.
        return GLquat(quat.getW(), quat.getX(), quat.getY(), quat.getZ());
    }
}
