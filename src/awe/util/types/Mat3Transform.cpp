/*
 * Mat3Transform.cpp
 *
 *  Created on: 27 Sep 2020
 *      Author: wilson
 */

#include "Mat3Transform.h"

#include <cmath>

namespace awe {
    
    GLmat3 Mat3Transform::translate(GLfloat x, GLfloat y) {
        return GLmat3({1,0,0, 0,1,0, x,y,1});
    }
    
    GLmat3 Mat3Transform::scale(GLfloat x, GLfloat y) {
        return GLmat3({x,0,0, 0,y,0, 0,0,1});
    }
    
    GLmat3 Mat3Transform::rotate(GLfloat angle) {
        return GLmat3({std::cos(angle),-std::sin(angle),0,
                       std::sin(angle),std::cos(angle),0,
                       0,0,1});
    }
}
