/*
 * Types.h
 *
 *  Created on: 16 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_TYPES_TYPES_H_
#define AWE_UTIL_TYPES_TYPES_H_

#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/vec4.hpp>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <GL/gl.h>

namespace awe {
    
    /**
     * All of these typedefs are equivalents of base glm types, but
     * guaranteed to work nicely with OpenGL, since they use OpenGL typedefs.
     */
     
    // Equivalent of glm::mat4.
    using GLmat4 = glm::tmat4x4<GLfloat>;
    
    // Equivalent of glm::mat3.
    using GLmat3 = glm::tmat3x3<GLfloat>;
    
    // Equivalent of glm::mat2.
    using GLmat2 = glm::tmat2x2<GLfloat>;
    
    // Equivalent of glm::vec4.
    using GLvec4 = glm::tvec4<GLfloat>;
    
    // Equivalent of glm::vec3.
    using GLvec3 = glm::tvec3<GLfloat>;
    
    // Equivalent of glm::vec2.
    using GLvec2 = glm::tvec2<GLfloat>;
    
    
    
    // Equivalent of glm::imat4.
    using GLimat4 = glm::tmat4x4<GLint>;
    
    // Equivalent of glm::imat3.
    using GLimat3 = glm::tmat3x3<GLint>;
    
    // Equivalent of glm::ivec4.
    using GLivec4 = glm::tvec4<GLint>;
    
    // Equivalent of glm::ivec3.
    using GLivec3 = glm::tvec3<GLint>;
    
    // Equivalent of glm::ivec2.
    using GLivec2 = glm::tvec2<GLint>;
    
    
    
    // Equivalent of glm::imat4.
    using GLumat4 = glm::tmat4x4<GLuint>;
    
    // Equivalent of glm::imat3.
    using GLumat3 = glm::tmat3x3<GLuint>;
    
    // Equivalent of glm::ivec4.
    using GLuvec4 = glm::tvec4<GLuint>;
    
    // Equivalent of glm::ivec3.
    using GLuvec3 = glm::tvec3<GLuint>;
    
    // Equivalent of glm::ivec2.
    using GLuvec2 = glm::tvec2<GLuint>;
    
    
    // Equivalent of glm::quat
    using GLquat = glm::tquat<GLfloat>;
}

#endif
