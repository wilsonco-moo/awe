/*
 * Util.h
 *
 *  Created on: 14 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_UTIL_UTIL_H_
#define AWE_UTIL_UTIL_H_

#include <bullet3/LinearMath/btVector3.h>
#include <cstdint>
#include <string>

#include "types/Types.h"

class btCollisionShape;
class btRigidBody;

namespace tinyxml2 {
    class XMLElement;
}

namespace awe {
    class PhysicsWorld;

    /**
     * This class provides some convenient static utility methods, and some
     * helpful macros for dealing with tinyxml2.
     */
    class Util {
    public:
    
        /**
         * These two methods convert back and fourth between 4x4 matrices
         * and strings, without losing precision.
         * NOTE: If hex is set to false in mat4ToString, precision WILL be lost.
         */
        static std::string mat4ToString(const GLmat4 & matrix, bool hex = true, bool newlines = true);
        static GLmat4 stringToMat4(const std::string & string);
        
        /**
         * Rounds the value up to the nearest power of two.
         * The value is assumed to NOT be zero.
         * Reference: https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
         *            http://web.archive.org/web/20200120124112/https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
         */
        static uint32_t roundUpToPower2(uint32_t value);
        
        /**
         * Finds whether the cube (box) specified between the two corner points,
         * intersects with the sphere defined by sphereCentre and sphereRadius.
         * Reference: https://stackoverflow.com/a/4579069
         *            http://web.archive.org/web/20170322205504/https://stackoverflow.com/questions/4578967/cube-sphere-intersection-test
         * 
         * doesCubeIntersectSphere requires that cube corner points are element-wise sorted, i.e:
         *      cubeCorner1.x <= cubeCorner2.x &&
         *      cubeCorner1.y <= cubeCorner2.y &&
         *      cubeCorner1.z <= cubeCorner2.z
         *
         * doesCubeIntersectSphereUnsorted does not have this requirement, but is slower.
         */
        static bool doesCubeIntersectSphere(const GLvec3 & cubeCorner1, const GLvec3 & cubeCorner2, const GLvec3 & sphereCentre, GLfloat sphereRadius);
        static bool doesCubeIntersectSphereUnsorted(const GLvec3 & cubeCorner1, const GLvec3 & cubeCorner2, const GLvec3 & sphereCentre, GLfloat sphereRadius);
        
        /**
         * Reads the transformation specified in the provided XMLElement's attributes.
         * 
         * Note that transformations are done in the following order:
         * Scale then rotate X then rotate Y then rotate Z then translate.
         * 
         * This transformation order is exactly the same as blender.
         * Transformations can be extracted from blender, for the currently
         * selected object, using the python console.
         * >>> bpy.context.object.scale             This extracts the scale of the object.
         * >>> bpy.context.object.rotation_euler    This extracts the euler rotation of the object. Remember to convert from radians to degrees for the XML file.
         * >>> bpy.context.object.location          This extracts the location (translation) of the object.
         */
        static GLmat4 readTransformation(tinyxml2::XMLElement * element);
        
        /**
         * Works out a perspective transformation, using horizontal fov instead
         * of vertical fov.
         */
        static GLmat4 perspectiveX(GLfloat fovX, GLfloat viewWidth, GLfloat viewHeight, GLfloat nearClip, GLfloat farClip);
                
        /**
         * Calculates the inertia of a collision shape, using the specified mass,
         * then returns it. This simple method makes the process more convenient,
         * as it makes inertia calculation possible within an initialiser list.
         */
        static btVector3 getInertia(const btCollisionShape & shape, GLfloat mass);
    
        /**
         * Adds a rigid body to a physics world, then returns it. Much like the getInertia
         * method, this only exists to make this process more convenient, so it can be
         * done in an initialiser list.
         */
        static btRigidBody * getAndAddToPhysics(PhysicsWorld * physicsWorld, btRigidBody * rigidBody);
        
        /**
         * This method acts exactly like glGetUniformLocation, except an
         * additional parameter (identifier) is provided for logging purposes.
         * If the uniform cannot be found, a warning is printed to the log,
         * similar to:
         * WARNING: <identifier>: Cannot find uniform "<name>" in shader program.
         */
        static GLint getUniformLocationChecked(GLuint program, const char * identifier, const char * name);
        
        /**
         * This method acts exactly like glGetUniformBlockIndex, except an
         * additional parameter (identifier) is provided for logging purposes.
         * If the uniform cannot be found, a warning is printed to the log,
         * similar to:
         * WARNING: <identifier>: Cannot find uniform block index "<uniformBlockName>" in shader program.
         */
        static GLuint getUniformBlockIndexChecked(GLuint program, const char * identifier, const char * uniformBlockName);
        
        /**
         * This adds a callback to enable debug messages. Note that this is only
         * available for OpenGL versions 4.3+, so should only be used as a
         * debugging measure.
         */
        static void enableOpenGLDebugMessages(void);
        
        /**
         * Given the specified shader, this method compiles it using
         * glCompileShader then checks the result. If doing so produced a log,
         * the log is printed to the console. Logs are printed alongside the
         * provided name, in order to identify this particular shader.
         * If the shader failed to compile, an error message is printed,
         * and exit(EXIT_FAILURE) is run.
         */
        static void compileAndCheckShader(const std::string & name, GLuint shader);
        
        /**
         * Given the specified shader program, this method links it using
         * glLinkProgram then checks the result. If doing so produced a log,
         * the log is printed to the console. Logs are printed alongside the
         * provided name, in order to identify this particular shader program.
         * If the shader program failed to link, an error message is printed,
         * and exit(EXIT_FAILURE) is run.
         */
        static void linkAndCheckProgram(const std::string & name, GLuint program);
        
        /**
         * Given the provided identifying name, and sources for three shader
         * stages, this compiles and links an entire shader program. This is
         * done using the two methods above, to ensure that any logs are printed
         * alongside the provided identifying name.
         * To skip a program stage (such as the geometry shader), specify NULL
         * for the source string.
         */
        static GLuint compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource);
        
        /**
         * Converts the string to a bool value, ignoring case.
         * True, yes and 1 are considered true,
         * False, no and 0 are considered false,
         * everything else is considered false.
         */
        static bool stringToBool(const char * str);
    };
    
    // ------------------ XML UTILITY MACROS -----------------------------------
    
    /**
     * All of these macros require PRINT_XML_ERROR to be defined. This must
     * print the provided message, and do any appropriate error handling.
     */
    
    // Accesses a first child element with the specified name.
    #define GET_ELEM(elemVarName, baseElem, elemName)                                 \
        tinyxml2::XMLElement * elemVarName = (baseElem)->FirstChildElement(elemName); \
        if ((elemVarName) == NULL) {                                                  \
            PRINT_XML_ERROR("Cannot find element " << (elemName) << '.')              \
        }
    
    // Accesses the attribute with the specified name, as a const char *.
    #define GET_ATTRIB(attribVarName, baseElem, attribName)                  \
        const char * attribVarName = (baseElem)->Attribute(attribName);      \
        if ((attribVarName) == NULL) {                                       \
            PRINT_XML_ERROR("Cannot find attribute " << (attribName) << '.') \
        }
    
    // Accesses the attribute, converts it to specified type with specified conversion function.
    // Use one of the other GET_ATTRIB_* matcors instead of this, for convenience.
    #define GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, type, convFunc, ...) \
        type attribVarName;                                                                        \
        {                                                                                          \
            GET_ATTRIB(attribVarName##String, baseElem, attribName)                                \
            attribVarName = convFunc(attribVarName##String __VA_ARGS__);                           \
        }
    
    // Access attributes, converted appropriately to:
    // unsigned long int, float, GLfloat, double, GLmat4.
    // Add more macros here as appropriate for more types.
    #define GET_ATTRIB_UINT(attribVarName, baseElem, attribName)      GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, unsigned long int, std::strtoul, , NULL, 0)
    #define GET_ATTRIB_FLOAT(attribVarName, baseElem, attribName)     GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, float, std::strtof, , NULL)
    #define GET_ATTRIB_GLFLOAT(attribVarName, baseElem, attribName)   GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, GLfloat, std::strtof, , NULL)
    #define GET_ATTRIB_DOUBLE(attribVarName, baseElem, attribName)    GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, double, std::strtod, , NULL)
    #define GET_ATTRIB_MAT4(attribVarName, baseElem, attribName)      GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, GLmat4, awe::Util::stringToMat4)
    #define GET_ATTRIB_STDSTRING(attribVarName, baseElem, attribName) GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, std::string, std::string)
    #define GET_ATTRIB_BOOL(attribVarName, baseElem, attribName)      GET_ATTRIB_CONVERTED_TO_TYPE(attribVarName, baseElem, attribName, bool, awe::Util::stringToBool)

    /**
     * Allows easy iteration through the child elements of an element.
     * credit: https://stackoverflow.com/a/34622910
     */
    #define ELEM_CHILDREN_FOR_EACH(forLoopElementName, elementToSearch, nameToFind)                       \
        for(tinyxml2::XMLElement * forLoopElementName = (elementToSearch)->FirstChildElement(nameToFind); \
            (forLoopElementName) != NULL;                                                                 \
            forLoopElementName = (forLoopElementName)->NextSiblingElement(nameToFind))
    
    /**
     * Allows easy iteration through the child elements of an element, without
     * needing to supply a name for the element, (i.e: This spits out all
     * elements regardless of their name).
     */
    #define ELEM_CHILDREN_FOR_EACH_ALL(forLoopElementName, elementToSearch)                     \
        for(tinyxml2::XMLElement * forLoopElementName = (elementToSearch)->FirstChildElement(); \
            (forLoopElementName) != NULL;                                                       \
            forLoopElementName = (forLoopElementName)->NextSiblingElement())

    // -------------------------------------------------------------------------
}

#endif
