/*
 * Instance.cpp
 *
 *  Created on: 17 Jan 2020
 *      Author: wilson
 */

#include "Instance.h"

#include <bullet3/BulletCollision/CollisionShapes/btCollisionShape.h>
#include <bullet3/BulletCollision/CollisionShapes/btCompoundShape.h>
#include <bullet3/BulletCollision/CollisionShapes/btBoxShape.h>
#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <bullet3/LinearMath/btTransform.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

#include "../util/physics/InstanceCollisionShapeDef.h"
#include "../util/types/ConversionUtil.h"
#include "../util/physics/PhysicsWorld.h"
#include "../asset/AssetManager.h"
#include "../asset/Asset.h"

namespace awe {

    Instance::Instance(AssetManager * assetManager, PhysicsWorld * physicsWorld, const std::string & assetFilename, const std::string & instanceName, const GLmat4 & transformation) :
        btMotionState(),
    
        assetManager(assetManager),
        physicsWorld(physicsWorld),
        asset(assetManager->requestAsset(assetFilename)),
        transformation(transformation),
        skelNodes(),
        instanceName(instanceName),
        collisionScale(), // Set by onLoaded from decomposing instance transformation.
        collisionShapeDef(NULL),
        rigidBody(NULL) { // Created by onLoaded
        
        // Register ourself last, since this usually results in onLoaded being called.
        asset->registerInstance(this);
        
        // Decompose our transformation matrix, to extract the collision scale.
        GLvec3 pos, skew; GLvec4 perspec; GLquat orient;
        glm::decompose(transformation, collisionScale, orient, pos, skew, perspec);
    }
    
    Instance::~Instance(void) {
        asset->unregisterInstance(this);
        assetManager->abandonAsset(asset);
        
        // Unregister and delete our rigid body.
        if (rigidBody != NULL) {
            physicsWorld->unregisterRigidBody(rigidBody);
            delete rigidBody;
        }
        // Delete our collision def.
        delete collisionShapeDef;
    }
    
    void Instance::onLoaded(void) {
        
        if (getHasCollision()) {
            
            // Create our instance collision shape def. Get primary shape and mass for convenience.
            collisionShapeDef = new InstanceCollisionShapeDef(asset->getCollisionShapeDef(), collisionScale);
            btCollisionShape * primaryShape = collisionShapeDef->getPrimaryCollisionShape();
            GLfloat mass = collisionShapeDef->getTotalMass();
            
            // Work out the inertia from the collision shape. Using the default inertia results in a
            // shape which cannot rotate. Remarkably, nowhere in the documentation does it actually say that.
            btVector3 inertia;
            primaryShape->calculateLocalInertia(mass, inertia);
            
            // Create rigid body, using the mass and primary collision shape.
            // Note that the motion state is this (instance), since instance is a subclass of motion state.
            // Note that for static objects, the mass is set to zero to signify that it is static.
            btRigidBody::btRigidBodyConstructionInfo cInfo(isCollisionStatic() ? 0.0f : mass, this, primaryShape, inertia);
            rigidBody = rigidBodyConstruction(cInfo);
            
            // Add and register ourself with the physics world.
            physicsWorld->registerRigidBody(rigidBody, this);
        }
    }
    
    btRigidBody * Instance::rigidBodyConstruction(btRigidBody::btRigidBodyConstructionInfo & constructionInfo) {
        return new btRigidBody(constructionInfo);
    }

    GLvec3 Instance::calculatePosition(void) const {
        // Just transform a 0,0,0 position by the main transformation.
        return GLvec3(transformation * GLvec4(0,0,0,1));
    }
    
    GLvec3 Instance::getWorldCentreOfMass(void) const {
        return ConversionUtil::bulletToGlmVec3(rigidBody->getCenterOfMassPosition());
    }
    
    GLvec3 Instance::getWorldPhysicsOrigin(void) const {
        // Take a 0,0,0 position, transform by our collision shape def's world to physics transform,
        // then transform by the main transformation. This is the position which the physics
        // library thinks that this instance is located.
        return GLvec3(transformation * collisionShapeDef->getWorldToPhysicsTransform() * GLvec4(0,0,0,1));
    }    
    
    GLfloat Instance::getRigidBodyMass(void) const {
        GLfloat invMass = rigidBody->getInvMass();
        if (invMass == 0.0f) {
            return 0.0f; // Don't divide by zero for static rigid bodies.
        } else {
            return 1.0f / invMass;
        }
    }
    
    void Instance::setTransformation(const GLmat4 & newTransformation, bool updatePhysics) {
        transformation = newTransformation;
        if (updatePhysics && isLoaded() && getHasCollision()) {
            
            // Work out the physics transformation (see getWorldTransform).
            btTransform physicsTransform;
            physicsTransform.setFromOpenGLMatrix(glm::value_ptr(transformation * collisionShapeDef->getWorldToPhysicsTransform()));
            
            // Move the rigid body.
            setWorldTransform(physicsTransform);
        }
    }
    
    // ----------------- Pass-through methods to asset ---------------------
    
    bool Instance::isLoaded(void) const {
        return asset->isLoaded();
    }
    void Instance::waitForLoad(void) const {
        asset->waitForLoad();
    }
    bool Instance::getHasSkeleton(void) const {
        return asset->getHasSkeleton();
    }
    void Instance::update(void) {
        asset->updateInstance(this);
    }
    void Instance::updateAllInstances() {
        asset->updateAllInstances();
    }
    bool Instance::getHasCollision(void) const {
        return asset->getHasCollision();
    }
    bool Instance::isCollisionStatic(void) const {
        return asset->isCollisionStatic();
    }
    
    // ---------------- Bullet physics motion integration ------------------
    
    void Instance::getWorldTransform(btTransform & worldTrans) const {
        // To work out the transformation of shapes WITHIN the physics world,
        // first transform by collision shape def's world to physics transform, THEN
        // transform them by our regular transformation.
        worldTrans.setFromOpenGLMatrix(glm::value_ptr(transformation * collisionShapeDef->getWorldToPhysicsTransform()));
    }
    
    void Instance::setWorldTransform(const btTransform & worldTrans) {
        
        // Work out the transformation of our collision box in the physics world.
        GLmat4 physicsTransform = 
            glm::translate( // Outer transformation done first (inner * outer)   (rotate then translate)
                GLmat4(1.0f), // Identity
                ConversionUtil::bulletToGlmVec3(worldTrans.getOrigin())
            )
            * glm::mat4_cast(ConversionUtil::bulletToGlmQuat(worldTrans.getRotation()));
            
        // Our real-world transform is first transforming by our collision
        // shape def's physics to world transform,
        // THEN transforming by the transformation within the physics world.
        // Don't update physics: This is being updated *by* the physics!
        setTransformation(physicsTransform * collisionShapeDef->getPhysicsToWorldTransform(), false);
        
        // Update since we just changed our transformation.
        update();
    }
}
