/*
 * Instance.h
 *
 *  Created on: 17 Jan 2020
 *      Author: wilson
 */

#ifndef AWE_INSTANCE_INSTANCE_H_
#define AWE_INSTANCE_INSTANCE_H_

#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <bullet3/LinearMath/btMotionState.h>
#include <vector>
#include <string>

#include "../util/geometry/SkelNode.h"
#include "../util/types/Types.h"

class btCollisionShape;
class btTransform;

namespace awe {
    class InstanceCollisionShapeDef;
    class PhysicsWorld;
    class AssetManager;
    class Asset;

    /**
     *
     */
    class Instance : public btMotionState {
    private:
        friend class Asset;
    
        AssetManager * assetManager;
        PhysicsWorld * physicsWorld;
        Asset * asset;
        GLmat4 transformation;
        std::vector<SkelNode> skelNodes;
        std::string instanceName;
        
        // Collision
        GLvec3 collisionScale;                         // The scale portion of our original transformation.
        InstanceCollisionShapeDef * collisionShapeDef; // Our collision def (only allocated if we have physics integration).
        btRigidBody * rigidBody;                       // Our rigid body (only allocated if we have physics integration).
        
        // Don't allow copying: That has not *yet* been implemented.
        Instance(const Instance & other);
        Instance & operator = (const Instance & other);
        
    public:
        /**
         * NOTE: Instance MUST be created AND destroyed from an OpenGL context.
         * Transformation is identity matrix by default.
         */
        Instance(AssetManager * assetManager, PhysicsWorld * physicsWorld, const std::string & assetFilename, const std::string & instanceName, const GLmat4 & transformation = GLmat4(1.0f));
        virtual ~Instance(void);
        
    protected:
        /**
         * This is run by Asset, when:
         *  > We are created and registered, and our Asset is already loaded.
         *  > When our Asset finishes loading, after we are created.
         * This can be overidden by subclasses, but the super method
         * (Instance::onLoaded) must still be called.
         * This is ALWAYS called AFTER our skeleton nodes have been set up by
         * our asset.
         */
        virtual void onLoaded(void);
        
        /**
         * This is provided so that subclasses can customise the creation of the
         * rigid body, (such as using a rigid body subclass, or changing the friction).
         * The construction info automatically has a mass, motion state, collision
         * shape and inertia assigned before being passed to this function.
         * By default, this just creates a rigid body from the construction info
         * without modification.
         */
        virtual btRigidBody * rigidBodyConstruction(btRigidBody::btRigidBodyConstructionInfo & constructionInfo);
        
    public:
        // ----------------- Pass-through methods to asset ---------------------
        
        /**
         * Returns true if our associated asset has loaded.
         */
        bool isLoaded(void) const;
        
        /**
         * Waits for our associated asset to finish loading.
         * Once this returns, isLoaded will return true.
         */
        void waitForLoad(void) const;
        
        /**
         * Returns true if this instance has a loaded skeleton.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        bool getHasSkeleton(void) const;
        
        /**
         * Updates the data stored in the instance buffer about the this
         * instance. This should be called by the instance whenever it's
         * transformation or skeleton is updated.
         * This MUST be called from an OpenGL context.
         * If the asset is not loaded yet, this does nothing.
         */
        void update(void);
        
        /**
         * This acts as if updateInstance has been performed for all of the
         * instances which share the same asset. This is SIGNIFICANTLY faster
         * than calling it for all instances individually, as this copies over
         * instance data with one buffer mapping.
         * If the asset is not loaded yet, this does nothing.
         * This is automatically called when the asset loads initially.
         */
        void updateAllInstances(void);
        
        // ----------------- Pass-through methods to asset (collision) ---------
        
        /**
         * Returns true if this asset is indented to have (static or dynamic)
         * collision.
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        bool getHasCollision(void) const;
        
        /**
         * Returns true if this asset is intended to have a static collision.
         * Note: The result of this call is not defined until isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        bool isCollisionStatic(void) const;
        
        /**
         * Gets the position in the world of our rigid body's centre of mass.
         * Note: The result of this call is not defined until isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        GLvec3 getWorldCentreOfMass(void) const;
        
        /**
         * Gets the position in the world of our rigid body's origin point,
         * i.e: The position of our rigid body according to the physics library.
         * Note that this ought to be identical to the centre of mass, unless
         * a compound shape is in use for our collision shape.
         * Note: The result of this call is not defined until isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        GLvec3 getWorldPhysicsOrigin(void) const;
        
        // -------------------- Collision specific stuff -----------------------
        
        /**
         * Returns the scale portion of our original transformation. This should
         * be used for updating our transformation.
         * Note: The result of this call is not defined until isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        inline const GLvec3 & getCollisionScale(void) const {
            return collisionScale;
        }
        
        /**
         * Allows access to our collision rigid body.
         * Note: The result of this call is not defined until isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        inline btRigidBody * getRigidBody(void) const {
            return rigidBody;
        }
        
        /**
         * A convenience method to access our mass, since our rigid body only
         * stores inverse mass. If our collision is static, this returns zero.
         * Note: The result of this call is not defined unless isLoaded returns
         *       true, and only if getHasCollision returns true.
         */
        GLfloat getRigidBodyMass(void) const;
        
        // ----------------------- Get/set methods -----------------------------
        
        /**
         * Allows access to our associated asset.
         */
        inline Asset * getAsset(void) const {
            return asset;
        }
        
        /**
         * Allows access to our internal transformation.
         */
        inline const GLmat4 & getTransformation(void) const {
            return transformation;
        }
        
        /**
         * Works out our position, based on our transformation.
         * This is a zero vector, transformed by our transformation.
         */
        GLvec3 calculatePosition(void) const;
        
        /**
         * Sets our transformation. Note that this will not update the data
         * stored in our Asset's instance buffer, until updateInstanceBuffer
         * is called.
         * Note however: If we have collision enabled, we are loaded, and
         * updatePhysics is set to true, this will immediately update our
         * position in the collision world.
         */
        void setTransformation(const GLmat4 & newTransformation, bool updatePhysics = true);
        
        /**
         * Allows access to our skeleton nodes. This will be empty unless we
         * have a skeleton, (i.e: getHasSkeleton returns true).
         * Note: The result of this call is not defined until isLoaded returns
         *       true.
         */
        inline const std::vector<SkelNode> & getSkelNodes(void) const { return skelNodes; }
        inline std::vector<SkelNode> & getSkelNodes(void) { return skelNodes; }
        
        /**
         * Gets the name of this instance.
         */
        inline const std::string & getInstanceName(void) const {
            return instanceName;
        }
        
        // ---------------- Bullet physics motion integration ------------------
        
        /**
         * Accesses our current world transform, as a bullet physics transformation.
         * This is used by bullet physics for accessing location information.
         */
        virtual void getWorldTransform(btTransform & worldTrans) const override;
        
        /**
         * Sets our current world transform. This is used by bullet physics for
         * updating our draw position, in response to the instance moving due
         * to physics.
         */
        virtual void setWorldTransform(const btTransform & worldTrans) override;
    };
}

#endif
