#!/usr/bin/env bash

# Shader preprocessing script, version 1
# Copyright (C) 9 August 2020  Daniel Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# processShaders searchPath [includePath includePath ...]
#
# Search path is where to look for shader files to preprocess, include paths
# are provided to the gcc preprocessor as places to look for #included files.
# Note: If the search path is not provided, the current working directory will
#       be used instead. If no include paths are provided, the current working
#       directory is used as a default.


# Extension of source shader files to look for.
extensionIn="glsl"

# Output file extension.
extensionOut="glsl-out"

# Find working directory, and search path. Search path is FIRST COMMAND LINE
# ARGUMENT, or working directory if that is not provided.
workingDir="$PWD"
searchPath="$workingDir"
if [ "$1" != "" ]
then
    searchPath="$1"
fi


# Find inclue paths from arguments 2 and onwards. If the user did not provide
# any of these, use the working directory as a default include path.
# Use absolute paths for include paths, but don't expand symbolic links.
# This is because we will be using these in gcc, but from different directories.
includePaths=""
argIndex=2
while true
do
    if [ "${!argIndex}" == "" ]
    then
        break
    else
        includePaths="$includePaths -I$(realpath -s "${!argIndex}")"
    fi
    argIndex="$((argIndex+1))"
done
if [ "$includePaths" == "" ]
then
    includePaths="-I$workingDir"
fi

# Iterate over files with appropriate extension.
find -L "$searchPath" -name '*.'"$extensionIn" | while read file; do
    
    baseName="$(basename "$file" ".$extensionIn")"
    dirName="$(dirname "$file")"
    cd "$dirName"
    
    inFile="$baseName.$extensionIn"
    outFile="$baseName.$extensionOut"
    
    # Generate a unique temporary file name.
    tmpFile="$baseName-tmpOut$RANDOM"
    while [ -e "$tmpFile" ]
    do
        tmpFile="$baseName-tmpOut$RANDOM"
    done
    
    # Run preprocessor on input file, write to output.
    # "-x c" forces gcc to interpret this as a ".c" file.
    # "-E"   tells gcc to only preprocess
    # "-P"   inhibits the preprocessor from generating linemarkers
    gcc -x c -E $includePaths -P "$inFile" -o "$tmpFile"
    
    # Exit if doing the preprocessing failed. Make sure the temporary file
    # is removed if it was created.
    if [ "$?" -ne "0" ]
    then
        rm -f "$tmpFile"
        exit 1
    fi
    
    # Wrap in a c++ style string literal, and write to the output file.
    echo -n "R\"GLSL_END_TOKEN(" > "$outFile"
    cat "$tmpFile" >> "$outFile"
    echo ")GLSL_END_TOKEN\"" >> "$outFile"
    
    # Remove the temporary file at the end.
    rm "$tmpFile"
    
    cd "$workingDir"
done
